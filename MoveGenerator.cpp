/* 
 * File:   MoveGenerator.cpp
 * Author: Workstation
 * 
 * Created on 04 February 2014, 11:59 AM
 * 
 * Copyright © Steve James <SD.James@outlook.com>
 * Unauthorized copying of this file, via any medium, is strictly prohibited
 * without the express permission of the author.
 * 
 */

#include "Board.h"


// <editor-fold defaultstate="collapsed" desc="Checking Moves (unfinished)">
///**
// * Generate <b>non-captures</b> that give check
// * @return 
// */
//Move* Board::generateCheckingMoves() const{
//
//    Move* moves = new Move[256]();
//    int ptr = 0;
//
//    const std::array<int, 7> &numPieces = (side == WHITE) ? whitePiecesListCount : blackPiecesListCount;
//    const std::array<std::array<int, MAX_PIECES>, 7> &pieceList =
//            (side == WHITE) ? whitePieceList : blackPieceList;
//    Bitboard empty = ~ALL_PIECES_BOARD;
//
//    Bitboard target;
//
//    int king, oppositeKing;
//    Bitboard oppositeBoard, friendlyBoard;
//    Bitboard fb1, fb2, diags, horiz;
//    const Bitboard* pawnMovement;
//    const Bitboard* pawnAttack;
//    const Bitboard* oppositePawnAttack;
//    if (side == WHITE) {
//        king = whiteKing;
//        oppositeKing = blackKing;
//        oppositeBoard = BLACK_PIECES_BOARD;
//        friendlyBoard = WHITE_PIECES_BOARD;
//        fb1 = WHITE_KNIGHT_BOARD | WHITE_ROOK_BOARD | WHITE_PAWN_BOARD;
//        fb2 = WHITE_KNIGHT_BOARD | WHITE_BISHOP_BOARD | WHITE_PAWN_BOARD;
//        diags = WHITE_BISHOP_BOARD | WHITE_QUEEN_BOARD;
//        horiz = WHITE_QUEEN_BOARD | WHITE_ROOK_BOARD;
//        pawnMovement = WHITE_PAWN_MOVES;
//        pawnAttack = WHITE_PAWN_ATTACKS;
//        oppositePawnAttack = BLACK_PAWN_ATTACKS;
//    } else {
//        king = blackKing;
//        oppositeKing = whiteKing;
//        oppositeBoard = WHITE_PIECES_BOARD;
//        friendlyBoard = BLACK_PIECES_BOARD;
//        fb1 = BLACK_KNIGHT_BOARD | BLACK_ROOK_BOARD | BLACK_PAWN_BOARD;
//        fb2 = BLACK_KNIGHT_BOARD | BLACK_BISHOP_BOARD | BLACK_PAWN_BOARD;
//        diags = BLACK_BISHOP_BOARD | BLACK_QUEEN_BOARD;
//        horiz = BLACK_QUEEN_BOARD | BLACK_ROOK_BOARD;
//        pawnMovement = BLACK_PAWN_MOVES;
//        pawnAttack = BLACK_PAWN_ATTACKS;
//        oppositePawnAttack = WHITE_PAWN_ATTACKS;
//    }
//
//    // <editor-fold defaultstate="collapsed" desc="Direct Checks">
//
//    // <editor-fold defaultstate="collapsed" desc="Pawn moves">
//    target = empty & oppositePawnAttack[oppositeKing];
//    for (int i = 0; i < numPieces[PAWN];) {
//        int idx = pieceList[PAWN][i++];
//        Bitboard tMoves = target & pawnMovement[idx];
//        while (tMoves) {
//            //get moves from bitboard
//            int dest = 63 - bitboard::LS1B(tMoves);
//
//            bool flag = true;
//            if (abs(dest - idx) == 16) {
//                //if it's a 2 move push, check that there's nothing directly in front of it
//                flag = !(board[(dest + idx) / 2]);
//            }
//            if (flag) {
//                Move move = createMove(idx, dest, MOVE_NORMAL, true);
//                moves[ptr++] = move;
//            }
//            tMoves &= tMoves - 1;
//        }
//    }
//    // </editor-fold>
//
//    // <editor-fold defaultstate="collapsed" desc="Queen Moves">
//    target = empty & (getRookMoves(oppositeKing, ALL_PIECES_BOARD) |
//            getBishopMoves(oppositeKing, ALL_PIECES_BOARD));
//    for (int i = 0; i < numPieces[QUEEN];) {
//        int pos = pieceList[QUEEN][i++];
//        Bitboard tMoves = target & (getBishopMoves(pos, ALL_PIECES_BOARD)
//                | getRookMoves(pos, ALL_PIECES_BOARD));
//        while (tMoves) {
//            int dest = 63 - bitboard::LS1B(tMoves);
//            Move move = createMove(pos, dest, MOVE_NORMAL, true);
//            moves[ptr++] = move;
//            tMoves &= tMoves - 1;
//        }
//    }
//    // </editor-fold>
//
//    // <editor-fold defaultstate="collapsed" desc="Rook moves">
//    target = empty & getRookMoves(oppositeKing, ALL_PIECES_BOARD);
//    for (int i = 0; i < numPieces[ROOK];) {
//        int pos = pieceList[ROOK][i++];
//        Bitboard tMoves = getRookMoves(pos, ALL_PIECES_BOARD) & target;
//        while (tMoves) {
//            int dest = 63 - bitboard::LS1B(tMoves);
//            Move move = createMove(pos, dest, MOVE_NORMAL, true);
//            moves[ptr++] = move;
//            tMoves &= tMoves - 1;
//        }
//    }
//    // </editor-fold>
//
//    // <editor-fold defaultstate="collapsed" desc="Bishop Moves">
//
//    target = empty & getBishopMoves(oppositeKing, ALL_PIECES_BOARD);
//    for (int i = 0; i < numPieces[BISHOP];) {
//        int pos = pieceList[BISHOP][i++];
//        Bitboard tMoves = getBishopMoves(pos, ALL_PIECES_BOARD) & target;
//        while (tMoves) {
//            int dest = 63 - bitboard::LS1B(tMoves);
//            Move move = createMove(pos, dest, MOVE_NORMAL, true);
//            moves[ptr++] = move;
//            tMoves &= tMoves - 1;
//        }
//    }
//    // </editor-fold>
//
//    // <editor-fold defaultstate="collapsed" desc="Knight Moves">
//    target = empty & OCCUPANCY_MASK_KNIGHT[oppositeKing];
//    for (int i = 0; i < numPieces[KNIGHT];) {
//        int idx = pieceList[KNIGHT][i++];
//        Bitboard tMoves = target & OCCUPANCY_MASK_KNIGHT[idx];
//        while (tMoves) {
//            //get moves from bitboard
//            int dest = 63 - bitboard::LS1B(tMoves);
//            Move move = createMove(idx, dest, MOVE_NORMAL, true);
//            moves[ptr++] = move;
//            tMoves &= tMoves - 1;
//        }
//    }
//    // </editor-fold>
//
//    // </editor-fold>
//
//
//    Bitboard blockers = fb1 & getBishopMoves(oppositeKing, oppositeBoard);
//    if (blockers) {
//        Bitboard checkers = getBishopMoves(oppositeKing, oppositeBoard & ~blockers) &
//                diags;
//        if (checkers) {
//
//        }
//
//    }
//
//
//
//    return moves;
//
//}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Capture Generator">

/**
 * Generates captures and promotions
 */
MoveList Board::generateCaptures() const {

    MoveList moves{0};
    int ptr = 0;

    const PieceListCount &numPieces = (side == WHITE) ? whitePieceListCount : blackPieceListCount;
    const PieceList &pieceList = (side == WHITE) ? whitePieceList : blackPieceList;

    int king;
    Bitboard oppositeKing, oppositeBoard, friendlyBoard;
    const Bitboard* pawnMovement;
    const Bitboard* pawnAttack;
    if (side == WHITE) {
        king = whiteKing;
        oppositeKing = BLACK_KING_BOARD;
        oppositeBoard = BLACK_PIECES_BOARD;
        friendlyBoard = WHITE_PIECES_BOARD;
        pawnMovement = WHITE_PAWN_MOVES;
        pawnAttack = WHITE_PAWN_ATTACKS;
    } else {
        king = blackKing;
        oppositeKing = WHITE_KING_BOARD;
        oppositeBoard = WHITE_PIECES_BOARD;
        friendlyBoard = BLACK_PIECES_BOARD;
        pawnMovement = BLACK_PAWN_MOVES;
        pawnAttack = BLACK_PAWN_ATTACKS;
    }


    // <editor-fold defaultstate="collapsed" desc="En-Passant">

    if (enPassant != -1) {
        bool directCheck;
        Bitboard tMoves;
        if (side == WHITE) {
            directCheck = (WHITE_PAWN_ATTACKS[enPassant] & BLACK_KING_BOARD) > 0;
            int shift = 63 - enPassant - 9;
            Bitboard mask = CLEAR_RANK_6 & (5ull << shift) & CLEAR_RANK_4;
            tMoves = mask & WHITE_PAWN_BOARD;
        } else {
            directCheck = (BLACK_PAWN_ATTACKS[enPassant] & WHITE_KING_BOARD) > 0;
            int shift = 63 - enPassant + 7;
            Bitboard mask = CLEAR_RANK_3 & (5ull << shift) & CLEAR_RANK_5;
            tMoves = mask & BLACK_PAWN_BOARD;
        }
        while (tMoves) {
            int from = 63 - bitboard::LS1B(tMoves);
            Move move = Utils::createMove(from, enPassant, MOVE_CAPTURE_EN_PASSANT, directCheck);
            moves[ptr++] = move;
            tMoves &= tMoves - 1;
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Pawn moves">
    for (int i = 0; i < numPieces[PAWN];) {
        int idx = pieceList[PAWN][i++];
        Bitboard tMoves = EMPTY_BOARD & pawnMovement[idx] & (MASK_RANK_1 | MASK_RANK_8);
        tMoves |= oppositeBoard & pawnAttack[idx];
        while (tMoves) {
            //get moves from bitboard
            int m = bitboard::LS1B(tMoves);
            int dest = 63 - m;
            if (!board[dest]) { //promotion         
                for (int i = MOVE_PROMOTION_QUEEN; i >= MOVE_PROMOTION_KNIGHT; --i) {
                    Move move = Utils::createMove(idx, dest, i);
                    moves[ptr++] = move;
                }
            } else {
                bool directCheck = (pawnAttack[dest] & oppositeKing);
                Move move = Utils::createMove(idx, dest, MOVE_NORMAL, directCheck);
                moves[ptr++] = move;
            }
            tMoves &= tMoves - 1;
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Queen Moves">
    for (int i = 0; i < numPieces[QUEEN];) {
        int pos = pieceList[QUEEN][i++];
        Bitboard tMoves = oppositeBoard & (getBishopMoves(pos, friendlyBoard)
                | getRookMoves(pos, friendlyBoard));
        while (tMoves) {
            int dest = 63 - bitboard::LS1B(tMoves);
            Move move = Utils::createMove(pos, dest);
            moves[ptr++] = move;
            tMoves &= tMoves - 1;
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Rook moves">
    for (int i = 0; i < numPieces[ROOK];) {
        int pos = pieceList[ROOK][i++];
        Bitboard tMoves = getRookMoves(pos, friendlyBoard) & oppositeBoard;
        while (tMoves) {
            int dest = 63 - bitboard::LS1B(tMoves);
            Move move = Utils::createMove(pos, dest);
            moves[ptr++] = move;
            tMoves &= tMoves - 1;
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Bishop Moves">
    for (int i = 0; i < numPieces[BISHOP];) {
        int pos = pieceList[BISHOP][i++];
        Bitboard tMoves = getBishopMoves(pos, friendlyBoard) & oppositeBoard;
        while (tMoves) {
            int dest = 63 - bitboard::LS1B(tMoves);
            Move move = Utils::createMove(pos, dest);
            moves[ptr++] = move;
            tMoves &= tMoves - 1;
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Knight Moves">
    for (int i = 0; i < numPieces[KNIGHT];) {
        int idx = pieceList[KNIGHT][i++];
        Bitboard tMoves = oppositeBoard & OCCUPANCY_MASK_KNIGHT[idx];
        while (tMoves) {
            //get moves from bitboard
            int dest = 63 - bitboard::LS1B(tMoves);
            bool directCheck = (OCCUPANCY_MASK_KNIGHT[dest] & oppositeKing);
            Move move = Utils::createMove(idx, dest, MOVE_NORMAL, directCheck);
            moves[ptr++] = move;
            tMoves &= tMoves - 1;
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="King Moves">
    Bitboard tMoves = (~friendlyBoard) & OCCUPANCY_MASK_KING[king];
    while (tMoves) {
        //get moves from bitboard
        int m = bitboard::LS1B(tMoves);
        int dest = 63 - m;
        Move move = Utils::createMove(king, dest);
        moves[ptr++] = move;
        tMoves &= tMoves - 1;
    }
    // </editor-fold>

    return moves;
}
// </editor-fold>

//TOOD speed up! (very slow)

void Board::generateCheckEvasions(Bitboard checkers, int king, MoveList& moves, int& ptr) const {

    const PieceListCount &numPieces = (side == WHITE) ? whitePieceListCount : blackPieceListCount;
    const PieceList &pieceList = (side == WHITE) ? whitePieceList : blackPieceList;

    /*
     *******************************************************************************
     *                                                                             *
     *   GenerateCheckEvasions() is used to generate moves when the king is in     *
     *   check.                                                                    *
     *                                                                             *
     *   Three types of check-evasion moves are generated:                         *
     *                                                                             *
     *   (1) Generate king moves to squares that are not attacked by the           *
     *   opponent's pieces.  This includes capture and non-capture moves.          *
     *                                                                             *
     *   (2) Generate interpositions along the rank/file that the checking attack  *
     *   is coming along (assuming (a) only one piece is checking the king, and    *
     *   (b) the checking piece is a sliding piece [bishop, rook, queen]).         *
     *                                                                             *
     *   (3) Generate capture moves, but only to the square(s) that are giving     *
     *   check.  Captures are a special case.  If there is one checking piece,     *
     *   then capturing it by any piece is tried.  If there are two pieces         *
     *   checking the king, then the only legal capture to try is for the king to  *
     *   capture one of the checking pieces that is on an un-attacked square.      *
     *                                                                             *
     *******************************************************************************
     */


    const Bitboard* pawnMovement = (side == WHITE) ? WHITE_PAWN_MOVES : BLACK_PAWN_MOVES;
    const Bitboard* pawnAttacks = (side == WHITE) ? WHITE_PAWN_ATTACKS : BLACK_PAWN_ATTACKS;
    Bitboard friendly = (side == WHITE) ? WHITE_PIECES_BOARD : BLACK_PIECES_BOARD;
    Bitboard target = 0;
    int pos1 = -1, pos2 = -1;
    Direction dir1 = Direction::NONE;
    Direction dir2 = Direction::NONE;
    int nCheckers = bitboard::getSparseBitCount(checkers);

    if (nCheckers == 1) {
        pos1 = 63 - bitboard::LS1B(checkers);
        if (abs(board[pos1]) != PAWN) {
            dir1 = DIRECTIONS[pos1][king];
        }
        target = bitboard::getInterposed(king, pos1, dir1) | checkers;
        //  bitboard::show(target);
    } else {
        pos1 = 63 - bitboard::LS1B(checkers);
        if (abs(board[pos1]) != PAWN) {
            dir1 = DIRECTIONS[pos1][king];
        }
        Bitboard temp = checkers & (checkers - 1);
        pos2 = 63 - bitboard::LS1B(temp);
        if (abs(board[pos2]) != PAWN) {
            dir2 = DIRECTIONS[pos2][king];
        }
    }

    Bitboard tMoves = OCCUPANCY_MASK_KING[king] & ~friendly;
    while (tMoves) {
        int to = 63 - bitboard::LS1B(tMoves);

        if (isLegalKingMove(to, -side, king, dir1, dir2, pos1, pos2)) {
            moves[ptr++] = Utils::createMove(king, to);
        }
        tMoves &= tMoves - 1;
    }

    if (nCheckers == 1) {

        for (int i = 0; i < numPieces[KNIGHT];) {
            int idx = pieceList[KNIGHT][i++];
            if (!isPinnedOnKing(idx, king)) {
                Bitboard tMoves = target & OCCUPANCY_MASK_KNIGHT[idx];
                while (tMoves) {
                    //get moves from bitboard
                    int dest = 63 - bitboard::LS1B(tMoves);
                    Move move = Utils::createMove(idx, dest);
                    moves[ptr++] = move;
                    tMoves &= tMoves - 1;
                }
            }
        }


        for (int i = 0; i < numPieces[BISHOP];) {
            int idx = pieceList[BISHOP][i++];
            if (!isPinnedOnKing(idx, king)) {
                Bitboard tMoves = target & getBishopMoves(idx, friendly);
                while (tMoves) {
                    //get moves from bitboard
                    int dest = 63 - bitboard::LS1B(tMoves);
                    Move move = Utils::createMove(idx, dest);
                    moves[ptr++] = move;
                    tMoves &= tMoves - 1;
                }
            }
        }

        for (int i = 0; i < numPieces[ROOK];) {
            int idx = pieceList[ROOK][i++];
            if (!isPinnedOnKing(idx, king)) {
                Bitboard tMoves = target & getRookMoves(idx, friendly);
                while (tMoves) {
                    //get moves from bitboard
                    int dest = 63 - bitboard::LS1B(tMoves);
                    Move move = Utils::createMove(idx, dest);
                    moves[ptr++] = move;
                    tMoves &= tMoves - 1;
                }
            }
        }

        for (int i = 0; i < numPieces[QUEEN];) {
            int idx = pieceList[QUEEN][i++];
            if (!isPinnedOnKing(idx, king)) {
                Bitboard tMoves = target & (getRookMoves(idx, friendly) | getBishopMoves(idx, friendly));
                while (tMoves) {
                    //get moves from bitboard
                    int dest = 63 - bitboard::LS1B(tMoves);
                    Move move = Utils::createMove(idx, dest);
                    moves[ptr++] = move;
                    tMoves &= tMoves - 1;
                }
            }
        }

        /*
         ************************************************************
         *                                                          *
         *   Now, produce pawn moves.  This is done differently due *
         *   to inconsistencies in the way a pawn moves when it     *
         *   captures as opposed to normal non-capturing moves.     *
         *   another exception is capturing enpassant.  The first   *
         *   step is to generate all possible pawn moves.  We do    *
         *   this in 2 operations:  (1) shift the pawns forward one *
         *   rank then AND with empty squares;  (2) shift the pawns *
         *   forward two ranks and then AND with empty squares.     *
         *                                                          *
         ************************************************************
         */

        Bitboard pawnTarget = EMPTY_BOARD & target;
        //   bitboard::show(pawnTarget);
        for (int i = 0; i < numPieces[PAWN];) {
            int idx = pieceList[PAWN][i++];
            if (!isPinnedOnKing(idx, king)) {
                Bitboard tMoves = (pawnTarget & pawnMovement[idx]) | (checkers & pawnAttacks[idx]);
                while (tMoves) {
                    //get moves from bitboard
                    int dest = 63 - bitboard::LS1B(tMoves);
                    if (dest < 8 || dest > 55) { //promotion         
                        for (int i = MOVE_PROMOTION_QUEEN; i >= MOVE_PROMOTION_KNIGHT; i--) {
                            Move move = Utils::createMove(idx, dest, i);
                            moves[ptr++] = move;
                        }
                    } else {
                        bool flag = true;
                        if (abs(dest - idx) == 16) {
                            //if it's a 2 move push, check that there's nothing directly in front of it
                            flag = ((board[(dest + idx) / 2] == EMPTY_SQUARE));
                        }
                        if (flag) {
                            Move move = Utils::createMove(idx, dest);
                            moves[ptr++] = move;
                        }
                    }
                    tMoves &= tMoves - 1;
                }
            }
        }

        /****************************************************************************
         * If there is an en passant, and we are in checkEvasion, then there must   *
         * be a pawn directly attacking our king, which can be captured en-passant. *
         * There can be no possibility of discovered checks, since only a bishop    *
         * hiding behind the checking pawn could cause one, which would imply that  *
         * before the opposition made the pawn push, that bishop had the king in    *
         * check, which is impossible (since the king is never left in check.       *
         * There may be the possibility of an en passant capture exposing the king  *
         * However, in this case, the king would not have been in check and thus we *
         * would not be in this method, but in the regular move-generation method.  *                                                     *
         ****************************************************************************/

        int capPawn;
        if (enPassant != -1) {
            Bitboard tMovesFrom;
            if (side == WHITE) {
                capPawn = enPassant + 8;
                if (!(BLACK_PAWN_ATTACKS[capPawn] & WHITE_KING_BOARD)) {
                    //pawn that can be taken en passant does not give check
                    return;
                }
                int shift = 63 - enPassant - 9;
                Bitboard mask = CLEAR_RANK_6 & (5ull << shift) & CLEAR_RANK_4;
                tMovesFrom = mask & WHITE_PAWN_BOARD;
            } else {
                capPawn = enPassant - 8;
                if (!(WHITE_PAWN_ATTACKS[capPawn] & BLACK_KING_BOARD)) {
                    //pawn that can be taken en passant does not give check
                    return;
                }
                int shift = 63 - enPassant + 7;
                Bitboard mask = CLEAR_RANK_3 & (5ull << shift) & CLEAR_RANK_5;
                tMovesFrom = mask & BLACK_PAWN_BOARD;
            }
            while (tMovesFrom) {
                int from = 63 - bitboard::LS1B(tMovesFrom);
                if (!isPinnedOnKing(from, king)) {
                    Move move = Utils::createMove(from, enPassant, MOVE_CAPTURE_EN_PASSANT);
                    moves[ptr++] = move;
                }
                tMovesFrom &= tMovesFrom - 1;
            }
        }
    }

}

// <editor-fold defaultstate="collapsed" desc="Pseudo-Move Generator">

MoveList Board::generatePseudoMoves() const {

    MoveList moves{0};
    int ptr = 0;
    int king = getKingPosition();

    // <editor-fold defaultstate="collapsed" desc="Castling Moves">
    if (side == WHITE) {
        bool flag = (whiteCastle == WHITE_CASTLE_BOTH);
        if ((whiteCastle == WHITE_CASTLE_SHORT || flag) && ((ALL_PIECES_BOARD & F1G1_MASK) == 0)) {
            Move move = Utils::createMove(E1, G1, MOVE_SHORT_CASTLE);
            moves[ptr++] = move;
        }
        if ((flag || whiteCastle == WHITE_CASTLE_LONG) && ((ALL_PIECES_BOARD & B1C1D1_MASK) == 0)) {
            Move move = Utils::createMove(E1, C1, MOVE_LONG_CASTLE);
            moves[ptr++] = move;
        }
    } else {
        bool flag = (blackCastle == BLACK_CASTLE_BOTH);
        if (flag || blackCastle == BLACK_CASTLE_SHORT) {
            if ((ALL_PIECES_BOARD & F8G8_MASK) == 0) {
                Move move = Utils::createMove(E8, G8, MOVE_SHORT_CASTLE);
                moves[ptr++] = move;
            }
        }
        if (flag || blackCastle == BLACK_CASTLE_LONG) {
            if ((ALL_PIECES_BOARD & B8C8D8_MASK) == 0) {

                Move move = Utils::createMove(E8, C8, MOVE_LONG_CASTLE);
                moves[ptr++] = move;
            }
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="En-Passant">

    if (enPassant != -1) {
        bool directCheck;
        Bitboard tMoves;
        if (side == WHITE) {
            directCheck = (WHITE_PAWN_ATTACKS[enPassant] & BLACK_KING_BOARD) > 0;
            int shift = 63 - enPassant - 9;
            Bitboard mask = CLEAR_RANK_6 & (5ull << shift) & CLEAR_RANK_4;
            tMoves = mask & WHITE_PAWN_BOARD;
        } else {
            directCheck = (BLACK_PAWN_ATTACKS[enPassant] & WHITE_KING_BOARD) > 0;
            int shift = 63 - enPassant + 7;
            Bitboard mask = CLEAR_RANK_3 & (5ull << shift) & CLEAR_RANK_5;
            tMoves = mask & BLACK_PAWN_BOARD;
        }
        while (tMoves > 0) {

            int k = bitboard::LS1B(tMoves);
            int from = 63 - k;
            Move move = Utils::createMove(from, enPassant, MOVE_CAPTURE_EN_PASSANT, directCheck);
            moves[ptr++] = move;
            tMoves &= tMoves - 1;
        }
    }
    // </editor-fold>

    const PieceListCount &numPieces = (side == WHITE) ? whitePieceListCount : blackPieceListCount;
    const PieceList &pieceList =
            (side == WHITE) ? whitePieceList : blackPieceList;

    Bitboard oppositeKing, oppositeBoard, friendlyBoard;
    const Bitboard* pawnMovement;
    const Bitboard* pawnAttack;
    if (side == WHITE) {

        oppositeKing = BLACK_KING_BOARD;
        oppositeBoard = BLACK_PIECES_BOARD;
        friendlyBoard = WHITE_PIECES_BOARD;
        pawnMovement = WHITE_PAWN_MOVES;
        pawnAttack = WHITE_PAWN_ATTACKS;
    } else {

        oppositeKing = WHITE_KING_BOARD;
        oppositeBoard = WHITE_PIECES_BOARD;
        friendlyBoard = BLACK_PIECES_BOARD;
        pawnMovement = BLACK_PAWN_MOVES;
        pawnAttack = BLACK_PAWN_ATTACKS;
    }

    // <editor-fold defaultstate="collapsed" desc="Pawn moves">

    for (int i = 0; i < numPieces[PAWN];) {
        int idx = pieceList[PAWN][i++];
        Bitboard tMoves = EMPTY_BOARD & pawnMovement[idx];
        Bitboard tAttacks = oppositeBoard & pawnAttack[idx];
        tMoves |= tAttacks;
        while (tMoves) {
            //get moves from bitboard
            int m = bitboard::LS1B(tMoves);
            int dest = 63 - m;
            if (dest < A7 || dest > H2) { //promotion         
                for (int i = MOVE_PROMOTION_QUEEN; i >= MOVE_PROMOTION_KNIGHT; i--) {
                    Move move = Utils::createMove(idx, dest, i);
                    moves[ptr++] = move;
                }
            } else {
                bool directCheck = (pawnAttack[dest] & oppositeKing);
                bool flag = true;
                if (abs(dest - idx) == 16) {
                    //if it's a 2 move push, check that there's nothing directly in front of it
                    flag = ((board[(dest + idx) / 2] == EMPTY_SQUARE));
                }
                if (flag) {
                    Move move = Utils::createMove(idx, dest, MOVE_NORMAL, directCheck);
                    moves[ptr++] = move;
                }
            }
            tMoves &= tMoves - 1;
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Queen Moves">
    for (int i = 0; i < numPieces[QUEEN];) {
        int pos = pieceList[QUEEN][i++];
        Bitboard tMoves = getBishopMoves(pos, friendlyBoard) | getRookMoves(pos, friendlyBoard);
        while (tMoves) {
            int dest = 63 - bitboard::LS1B(tMoves);
            Move move = Utils::createMove(pos, dest);
            moves[ptr++] = move;
            tMoves &= tMoves - 1;
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Rook moves">
    for (int i = 0; i < numPieces[ROOK];) {
        int pos = pieceList[ROOK][i++];
        Bitboard tMoves = getRookMoves(pos, friendlyBoard);
        while (tMoves) {
            int dest = 63 - bitboard::LS1B(tMoves);
            Move move = Utils::createMove(pos, dest);
            moves[ptr++] = move;
            tMoves &= tMoves - 1;
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Bishop Moves">
    for (int i = 0; i < numPieces[BISHOP];) {
        int pos = pieceList[BISHOP][i++];
        Bitboard tMoves = getBishopMoves(pos, friendlyBoard);
        while (tMoves) {
            int dest = 63 - bitboard::LS1B(tMoves);
            Move move = Utils::createMove(pos, dest);
            moves[ptr++] = move;
            tMoves &= tMoves - 1;
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Knight Moves">
    for (int i = 0; i < numPieces[KNIGHT];) {
        int idx = pieceList[KNIGHT][i++];
        Bitboard tMoves = (~friendlyBoard) & OCCUPANCY_MASK_KNIGHT[idx];
        while (tMoves) {
            //get moves from bitboard
            int dest = 63 - bitboard::LS1B(tMoves);
            bool directCheck = (OCCUPANCY_MASK_KNIGHT[dest] & oppositeKing);
            Move move = Utils::createMove(idx, dest, MOVE_NORMAL, directCheck);
            moves[ptr++] = move;
            tMoves &= tMoves - 1;
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="King Moves">
    Bitboard tMoves = (~friendlyBoard) & OCCUPANCY_MASK_KING[king];
    while (tMoves) {
        //get moves from bitboard
        int m = bitboard::LS1B(tMoves);
        int dest = 63 - m;
        Move move = Utils::createMove(king, dest);
        moves[ptr++] = move;
        tMoves &= tMoves - 1;
    }
    // </editor-fold>

    return moves;
}

MoveList Board::generatePseudoMoves(bool& legal) const {

    MoveList moves{0};
    int ptr = 0;
    int king = getKingPosition();
    Bitboard checkers = attacksTo(king, -side);
    if (checkers) {
        generateCheckEvasions(checkers, king, moves, ptr);
        legal = true;
        return moves;
    }
    legal = false;

    // <editor-fold defaultstate="collapsed" desc="Castling Moves">
    if (side == WHITE) {
        bool flag = (whiteCastle == WHITE_CASTLE_BOTH);
        if ((whiteCastle == WHITE_CASTLE_SHORT || flag) && ((ALL_PIECES_BOARD & F1G1_MASK) == 0)) {
            Move move = Utils::createMove(E1, G1, MOVE_SHORT_CASTLE);
            moves[ptr++] = move;
        }
        if ((flag || whiteCastle == WHITE_CASTLE_LONG) && ((ALL_PIECES_BOARD & B1C1D1_MASK) == 0)) {
            Move move = Utils::createMove(E1, C1, MOVE_LONG_CASTLE);
            moves[ptr++] = move;
        }
    } else {
        bool flag = (blackCastle == BLACK_CASTLE_BOTH);
        if (flag || blackCastle == BLACK_CASTLE_SHORT) {
            if ((ALL_PIECES_BOARD & F8G8_MASK) == 0) {
                Move move = Utils::createMove(E8, G8, MOVE_SHORT_CASTLE);
                moves[ptr++] = move;
            }
        }
        if (flag || blackCastle == BLACK_CASTLE_LONG) {
            if ((ALL_PIECES_BOARD & B8C8D8_MASK) == 0) {

                Move move = Utils::createMove(E8, C8, MOVE_LONG_CASTLE);
                moves[ptr++] = move;
            }
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="En-Passant">

    if (enPassant != -1) {
        bool directCheck;
        Bitboard tMoves;
        if (side == WHITE) {
            directCheck = (WHITE_PAWN_ATTACKS[enPassant] & BLACK_KING_BOARD) > 0;
            int shift = 63 - enPassant - 9;
            Bitboard mask = CLEAR_RANK_6 & (5ull << shift) & CLEAR_RANK_4;
            tMoves = mask & WHITE_PAWN_BOARD;
        } else {
            directCheck = (BLACK_PAWN_ATTACKS[enPassant] & WHITE_KING_BOARD) > 0;
            int shift = 63 - enPassant + 7;
            Bitboard mask = CLEAR_RANK_3 & (5ull << shift) & CLEAR_RANK_5;
            tMoves = mask & BLACK_PAWN_BOARD;
        }
        while (tMoves > 0) {

            int k = bitboard::LS1B(tMoves);
            int from = 63 - k;
            Move move = Utils::createMove(from, enPassant, MOVE_CAPTURE_EN_PASSANT, directCheck);
            moves[ptr++] = move;
            tMoves &= tMoves - 1;
        }
    }
    // </editor-fold>

    const PieceListCount &numPieces = (side == WHITE) ? whitePieceListCount : blackPieceListCount;
    const PieceList &pieceList =
            (side == WHITE) ? whitePieceList : blackPieceList;

    Bitboard oppositeKing, oppositeBoard, friendlyBoard;
    const Bitboard* pawnMovement;
    const Bitboard* pawnAttack;
    if (side == WHITE) {

        oppositeKing = BLACK_KING_BOARD;
        oppositeBoard = BLACK_PIECES_BOARD;
        friendlyBoard = WHITE_PIECES_BOARD;
        pawnMovement = WHITE_PAWN_MOVES;
        pawnAttack = WHITE_PAWN_ATTACKS;
    } else {

        oppositeKing = WHITE_KING_BOARD;
        oppositeBoard = WHITE_PIECES_BOARD;
        friendlyBoard = BLACK_PIECES_BOARD;
        pawnMovement = BLACK_PAWN_MOVES;
        pawnAttack = BLACK_PAWN_ATTACKS;
    }

    // <editor-fold defaultstate="collapsed" desc="Pawn moves">

    for (int i = 0; i < numPieces[PAWN];) {
        int idx = pieceList[PAWN][i++];
        Bitboard tMoves = EMPTY_BOARD & pawnMovement[idx];
        Bitboard tAttacks = oppositeBoard & pawnAttack[idx];
        tMoves |= tAttacks;
        while (tMoves) {
            //get moves from bitboard
            int m = bitboard::LS1B(tMoves);
            int dest = 63 - m;
            if (dest < 8 || dest > 55) { //promotion         
                for (int i = MOVE_PROMOTION_QUEEN; i >= MOVE_PROMOTION_KNIGHT; i--) {
                    Move move = Utils::createMove(idx, dest, i);
                    moves[ptr++] = move;
                }
            } else {
                bool directCheck = (pawnAttack[dest] & oppositeKing);
                bool flag = true;
                if (abs(dest - idx) == 16) {
                    //if it's a 2 move push, check that there's nothing directly in front of it
                    flag = ((board[(dest + idx) / 2] == EMPTY_SQUARE));
                }
                if (flag) {
                    Move move = Utils::createMove(idx, dest, MOVE_NORMAL, directCheck);
                    moves[ptr++] = move;
                }
            }
            tMoves &= tMoves - 1;
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Queen Moves">
    for (int i = 0; i < numPieces[QUEEN];) {
        int pos = pieceList[QUEEN][i++];
        Bitboard tMoves = getBishopMoves(pos, friendlyBoard) | getRookMoves(pos, friendlyBoard);
        while (tMoves) {
            int dest = 63 - bitboard::LS1B(tMoves);
            Move move = Utils::createMove(pos, dest);
            moves[ptr++] = move;
            tMoves &= tMoves - 1;
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Rook moves">
    for (int i = 0; i < numPieces[ROOK];) {
        int pos = pieceList[ROOK][i++];
        Bitboard tMoves = getRookMoves(pos, friendlyBoard);
        while (tMoves) {
            int dest = 63 - bitboard::LS1B(tMoves);
            Move move = Utils::createMove(pos, dest);
            moves[ptr++] = move;
            tMoves &= tMoves - 1;
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Bishop Moves">
    for (int i = 0; i < numPieces[BISHOP];) {
        int pos = pieceList[BISHOP][i++];
        Bitboard tMoves = getBishopMoves(pos, friendlyBoard);
        while (tMoves) {
            int dest = 63 - bitboard::LS1B(tMoves);
            Move move = Utils::createMove(pos, dest);
            moves[ptr++] = move;
            tMoves &= tMoves - 1;
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Knight Moves">
    for (int i = 0; i < numPieces[KNIGHT];) {
        int idx = pieceList[KNIGHT][i++];
        Bitboard tMoves = (~friendlyBoard) & OCCUPANCY_MASK_KNIGHT[idx];
        while (tMoves) {
            //get moves from bitboard
            int dest = 63 - bitboard::LS1B(tMoves);
            bool directCheck = (OCCUPANCY_MASK_KNIGHT[dest] & oppositeKing);
            Move move = Utils::createMove(idx, dest, MOVE_NORMAL, directCheck);
            moves[ptr++] = move;
            tMoves &= tMoves - 1;
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="King Moves">
    Bitboard tMoves = (~friendlyBoard) & OCCUPANCY_MASK_KING[king];
    while (tMoves) {
        //get moves from bitboard
        int m = bitboard::LS1B(tMoves);
        int dest = 63 - m;
        Move move = Utils::createMove(king, dest);
        moves[ptr++] = move;
        tMoves &= tMoves - 1;
    }
    // </editor-fold>

    return moves;
}
// </editor-fold>

bool Board::isPinnedOnKing(int sq, int king, Bitboard white, Bitboard black) const {


    Bitboard friendly, bishops, rooks, opp;
    Bitboard k = bitboard::getBitBoard(king);

    if (side == WHITE) {
        friendly = black;
        opp = white;
        bishops = BLACK_QUEEN_BOARD | BLACK_BISHOP_BOARD;
        rooks = BLACK_QUEEN_BOARD | BLACK_ROOK_BOARD;
    } else {
        opp = black;
        friendly = white;
        bishops = WHITE_QUEEN_BOARD | WHITE_BISHOP_BOARD;
        rooks = WHITE_QUEEN_BOARD | WHITE_ROOK_BOARD;
    }

    Direction dir = DIRECTIONS[sq][king];
    switch (dir) {

        case Direction::NONE:
            return false;
        case Direction::HORIZONTAL:
        case Direction::VERTICAL:
            if (getRookMoves(sq, friendly) & k)
                return getRookMoves(sq, opp) & rooks;
            return false;
        case Direction::LOWER_LEFT_TOP_RIGHT:
        case Direction::LOWER_RIGHT_TOP_LEFT:
            if (getBishopMoves(sq, friendly) & k)
                return getBishopMoves(sq, opp) & bishops;
            return false;
    }
    return false;
}

bool Board::isPinnedOnKing(int sq, int king) const {


    Bitboard friendly, bishops, rooks, opp;
    Bitboard k = bitboard::getBitBoard(63 - king);
    if (side == WHITE) {
        friendly = BLACK_PIECES_BOARD;
        opp = WHITE_PIECES_BOARD;
        bishops = BLACK_QUEEN_BOARD | BLACK_BISHOP_BOARD;
        rooks = BLACK_QUEEN_BOARD | BLACK_ROOK_BOARD;
    } else {
        opp = BLACK_PIECES_BOARD;
        friendly = WHITE_PIECES_BOARD;
        bishops = WHITE_QUEEN_BOARD | WHITE_BISHOP_BOARD;
        rooks = WHITE_QUEEN_BOARD | WHITE_ROOK_BOARD;
    }

    Direction dir = DIRECTIONS[sq][king];
    switch (dir) {

        case Direction::NONE:
            return false;
        case Direction::HORIZONTAL:
            if (getRookMoves(sq, friendly) & k)
                return (getRookMoves(sq, opp) & MASK_RANKS[sq]) & rooks;
            return false;
        case Direction::VERTICAL:
            if (getRookMoves(sq, friendly) & k)
                return (getRookMoves(sq, opp) & MASK_FILES[sq]) & rooks;
            return false;
        case Direction::LOWER_LEFT_TOP_RIGHT:
            if (getBishopMoves(sq, friendly) & k)
                return (getBishopMoves(sq, opp) & MASK_A1_H8_DIAG[sq]) & bishops;
            return false;
        case Direction::LOWER_RIGHT_TOP_LEFT:
            if (getBishopMoves(sq, friendly) & k)
                return (getBishopMoves(sq, opp) & MASK_A8_H1_DIAG[sq]) & bishops;
            return false;
    }
    return false;
}
