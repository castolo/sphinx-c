/* 
 * File:   Sphinx.cpp
 * Author: Steve James
 * 
 * Created on 11 April 2013, 8:37 AM
 */

#include "Sphinx.h"
#include "TranspositionTable.h"
#include <fstream>
#include <ctime>

#include <cstring>
#include <algorithm>
#include <stdio.h>

#include "Constants.h"
#include "HashBoard.h"
#include "Board.h"
#include "Line.h"

void Sphinx::reset() {
    openingBook = nullptr;
    table = nullptr;
    sequenceNum = 0;
    thinking = false;
    interrupted = false;
    std::fill(options.begin(), options.end(), true);
}

Move Sphinx::playMove(std::string str) {
    MoveList moves = board.generatePseudoMoves();
    for (int i = 0; Move move = moves[i]; i++) {
        if (str == Utils::toInputNotation(move)) {
            board.makeMove(move);
            return move;
        }
    }
    return NULL_MOVE;
}


// <editor-fold defaultstate="collapsed" desc="Alpha-beta with TT">

int Sphinx::alphaBeta(int ply, int alpha, int beta, bool nullMovesAllowed) {

    if (isInterrupted() || !isTimeLeft()) {
        interrupted = true;
        return 0;
    }
    nodes++;


    if (board.getRepetitions() >= 2) {
        return -DRAW_VALUE;
    }
    Optional<HashEntry> entry = table->get(board.getHash());
    if ((entry) && (entry->depth >= ply)) {
        switch (entry->flag) {
            case EvalType::EXACT:
                return entry->eval;
            case EvalType::UPPERBOUND:
                if (entry->eval <= alpha) {
                    return alpha;
                }
                break;
            case EvalType::LOWERBOUND:
                if (entry->eval >= beta) {
                    return beta;
                }
                break;
            default:
                throw std::logic_error("Entry has no flag type!");

        }
    }

    if (ply <= 0) {
        int eval = quiescenceSearch(alpha, beta);
        if (eval >= beta) {
            table->put(board.getHash(), NULL_MOVE, eval, 0, EvalType::LOWERBOUND, sequenceNum);
        } else if (eval <= alpha) {
            table->put(board.getHash(), NULL_MOVE, eval, 0, EvalType::UPPERBOUND, sequenceNum);
        } else {
            table->put(board.getHash(), NULL_MOVE, eval, 0, EvalType::EXACT, sequenceNum);
        }
        return eval;
    }


    /*********************
     * Null-Move Pruning *
     *********************/
    int R = 2;
    if (nullMovesAllowed && !useLastMove) {
        if (board.canMakeNullMove()) {
            board.makeMove(NULL_MOVE);
            int score = -alphaBeta(ply - R - 1, -beta, -beta + 1, false);
            board.undoMove(NULL_MOVE);
            if (score >= beta) {
                if (!isInterrupted()) {
                    table->put(board.getHash(), NULL_MOVE, score, ply, EvalType::LOWERBOUND, sequenceNum);
                }
                return score;
            }
        }
    }
    MoveList pseudo = board.generatePseudoMoves();
    bool legalMove = false;
    int bestScore = -INFTY;
    Move bestMove = 0;
    EvalType flag = EvalType::UPPERBOUND;
    StaggeredMoves<12> moves = rankMoves(pseudo, ply);
    int eval;
    bool isInCheck = board.isInCheck();
    int i = 0;
    for (Move move : moves) {
        if (board.makeMove(move)) {
            legalMove = true;
            int reps = 0;
            if (ply >= depth - 2) {
                reps = board.getRepetitions();
            }
            if (reps >= 2) {
                eval = -DRAW_VALUE;
            } else if (flag == EvalType::EXACT) {
                /***********************
                 * Late Move Reduction *
                 ***********************/
                if (!isInCheck && canReduceMove(ply, i, move)) {
                    eval = -alphaBeta(ply - 2, -alpha - 1, -alpha, true);
                } else {
                    eval = alpha + 1; //ensures that full search is done below
                }
                /******************************
                 * Principal Variation Search *
                 ******************************/
                if (eval > alpha) {
                    eval = -alphaBeta(ply - 1, -alpha - 1, -alpha,
                            true);
                    //If we find a line better than PVS, redo the search:
                    if (eval > alpha && eval < beta) {
                        eval = -alphaBeta(ply - 1, -beta, -alpha,
                                true);
                    }
                }
            } else {
                eval = -alphaBeta(ply - 1, -beta, -alpha,
                        true);
            }
            board.undoMove(move);
            if (isInterrupted()) {
                return 0;
            }
            if (eval > bestScore) {
                if (eval >= beta) {
                    table->put(board.getHash(), move, eval, ply, EvalType::LOWERBOUND, sequenceNum);
                    killerHeuristic.add(ply, move);
                    useLastMove = false;
                    return beta;
                }
                bestScore = eval;
                bestMove = move;
                if (eval > alpha) {
                    flag = EvalType::EXACT;
                    useLastMove = false;
                    alpha = eval;
                }
            }
            table->put(board.getHash(), bestMove, bestScore, ply, flag, sequenceNum);
        } else {
            board.undoMove(move);
        }
        ++i;
    }
    if (!legalMove) {
        return completionCheck(ply);
    }
    if (ply == depth) {
        //first call to alpha-beta, store best move in case it's overwritten in TT
        globalBestMove = bestMove;
    }
    table->put(board.getHash(), bestMove, bestScore, ply, flag, sequenceNum);
    return alpha;
}
// </editor-fold>

Line Sphinx::search(Time whiteTime, Time blackTime) {

    Time timeLeft = board.getSide() == WHITE ? whiteTime : blackTime;
    if (!table) {
        table = std::make_unique<TranspositionTable>(Byte(500000000));
    }
    if (options[Option::OPENING_BOOK] && !openingBook) {
        openingBook = std::make_unique<PolyGlotBook>();
    }
    Time mtime = 0;
    thinking = true;
    interrupted = false;
    timeAllowed = calculateTime(timeLeft);
    sequenceNum++;
    nodes = 0;
    searchStarted = Utils::getCurrentTime();
    Line ans;
    useLastMove = false;
    int alpha = -INFTY;
    int beta = INFTY;
    killerHeuristic.clear();
    int score = 0;
    globalBestMove = NULL_MOVE;

    // <editor-fold defaultstate="collapsed" desc="Endgame Tablebase">

    /**************************
     *  Try Endgame Tablebase *
     **************************/
    if (options[Option::ENDGAME_TABLEBASE]) {
        //        int side = isSimpleEndgame();
        //        if (side != 0) {
        //            if (!tableBase) {
        //                tableBase = new EndgameTable();
        //                tableBase->read();
        //            }
        //            Line line = tableBase->probe(&board, side);
        //            if ((line.getBestMove() > 0) && (line.getDepth() > 0)) {
        //                if (options[OPTION_VERBOSE]) {
        //                    std::cout << line.toString() << std::endl;
        //                }
        //                return line;
        //            }
        //        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Opening  Book">

    /**********************
     *  Try Opening Book  *
     **********************/

    if (options[Option::OPENING_BOOK] && openingBook) {
        Optional<Move> move = openingBook->probe(board);
        if (move) {
            Line line;
            std::vector<Move> moves;
            moves.push_back(move.value());
            line = Line(moves, 0, moves.size(), 0, 0);
            thinking = false;
            return line;
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Sanity Check">
    /******************************************
     * Sanity Check - if only 1 move, play it *
     ******************************************/
    MoveList moves = board.generatePseudoMoves();
    int count = 0;
    int only = 0;
    for (int i = 0; Move move = moves[i]; i++) {
        if (board.makeMove(move)) {
            only = move;
            count++;
        }
        board.undoMove(move);
        if (count > 1) {
            break;
        }
    }
    if (count == 1) {
        std::vector<Move> temp;
        temp.push_back(only);
        Line single = Line(temp, 0, 1, 1, 0);
        return single;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Iterative Deepening">
    /**********************
     * Iterative deepening*
     **********************/
    for (depth = 1; (depth <= MIN_DEPTH) || ((abs(score) < MATE_VALUE) && (mtime < timeAllowed) && (depth <= MAX_DEPTH)); ++depth) {
        score = alphaBeta(depth, alpha, beta, false);
        if (isInterrupted()) {
            break;
        }
        useLastMove = true;
        if (score <= alpha || score >= beta) {
            //Fell out of aspiration window => redo search
            alpha = -INFTY;
            beta = INFTY;
            depth--;
        } else {
            mtime = Utils::getCurrentTime() - searchStarted;
            std::vector<Move> moves = table->extractPrincipalVariation(board, depth);
            if (ans.getDepth() > 0) {
                previousLine = ans;
            }

            ans = Line(moves, score, depth, nodes, mtime);
            if (ans.empty()) {
                //can happen that entries are overwritten in TT? so use globalBestMove
                //instead
                if (globalBestMove != NULL_MOVE) {
                    ans.addMove(globalBestMove);
                } else {
                    std::cerr << "Uh-oh. Line is empty and best move is null! :(" << std::endl;
                }
            }
            if (options[Option::VERBOSE]) {
                std::cout << ans << std::endl;
            }
            /**********************
             * Aspiration Windows *
             **********************/
            alpha = score - ASPIRATION_WINDOW_SIZE;
            beta = score + ASPIRATION_WINDOW_SIZE;
        }
    }
    // </editor-fold>

    thinking = false;
    return ans;
}

//TODO WHAT ABOUT CHECK AVOIDANCE / CHECKMATE ?????

int Sphinx::quiescenceSearch(int alpha, int beta) {
    nodes++;
    //    if (maxDepth == 0) {
    //        return evaluatePosition();
    //    }
    //    Move *moves = board.generateNoisyPseudoMoves();
    //    int eval = evaluatePosition();
    //    if (eval >= beta) {
    //        delete[]moves;
    //        return beta;
    //    }
    //    alpha = max(eval, alpha);
    //    for (int i = 0; Move move = moves[i]; i++) {
    //        if (board.makeMove(move)) {
    //            eval = -quiescenceSearch(-beta, -alpha, maxDepth - 1);
    //            board.undoMove(move);
    //            if (eval > alpha) {
    //                alpha = eval;
    //                if (eval >= beta) {
    //                    delete[]moves;
    //                    return beta;
    //                }
    //            }
    //        } else {
    //            board.undoMove(move);
    //        }
    //    }
    //    delete[] moves;
    //    return alpha;
    MoveList moves = getNoisyMoves();
    int eval;
    if (moves.empty()) {
        eval = completionCheck(0); //checkmate in quiescence
    } else {
        eval = evaluatePosition();
    }
    if (eval >= beta) {
        return beta;
    }
    if (eval > alpha) {
        alpha = eval;
    }

    for (int i = 0; Move move = moves[i]; ++i) {
        if (move != NULL_MOVE && abs(board.getCapturedPiece(move)) != KING) {
            if (board.makeMove(move)) {
                eval = -quiescenceSearch(-beta, -alpha);
                board.undoMove(move);
                if (eval >= beta) {
                    return beta;
                }
                if (eval > alpha) {
                    alpha = eval;
                }
            } else {
                board.undoMove(move);
            }
        }
    }
    return alpha;
}

//TODO must improve noisy moves (own move gen function))

MoveList Sphinx::getNoisyMoves() {
    MoveList moves = board.generatePseudoMoves();
    int i;
    int legalCount = 0;
    Move legal = -1;
    for (i = 0; Move move = moves[i]; ++i) {
        if (legalCount < 2) {
            if (board.makeMove(move)) {
                ++legalCount;
                legal = move;
            }
            board.undoMove(move);
        }
        bool B = Utils::isDirectCheck(move);
        bool C = board.getCapturedPiece(move) == EMPTY_SQUARE;
        if (!B && C) {
            moves[i] = 0;
        }
    }

    switch (legalCount) {
        case 0:
            //no legal moves at all!
            return MoveList();

        case 1:

            //one legal move (treat as noisy)
            moves[0] = legal;
            moves[1] = NULL_MOVE;
            return moves;
        default:
            quicksort(moves, 0, i);
            return moves;
    }

    //    if (legalCount == 1) {
    //        moves[0] = legal;
    //    } else if (legalCount == 0) {
    //        moves[0] = NULL_MOVE;
    //    }
    //
    //    //    std::sort(moves.begin(), moves.begin() + i, [this] (Move a, Move b) -> bool {
    //    //        return this->compare(a, b);
    //    //    });
    //    quicksort(moves, 0, i);
    //    return moves;
}

//int Sphinx::quiescenceSearch(int alpha, int beta) {
//    nodes++;
//    int eval = evaluatePosition();
//    if (eval >= beta) {
//        return beta;
//    }
//
//    /************************************************************
//     * Big delta pruning                                        *
//     * See http://chessprogramming.wikispaces.com/Delta+Pruning *                                       *
//     ************************************************************/
//    int bigDelta = PIECE_VALUE[QUEEN];
//    static const int MARGIN = 2 * PIECE_VALUE[PAWN];
//    //If possible promotion
//    if ((board.getSide() == WHITE) ? MASK_RANK_7 & board.getWhitePawns() :
//            MASK_RANK_2 & board.getBlackPawns()) {
//        bigDelta += bigDelta - MARGIN;
//    }
//    if (eval + bigDelta < alpha) {
//        return alpha; //prune
//    }
//
//    alpha = std::max(eval, alpha);
//
//    Move* moves = board.generateNoisyPseudoMoves();
//
//    for (int i = 0; Move move = moves[i]; i++) {
//        if (abs(board.getCapturedPiece(move)) == KING) {
//            delete[] moves;
//            return INFTY;
//        }
//        /***************************************************
//         * Real delta pruning - not to be used in endgames *
//         * or when promoting                               *
//         ***************************************************/
//        int cp = board.getCapturedPiece(move);
//        if (eval + PIECE_VALUE[cp] + MARGIN < alpha &&
//                getFlags(move) < MOVE_PROMOTION_KNIGHT &&
//                getMaterial(board.getSide()*-1) - cp > 1300) {
//            //not sure about this getMaterial malarkey
//            continue;
//        }
//        /***************
//         * SEE Pruning *
//         ***************/
//        if ((cp) && (board.staticExchangeEvaluation(move) < 0)) {
//            continue;
//        }
//        if (board.makeMove(move)) {
//            eval = -quiescenceSearch(-beta, -alpha);
//            board.undoMove(move);
//            if (eval >= beta) {
//                delete[]moves;
//                return beta;
//            }
//            if (eval > alpha) {
//                alpha = eval;
//            }
//        } else {
//            board.undoMove(move);
//        }
//
//    }
//    delete[]moves;
//    return alpha;
//}

// <editor-fold defaultstate="collapsed" desc="Quicksort for noisy moves">

bool Sphinx::compare(Move a, Move b) {
    if (b == 0) {
        return true;
    }
    if (a == 0) {
        return false;
    }
    bool aCheck = Utils::isDirectCheck(a);
    bool bCheck = Utils::isDirectCheck(b);

    if (aCheck && (!bCheck)) {
        return true;
    }
    if ((!aCheck) && bCheck) {
        return false;
    }

    int aCp = abs(board.getCapturedPiece(a));
    int bCp = abs(board.getCapturedPiece(b));

    if (aCp > bCp) {
        return true;
    }
    if (aCp < bCp) {
        return false;
    }
    int aMp = abs(board.getMovingPiece(a));
    int bMp = abs(board.getMovingPiece(b));

    if (aMp > bMp) {
        return true;
    }
    if (aMp < bMp) {

        return false;
    }
    return true;
}

int Sphinx::partition(MoveList& A, int p, int r) {
    Move x = A[r];
    int i = p - 1;
    for (int j = p; j < r; ++j) {
        if (compare(A[j], x)) {
            i++;
            Move temp = A[i];
            A[i] = A[j];
            A[j] = temp;
        }
    }
    Move temp = A[i + 1];
    A[i + 1] = A[r];
    A[r] = temp;

    return i + 1;
}

void Sphinx::quicksort(MoveList& A, int p, int r) {
    if (p < r) {

        int q = partition(A, p, r);
        quicksort(A, p, q - 1);
        quicksort(A, q + 1, r);
    }
}

// </editor-fold>

StaggeredMoves<12> Sphinx::rankMoves(const MoveList& moves, int ply) const {


    StaggeredMoves<12> sorted;
    //    static const int N = 12;
    //    std::vector<Move> classes[N];
    //    for (int i = 0; i < N; i++) {
    //        classes[i].reserve(100);
    //    }
    static const int HISTORY = 0;
    static const int HASH_TABLE = 1;
    static const int PRIMARY_KILLERS = 2;
    static const int SECONDARY_KILLERS = 3;
    static const int PROMOTION = 4;
    static const int BIG_WIN = 5;
    static const int WIN = 6;
    static const int EVEN = 7;
    static const int CHECK = 8;
    static const int REST = 9;
    static const int LOSS = 10;
    static const int BIG_LOSS = 11;

    //    std::vector<int> history;
    //    std::vector<int> tableMoves;
    //    std::vector<int> primaryKillers;
    //    std::vector<int> secondaryKillers;
    //    std::vector<int> promotion;
    //    std::vector<int> bigWinningCaptures; //>200
    //    std::vector<int> winningCaptures;
    //    std::vector<int> evenCaptures;
    //    std::vector<int> check;
    //    std::vector<int> rest;  
    //    std::vector<int> losingCaptures;
    //    std::vector<int> bigLosingCaptures; //<-200

    int last = -1;
    if (useLastMove && (ply > 1) && (previousLine.getDepth() > 0)) {

        if (static_cast<int> (previousLine.getMoves().size()) > depth - ply) {
            last = previousLine.getMoves()[depth - ply];
        }
    }

    int i;

    for (i = 0; Move move = moves[i]; i++) {
        bool added = false;
        Optional<HashEntry> entry = table->get(board.getHash());
        if (move == last) {
            sorted.add(move, HISTORY);
            added = true;
        } else if ((entry) && (entry->move) && (move == entry->move)) {
            sorted.add(move, HASH_TABLE);
            added = true;
        } else if (killerHeuristic.getSize(ply) > 0) {
            if (move == killerHeuristic.getMove(ply, 0)) {
                sorted.add(move, PRIMARY_KILLERS);
                added = true;
            } else if ((killerHeuristic.getSize(ply) > 1) &&
                    (move == killerHeuristic.getMove(ply, 1))) {
                sorted.add(move, SECONDARY_KILLERS);
                added = true;
            }
        }
        if (!added) {
            bool c = Utils::isDirectCheck(move);
            int type = Utils::getFlags(move);
            switch (Utils::getFlags(type)) {

                case MOVE_SHORT_CASTLE:
                case MOVE_LONG_CASTLE:
                    sorted.add(move, REST);
                    break;
                case MOVE_NORMAL:
                    if (board.getCapturedPiece(move) != EMPTY_SQUARE) {

                        /**********************************************
                         * STATIC EXCHANGE EVALUATION - Move Ordering *
                         **********************************************/
                        int score = BoardEvaulator::staticExchangeEvaluation(board, move);
                        switch (Utils::sign(score)) {
                            case -1:
                                if (score < -200) {
                                    sorted.add(move, BIG_LOSS);
                                } else {
                                    sorted.add(move, LOSS);
                                }
                                break;
                            case 1:
                                if (score > 200) {
                                    sorted.add(move, BIG_WIN);
                                } else {
                                    sorted.add(move, WIN);
                                }
                                break;
                            default:
                                sorted.add(move, EVEN);
                                break;
                        }
                    } else if (c) {
                        sorted.add(move, CHECK);

                    } else {
                        sorted.add(move, REST);
                    }
                    break;
                case MOVE_CAPTURE_EN_PASSANT:
                    sorted.add(move, EVEN);
                    break;
                case MOVE_PROMOTION_KNIGHT:
                case MOVE_PROMOTION_BISHOP:
                case MOVE_PROMOTION_ROOK:
                case MOVE_PROMOTION_QUEEN:
                    sorted.add(move, PROMOTION);

                    break;
            }
        }
    }
    return sorted;
}

bool Sphinx::canReduceMove(int ply, int moveNum, Move move) {
    if (moveNum >= 4 && ply >= 3) {
        if (board.getLastCapturedPiece() != EMPTY_SQUARE) {
            return false; //don't reduce captures
        }
        if (Utils::getFlags(move) >= MOVE_PROMOTION_KNIGHT) {
            return false; //don't reduce promotions
        }
        //don't reduce checking moves
        if (Utils::isDirectCheck(move)) {
            return false; //don't reduce checking moves
        }
        board.makeMove(move);
        bool causesCheck = board.isInCheck();
        board.undoMove(move);
        return !causesCheck;
    }
    return false;
}


