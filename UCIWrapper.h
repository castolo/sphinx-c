/* 
 * File:   UCIWrapper.h
 * Author: Steve James <SD.James@outlook.com>
 *
 * This class is responsible for implementing the UCI protocol, which relieves
 * the engine of the responsibility.
 * 
 * Created on 23 December 2014, 4:41 PM
 */

#ifndef UCIWRAPPER_H
#define	UCIWRAPPER_H

#include <iostream>
#include <memory>

#include "AlternateAPI.h"

/**
 * This class is responsible for implementing the UCI protocol, which relieves
 * the engine of the responsibility.
 */
class UCIWrapper {
public:

    /**
     * Creates the interface to the UCI GUI program.
     * 
     * @param inputStream the stream from which the player should read commands 
     */
    explicit UCIWrapper(std::istream& inputStream = std::cin) : inputStream(inputStream) {
        createEngine();
        /**
         * Setup IO Streams - recommended to be unbuffered. See:
         * http://www.gnu.org/software/xboard/engine-intf.html for motivation.
         */
        std::cout.setf(std::ios::unitbuf);
        this->inputStream.rdbuf()->pubsetbuf(NULL, 0);
    }

    /**
     * Starts the player who enters a UCI loop, listening for commands and 
     * returning accordingly.
     */
    void start();

private:

    /**
     * Parses a UCI command, returning the command id and setting the instruction
     * and arguments, if any.
     * @param command the command received
     * @param instruction the actual instruction to perform
     * @param args the arguments of the command, if any
     * @return whether the command was successfully parsed
     */
    bool parseCommand(std::string command, std::string& instruction, std::vector<std::string>& args) const;

    /**
     * Show the options supported by the engine
     */
    inline void showOptions() const {
        std::cout << "option name OwnBook type check default true" << std::endl;
        std::cout << "option name BookFile type string default performance.bin" << std::endl;
    }

    /**
     * Identifies the engine
     */
    inline void identify() const {
        std::cout << "id name " << engine->getName() << ' ' << engine->getVersion() << std::endl;
        std::cout << "id author " << engine->getAuthor() << std::endl;
        //options go here
        showOptions();
    }

    /**
     * Creates the engine 
     */
    inline void createEngine() {
        //TODO need to check that this is right.
        engine.reset();
        engine = std::make_unique<Sphinx>();
    }

    /*
     * An enum specifying the different commands that the player supports
     */
    enum class UCI {
        UCI_SUPPORT, READY, SET_OPTION, NEW_GAME, POSITION, GO, STOP, PONDER_HIT,
        QUIT,
        //non-standard uci follows
        NS_DIVIDE, NS_PERFT, NS_PERFT_SPEED, NS_SHOW_BOARD, NS_SHOW_BOOK, NS_QSEARCH
    };

    enum class Mode {
        WAITING, THINKING, DEBUGGING, QUITTING,
    };

    /*
     * A mapping from string names to GTP commands
     */
    const std::unordered_map<std::string, UCI> commands = {
        {"uci", UCI::UCI_SUPPORT},
        {"isready", UCI::READY},
        {"setoption", UCI::SET_OPTION},
        {"ucinewgame", UCI::NEW_GAME},
        {"position", UCI::POSITION},
        {"go", UCI::GO},
        {"stop", UCI::STOP},
        {"ponderhit", UCI::PONDER_HIT},
        {"quit", UCI::QUIT},
        {"divide", UCI::NS_DIVIDE},
        {"perft", UCI::NS_PERFT},
        {"perftspeed", UCI::NS_PERFT_SPEED},
        {"showboard", UCI::NS_SHOW_BOARD},
        {"showbook", UCI::NS_SHOW_BOOK},
        {"printbook", UCI::NS_SHOW_BOOK},
        {"qsearch", UCI::NS_QSEARCH},

    };

    Mode mode = Mode::WAITING;
    std::istream& inputStream;
    Board board = Board();
    std::unique_ptr<Sphinx> engine;

};

#endif	/* UCIWRAPPER_H */

