/* 
 * File:   TranspositionTable.h
 * Author: Steve James
 *
 * Created on 19 April 2013, 11:19 AM
 * 
 * Copyright © Steve James <SD.James@outlook.com>
 * Unauthorized copying of this file, via any medium, is strictly prohibited
 * without the express permission of the author.
 * 
 */

#ifndef TRANSPOSITIONTABLE_H
#define	TRANSPOSITIONTABLE_H

#include <stdint.h>
#include <vector>
#include <cstddef>
#include <math.h>
#include <stdlib.h>
#include <set>
#include "Board.h"
#include "Optional.h" 
#include "LinkedHashMap.h"

// <editor-fold defaultstate="collapsed" desc="TT Entry">

struct HashEntry {
    Move move = 0;
    int eval = 0;
    int depth = 0;
    EvalType flag = EvalType::NONE;
    int ancient = 0;

    HashEntry(Move m, int e, int d, EvalType f, int a) : move(m), eval(e),
    depth(d), flag(f), ancient(a) {
    }


};
// </editor-fold>

namespace {
    using LHM = LinkedHashMap<Hash, Optional<HashEntry>>;
}

class TranspositionTable : protected LHM {
public:

    /**
     * The maximum number of elements in allowed in the table
     */
    static const int DEFAULT_SIZE = 8388608;

    TranspositionTable(int maxNumElements = DEFAULT_SIZE) : LHM(maxNumElements) {
    }

    TranspositionTable(Byte maxBytes) : LHM(maxBytes) {
    }

    
    
    inline Optional<HashEntry> get(Hash hash) const override {
        return getOrDefault(hash, std::experimental::nullopt);
    }

    inline Optional<HashEntry> getFromBoard(Board & board) const {
        Optional<HashEntry> e = getOrDefault(board.getHash(), std::experimental::nullopt);
        if (e) {
            bool legal = false;
            if (e->move) {
                legal = board.makeMove(e->move);
                board.undoMove(e->move);
            }
            if (!legal) {
                return std::experimental::nullopt;
            }
        }
        return e;
    }

    /**
     * Maps the specified hash to the specified values in the table, provided
     * that certain conditions are met. A depth-first replacement scheme is used
     * here. This means that the entry is added to the table in any of the
     * following cases: <ul><li>There is currently no mapping for the give
     * hash</li><li> The depth to which the position has been analysed is
     * greater or equal to the current element's depth</li> <li>The current
     * element's {@code ancient} variable has rendered it obsolete</li></ul>
     *
     * @param hash the board's position
     * @param move the move to be played in said position
     * @param eval the score assigned to the move
     * @param depth the depth to which the position was analysed
     * @param flag the type of node to which the score corresponds (see {@link Constants#EVALTYPE_EXACT},
     * {@link Constants#EVALTYPE_LOWERBOUND} and
     * {@link Constants#EVALTYPE_UPPERBOUND})
     * @param ancient the iteration at which the entry is being added to the
     * table
     */
    inline void put(Hash hash, Move move, int eval, int depth, EvalType flag, int ancient) {
        depth = (depth < 0) ? 0 : depth;
        Optional<HashEntry> newEntry = std::experimental::make_optional(HashEntry{move,
            eval, depth, flag, ancient});


        if (!contains(hash)) {
            insert(hash, newEntry);
        } else {
            Optional<HashEntry> current = get(hash);
            if (shouldReplace(current.value(), newEntry.value())) {
                insert(hash, newEntry);
            }
        }
    }

    /**
     * Extracts the principal variation from the transposition table. Note that the PV may be
     * incomplete owing to hash table overwrites. In this case, a PV with smaller size than the prescribed 
     * depth may be returned. The PV will however contain at least one move - the first move.
     * @param board the board
     * @param depth the size of the expected principal variation
     * @return the moves that constitute the principal variation
     */
    std::vector<Move> extractPrincipalVariation(Board&, int);


private:

    /**
     * Replacement policy (TODO: aging properly)
     * @param current the current TT entry
     * @param newEntry the new entry
     * @return whether the new entry should replace the current one
     */
    inline bool shouldReplace(const HashEntry& current, const HashEntry & newEntry) const {
        //replace by depth
        if (current.depth <= newEntry.depth) {
            return true;
        }

        //replace by age (TODO: fix this)
        if (std::abs(current.ancient - newEntry.ancient > 1)) {
            return true;
        }

        //current entry was just storing flag/bound data, not actual move info
        if (current.move == 0 && newEntry.move > 0) {
            return true;
        }

        //if nothing is true, don't replace
        return false;
    }
};

#endif	/* TRANSPOSITIONTABLE_H */
