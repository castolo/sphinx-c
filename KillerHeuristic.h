/* 
 * File:   KillerHeuristic.h
 * Author: Steve James <SD.James@outlook.com>
 *
 * This class is used to store moves in two levels to be used for the Killer 
 * Heuristic. For more information about this heuristic, see
 * https://chessprogramming.wikispaces.com/Killer+Heuristic
 * 
 * Created on 11 April 2013, 8:38 AM
 * 
 */

#ifndef KILLERHEURISTIC_H
#define	KILLERHEURISTIC_H

#include "Constants.h"
#include "Move.h"
#include <vector>
#include <algorithm> 
#include <array>

/*
 * This class is used to store moves at a number of levels to be used for the Killer 
 * Heuristic. For more information about this heuristic, see
 * https://chessprogramming.wikispaces.com/Killer+Heuristic
 */
class KillerHeuristic {
public:

    /**
     * Adds a killer move to the data structure, inserting it into the correct
     * position in the list (as explained in the class description)
     *
     * @param ply the ply the move occurred at
     * @param move the killer move
     */
    void add(int ply, Move move);

    /**
     * Returns the number of killer moves that have been found at a particular
     * ply
     *
     * @param ply the ply to check for killer moves
     * @return the number of killer moves found
     */
    inline int getSize(int ply) const {
        return sizes[ply - 1];
    }

    /**
     * Returns all killer moves for a particular ply
     *
     * @param ply the ply at which the killer moves occurred
     * @return a list of killer moves at a particular ply
     */
    std::vector<Move> getMovesAt(int ply);

    /**
     * Returns a killer move for a particular ply, at a particular position in
     * the list
     *
     * @param ply the ply at which the killer move occurred
     * @param rank the position of the move in the list
     * @return a killer move at a particular ply and list position
     */
    inline Move getMove(int ply, int rank) const {
        return (ply < 1) ? 0 : map[ply - 1][rank];
    }

    /**
     * Clears the entire data structure
     */
    inline void clear() {
        std::fill(sizes.begin(), sizes.end(), 0);
    }

private:

    std::array<std::array<Move, MAX_KILLERS>, DEFAULT_MAX_DEPTH> map; //[DEFAULT_MAX_DEPTH][MAX_KILLERS];
    std::array<std::array<int, MAX_KILLERS>, DEFAULT_MAX_DEPTH> ranks; //[DEFAULT_MAX_DEPTH][MAX_KILLERS];
    std::array<int, DEFAULT_MAX_DEPTH> sizes = {{0}};

    /**
     * Perform an insertion sort at the given ply
     * @param ply the level to sort
     */
    void sort(int ply);
};

#endif	/* KILLERHEURISTIC_H */

