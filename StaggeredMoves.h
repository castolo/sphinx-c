/* 
 * \class StaggeredMoves
 * 
 * File:   StaggeredMoves.h
 * Author: Steve
 * Created on 11 January 2014, 8:50 PM
 *
 * \brief This data structure is designed to store moves on
 *  a number of levels.
 * 
 * The number of levels is specified by the 
 * template parameter <code>N</code>. Moves of greater 
 * importance should be stored at lower levels than those
 * of lesser importance. 
 * 
 * <b> Note that once a move has been inserted, it cannot be
 *  modified or erased.</b>
 * 
 * The main purpose of the data structure is to insert moves
 * at different levels while providing a clean interface for
 * accessing each move in its correct order. Moves are 
 * accessed in the following order. Moves stored at level 0 
 * are accessed in a first-in first-out manner, followed by
 * moves stored at level 1 (again in a FIFO manner) and so
 * on, until the final level. Empty levels are disregarded 
 * when iterating through the data structure. 
 * 
 * Iterating through the data structure must be done in the 
 * following manner:
 *  <pre>
 * {@code
 * StaggeredMoves<3> moves;
 * moves.add(20, 2);
 * moves.add(10,0);
 * for (Move move:moves){{
 *  //Do something with move e.g. board.makeMove(move);
 * cout<<move<<endl;
 * }
 * }
 * </pre> 
 *  
 * The output of this code is:
 * <code>
 * 10
 * 20
 * <code>
 * 
 * Note that for pre-C++11 compilers, iterators should 
 * instead be used. 
 * 
 */

#ifndef StaggeredMoves_H
#define	StaggeredMoves_H

#include <exception>
#include <array>
#include "Move.h"

template<unsigned int N>
class StaggeredMoves {
private:

    std::array<std::array<Move, 256>, N> moves;
    std::array<int, N> sizes;
    unsigned int totalSize;
    unsigned int levels;

    // <editor-fold defaultstate="collapsed" desc="Iterator">   

    class Iterator {
        const StaggeredMoves<N>* sm;
        int index;
        unsigned int level;
    public:

        Iterator(const StaggeredMoves<N>* StaggeredMoves, int idx, int lev) : sm(StaggeredMoves), index(idx), level(lev) {
        }

        inline Move operator*() {
            return sm->moves[level][index];

        }

        inline Iterator& operator++() {
            if (++index == sm->sizes[level]) {
                index = 0;
                ++level;
                while (level < sm->levels && !sm->sizes[level]) {
                    ++level;
                }
            }
            return *this;
        }

        inline bool operator!=(const Iterator& it) const {
            return index != it.index || level != it.level;
        }
    };
    // </editor-fold>

public:

    /**
     * Creates an empty data structure for storing moves in different levels
     */
    StaggeredMoves() : sizes {0}, totalSize(0), levels(N) {
    }

    /**
     * Adds a move to the structure at a specified level
     * @param move the move
     * @param level the level
     */
    inline void add(Move move, int level) {
        moves[level][sizes[level]++] = move;
        ++totalSize;
    }

    /**
     * Clears the data structure
     */
    inline void clear() {
        for (int i = 0; i < sizes.size(); i++) {
            sizes[i] = 0;
        }
        totalSize = 0;
    }

    /**
     * Returns the total number of moves held by the structure
     */
    inline unsigned int size() {
        return totalSize;
    }

    /**
     * Returns an iterator that points to the first move held in the structure
     */
    inline Iterator begin() const {
        unsigned int level = 0;
        while (level < levels && !sizes[level]) {
            ++level;
        }
        return Iterator(this, 0, level);
    }

    /**
     * Returns an iterator that points to the position immediately after the last move
     * held in the structure
     */
    inline Iterator end() const {
        return Iterator(this, 0, levels);
    }

    inline Move at(int idx) const throw (std::out_of_range) {
        if (idx >= totalSize) {
            throw std::out_of_range("Accessing invalid move in structure");
        }
        return (*this)[idx];
    }

    inline Move operator[](int idx) const noexcept {
        int level = 0;
        while (sizes[level] <= idx) {
            idx -= sizes[level];
            ++level;
        }
        return moves[level][idx];
    }

};

typedef StaggeredMoves<8> MOVES;

#endif	/* StaggeredMoves_H */

