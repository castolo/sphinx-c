#include "HashBoard.h"
#include "Utils.h"

HashBoard::HashBoard() : hash(0) {
    sideHash = Utils::random<Hash>();
    for (int i = 0; i < 4; ++i) {
        whiteCastleHash[i] = Utils::random<Hash>();
        blackCastleHash[i] = Utils::random<Hash>();
    }
    for (int i = 0; i < 64; ++i) {
        enPassantHash[i] = Utils::random<Hash>();
        for (int j = 0; j < 6; ++j) {
            piecesHash[j][0][i] = Utils::random<Hash>();
            piecesHash[j][1][i] = Utils::random<Hash>();
        }
    }
}
