/* 
 * File:   BoardEvaluator.cpp
 * Author: Workstation
 * 
 * Created on 20 December 2014, 9:03 PM
 */

#include "BoardEvaluator.h"
#include <functional>

// <editor-fold defaultstate="collapsed" desc="SEE Helpers">
//For SEE
Bitboard BoardEvaulator::attacksTo(const Board& board, int square) {
    Bitboard attacks = BLACK_PAWN_ATTACKS[square] & board.WHITE_PAWN_BOARD;
    attacks |= WHITE_PAWN_ATTACKS[square] & board.BLACK_PAWN_BOARD;
    attacks |= OCCUPANCY_MASK_KNIGHT[square] & board.WHITE_KNIGHT_BOARD;
    attacks |= OCCUPANCY_MASK_KNIGHT[square] & board.BLACK_KNIGHT_BOARD;
    Bitboard whiteMagicBishopMoves = board.getBishopMoves(square, board.WHITE_PIECES_BOARD);
    Bitboard blackMagicBishopMoves = board.getBishopMoves(square, board.BLACK_PIECES_BOARD);
    Bitboard whiteMagicRookMoves = board.getRookMoves(square, board.WHITE_PIECES_BOARD);
    Bitboard blackMagicRookMoves = board.getRookMoves(square, board.BLACK_PIECES_BOARD);
    attacks |= blackMagicBishopMoves & (board.WHITE_BISHOP_BOARD | board.WHITE_QUEEN_BOARD);
    attacks |= whiteMagicBishopMoves & (board.BLACK_BISHOP_BOARD | board.BLACK_QUEEN_BOARD);
    attacks |= blackMagicRookMoves & (board.WHITE_ROOK_BOARD | board.WHITE_QUEEN_BOARD);
    attacks |= whiteMagicRookMoves & (board.BLACK_ROOK_BOARD | board.BLACK_QUEEN_BOARD);
    attacks |= OCCUPANCY_MASK_KING[square] & board.WHITE_KING_BOARD;
    attacks |= OCCUPANCY_MASK_KING[square] & board.BLACK_KING_BOARD;
    return attacks;
}

// </editor-fold>

Evaluation BoardEvaulator::staticExchangeEvaluation(const Board& board, Move move) {
    int gain[32];
    int depth = 0;
    int source = Utils::getSource(move);
    int dest = Utils::getDestination(move);

    Bitboard destBB = bitboard::getBitBoard(63 - dest);

    int target = board.getCapturedPiece(move);
    int piece = board[source];
    int mSide = Utils::sign(piece);
    piece = abs(piece);
    Bitboard attackers = attacksTo(board, dest);
    Bitboard occupancy = board.ALL_PIECES_BOARD;
    Bitboard fromSet = bitboard::getBitBoard(63 - source);
    Bitboard hiders = board.WHITE_PAWN_BOARD | board.BLACK_PAWN_BOARD |
            board.WHITE_BISHOP_BOARD | board.BLACK_BISHOP_BOARD | board.WHITE_ROOK_BOARD |
            board.BLACK_ROOK_BOARD | board.WHITE_QUEEN_BOARD | board.BLACK_QUEEN_BOARD; //pieces may be hiding behind these
    Bitboard rookSliders = board.WHITE_ROOK_BOARD | board.WHITE_QUEEN_BOARD | board.BLACK_ROOK_BOARD |
            board.BLACK_QUEEN_BOARD;
    Bitboard bishopSliders = board.WHITE_BISHOP_BOARD | board.WHITE_QUEEN_BOARD | board.BLACK_BISHOP_BOARD |
            board.BLACK_QUEEN_BOARD;

    gain[depth] = DEFAULT_PIECE_VALUE[abs(target)];
    do {
        depth++;
        mSide *= -1; // next depth and side
        gain[depth] = DEFAULT_PIECE_VALUE[piece] - gain[depth - 1]; // speculative store, if defended
        if (std::max(-gain[depth - 1], gain[depth]) < 0) {
            break; // pruning does not influence the result
        }
        attackers ^= fromSet; // reset bit in set to traverse
        occupancy ^= fromSet; // reset bit in temporary occupancy (for x-Rays)
        if (fromSet & hiders) {
            rookSliders ^= fromSet;
            bishopSliders ^= fromSet;
            attackers |= considerXRays(destBB, fromSet,
                    occupancy, rookSliders, bishopSliders); //look for pieces hidden behind
        }
        fromSet = getLeastValuablePiece(board, attackers, mSide, piece);
    } while (fromSet);
    while (--depth) {
        gain[depth - 1] = -std::max(-gain[depth - 1], gain[depth]);
    }
    return gain[0];
}

Evaluation BoardEvaulator::evaluate(const Board& board) {
    if (board.fiftyRule >= 100 || !board.hasSufficientMaterial()) {
        return -DRAW_VALUE;
    }
    Evaluation eval = 0;
    /************************************
     * Evaluate material of either side * 
     ************************************/
    Evaluation whiteMaterial = 0;
    Evaluation blackMaterial = 0;
    for (int piece = PAWN; piece <= QUEEN; ++piece) {
        whiteMaterial += board.whitePieceListCount[piece] * DEFAULT_PIECE_VALUE[piece];
        blackMaterial += board.blackPieceListCount[piece] * DEFAULT_PIECE_VALUE[piece];
    }
    eval = board.side * (whiteMaterial - blackMaterial);
    eval += evaluatePawnStructure(board);
    eval += evaluateMobilityandKingSafety(board, whiteMaterial, blackMaterial);
        
    return eval;
}

Evaluation BoardEvaulator::evaluateMobilityandKingSafety(const Board& board,
        Evaluation whiteMaterial, Evaluation blackMaterial) {


    const int WHITE_IDX = 0;
    const int BLACK_IDX = 1;

    /*******************************************************************************
     * C++ Standard 8.3.2/4: There shall be no references to references, no arrays *
     * of references, and no pointers to references.                               *
     *******************************************************************************/    
    const std::reference_wrapper<const PieceListCount> pieceCount[2] = {
        std::ref(board.whitePieceListCount), std::ref(board.blackPieceListCount)
    };
     const std::reference_wrapper<const PieceList> pieces[2] = {
        std::ref(board.whitePieceList), std::ref(board.blackPieceList)
    };

    const Bitboard kingArea[] = {
        OCCUPANCY_MASK_KING[board.whiteKing], OCCUPANCY_MASK_KING[board.blackKing]
    };

    int attackers[] = {0, 0};
    int attackTot[] = {0, 0};

    Evaluation mobility[] = {0, 0};

    int count;
    double temp;

    for (int j = WHITE_IDX; j <= BLACK_IDX; ++j) {
        count = pieceCount[j].get()[KNIGHT];
        temp = 0;
        for (int i = 0; i < count;) {
            int idx = pieces[j].get()[KNIGHT][i++];
            temp += (DEFAULT_KNIGHT_MOBILITY * bitboard::getSparseBitCount(OCCUPANCY_MASK_KNIGHT[idx]));
            if (OCCUPANCY_MASK_KNIGHT[idx] & kingArea[j ^ 1]) {
                attackTot[j] += DEFAULT_KNIGHT_ATTACK_WEIGHT;
                attackers[j]++;
            }
        }
        mobility[j] += (count > 0) ? static_cast<int> (temp / count) : 0;
    }


    for (int j = WHITE_IDX; j <= BLACK_IDX; ++j) {
        count = pieceCount[j].get()[PAWN];
        temp = 0;
        for (int i = 0; i < count;) {
            int idx = pieces[j].get()[PAWN][i++];
            auto &moves = (j == WHITE_IDX) ? WHITE_PAWN_MOVES : BLACK_PAWN_MOVES;
            auto &attacks = (j == WHITE_IDX) ? WHITE_PAWN_ATTACKS : BLACK_PAWN_ATTACKS;
            temp += (DEFAULT_PAWN_MOBILITY * bitboard::getDenseBitCount(
                    (board.EMPTY_BOARD & moves[idx]) |
                    (board.ALL_PIECES_BOARD & attacks[idx])
                    ));
            //what about pawn attacking king area?
        }
        mobility[j] += (count > 0) ? static_cast<int> (temp / count) : 0;
    }

    for (int j = WHITE_IDX; j <= BLACK_IDX; ++j) {
        count = pieceCount[j].get()[ROOK];
        temp = 0;
        for (int i = 0; i < count;) {
            int idx = pieces[j].get()[ROOK][i++];
            Bitboard subset = (j == WHITE_IDX) ?
                    (board.WHITE_PAWN_BOARD) | (board.WHITE_KING_BOARD) :
                    (board.BLACK_PAWN_BOARD) | (board.BLACK_KING_BOARD);
            Bitboard att = board.getMagicRookMoves(idx, subset, subset); //TODO check
            //Bitboard att = getRookMoves(idx, FULL_BOARD);
            temp += (DEFAULT_ROOK_MOBILITY * bitboard::getDenseBitCount(att));
            if (att & kingArea[j ^ 1]) {
                attackTot[j] += DEFAULT_ROOK_ATTACK_WEIGHT;
                attackers[j]++;
            }
        }
        mobility[j] += (count > 0) ? static_cast<int> (temp / count) : 0;
    }

    for (int j = WHITE_IDX; j <= BLACK_IDX; ++j) {
        count = pieceCount[j].get()[BISHOP];
        temp = 0;
        for (int i = 0; i < count;) {
            int idx = pieces[j].get()[BISHOP][i++];

            Bitboard att = (j == WHITE_IDX) ?
                    board.getMagicBishopMoves(idx, board.WHITE_PAWN_BOARD, board.WHITE_PAWN_BOARD) :
                    board.getMagicBishopMoves(idx, board.BLACK_PAWN_BOARD, board.BLACK_PAWN_BOARD);

            temp += (DEFAULT_BISHOP_MOBILITY * bitboard::getDenseBitCount(att));
            if (att & kingArea[j^1]) {
                attackTot[j] += DEFAULT_BISHOP_ATTACK_WEIGHT;
                attackers[j]++;
            }
        }
        mobility[j] += (count > 0) ? static_cast<int> (temp / count) : 0;
    }

    for (int j = WHITE_IDX; j <= BLACK_IDX; ++j) {
        count = pieceCount[j].get()[QUEEN];
        temp = 0;
        for (int i = 0; i < count;) {
            int idx = pieces[j].get()[QUEEN][i++];

            Bitboard att = (j == WHITE_IDX) ?
                    board.getMagicBishopMoves(idx, board.WHITE_PAWN_BOARD, board.WHITE_PAWN_BOARD) |
                    board.getMagicRookMoves(idx, board.WHITE_PAWN_BOARD, board.WHITE_PAWN_BOARD) :
                    board.getMagicBishopMoves(idx, board.BLACK_PAWN_BOARD, board.BLACK_PAWN_BOARD) |
                    board.getMagicRookMoves(idx, board.BLACK_PAWN_BOARD, board.BLACK_PAWN_BOARD);

            temp += (DEFAULT_QUEEN_MOBILITY * bitboard::getDenseBitCount(att));
            if (att & kingArea[j^1]) {
                attackTot[j] += DEFAULT_QUEEN_ATTACK_WEIGHT;
                attackers[j]++;
            }
        }
        mobility[j] += (count > 0) ? static_cast<int> (temp / count) : 0;
    }


    bool wFlag = (board.BLACK_QUEEN_BOARD > 0) && ((board.BLACK_BISHOP_BOARD > 0) || (board.BLACK_ROOK_BOARD > 0) ||
            (board.BLACK_KNIGHT_BOARD > 0));
    bool bFlag = (board.WHITE_QUEEN_BOARD > 0) && ((board.WHITE_BISHOP_BOARD > 0) || (board.WHITE_ROOK_BOARD > 0) ||
            (board.WHITE_KNIGHT_BOARD > 0));
    int whiteKingSafety = 0;
    int blackKingSafety = 0;


    if (!wFlag) {
        whiteKingSafety = static_cast<int> (DEFAULT_KING_MOBILITY * bitboard::getSparseBitCount(OCCUPANCY_MASK_KING[board.whiteKing]));
    } else {
        whiteKingSafety = evaluateWhiteKingShield(board);
        whiteKingSafety -= scale(attackTot[BLACK_IDX], attackers[BLACK_IDX]);

        /* Scale the middlegame king evaluation against remaining enemy material */
        whiteKingSafety *= blackMaterial;
        whiteKingSafety /= DEFAULT_START_MATERIAL;
    }

    if (!bFlag) {
        blackKingSafety += static_cast<int> (DEFAULT_KING_MOBILITY * bitboard::getSparseBitCount(OCCUPANCY_MASK_KING[board.blackKing]));
    } else {

        blackKingSafety = evaluateBlackKingShield(board);
        blackKingSafety -= scale(attackTot[WHITE_IDX], attackers[WHITE_IDX]);

        /* Scale the middlegame king evaluation against remaining enemy material */
        blackKingSafety *= whiteMaterial;
        blackKingSafety /= DEFAULT_START_MATERIAL;

    }
    return board.side * ((whiteKingSafety - blackKingSafety)+(mobility[WHITE_IDX] - mobility[BLACK_IDX]));

}

Evaluation BoardEvaulator::evaluatePawnStructure(const Board& board) {

    int A = std::max(0, bitboard::getSparseBitCount(board.WHITE_PAWN_BOARD & MASK_FILE_A) - 1);
    int a = std::max(0, bitboard::getSparseBitCount(board.BLACK_PAWN_BOARD & MASK_FILE_A) - 1);
    int B = std::max(0, bitboard::getSparseBitCount(board.WHITE_PAWN_BOARD & MASK_FILE_B) - 1);
    int b = std::max(0, bitboard::getSparseBitCount(board.BLACK_PAWN_BOARD & MASK_FILE_B) - 1);
    int C = std::max(0, bitboard::getSparseBitCount(board.WHITE_PAWN_BOARD & MASK_FILE_C) - 1);
    int c = std::max(0, bitboard::getSparseBitCount(board.BLACK_PAWN_BOARD & MASK_FILE_C) - 1);
    int D = std::max(0, bitboard::getSparseBitCount(board.WHITE_PAWN_BOARD & MASK_FILE_D) - 1);
    int d = std::max(0, bitboard::getSparseBitCount(board.BLACK_PAWN_BOARD & MASK_FILE_D) - 1);
    int E = std::max(0, bitboard::getSparseBitCount(board.WHITE_PAWN_BOARD & MASK_FILE_E) - 1);
    int e = std::max(0, bitboard::getSparseBitCount(board.BLACK_PAWN_BOARD & MASK_FILE_E) - 1);
    int F = std::max(0, bitboard::getSparseBitCount(board.WHITE_PAWN_BOARD & MASK_FILE_F) - 1);
    int f = std::max(0, bitboard::getSparseBitCount(board.BLACK_PAWN_BOARD & MASK_FILE_F) - 1);
    int G = std::max(0, bitboard::getSparseBitCount(board.WHITE_PAWN_BOARD & MASK_FILE_G) - 1);
    int g = std::max(0, bitboard::getSparseBitCount(board.BLACK_PAWN_BOARD & MASK_FILE_G) - 1);
    int H = std::max(0, bitboard::getSparseBitCount(board.WHITE_PAWN_BOARD & MASK_FILE_H) - 1);
    int h = std::max(0, bitboard::getSparseBitCount(board.BLACK_PAWN_BOARD & MASK_FILE_H) - 1);

    int doubled = DEFAULT_DOUBLED_PAWN_PENALTY * ((A - a) + (B - b) + (C - c) + (D - d) + (E - e) + (F - f) + (G - g) + (H - h));

    int whitePassed = 0;
    int blackPassed = 0;

    int whiteIsolated = 0;
    int blackIsolated = 0;

    //TODO: must do pawn advancement
    int whiteAdvanceBonus = 0;
    int blackAdvanceBonus = 0;

    Bitboard bb = board.WHITE_PAWN_BOARD;
    while (bb > 0) {
        bool bonusGranted = false;
        int idx = 63 - bitboard::LS1B(bb);
        if ((FILE_NEIGHBOUR[idx] & board.WHITE_PAWN_BOARD) == 0) {
            whiteIsolated++;
            if (idx < 24) { //if on 6th or 7th rank
                bonusGranted = true;
                int rate = 3 - idx / 8; //2 for rank 7, 1 for rank 6
                whiteAdvanceBonus += rate * DEFAULT_ISOLATED_ADVANCED_PAWN_WEIGHT * DEFAULT_PAWN_VALUE;
            }

        }

        if ((WHITE_FRONT_SPANS[idx] & board.BLACK_PAWN_BOARD) == 0) {
            whitePassed++;
            if (idx < 24 && !bonusGranted) { //if on 6th or 7th rank
                int rate = 3 - idx / 8; //2 for rank 7, 1 for rank 6
                whiteAdvanceBonus += rate * DEFAULT_SUPPORTED_ADVANCED_PAWN_WEIGHT * DEFAULT_PAWN_VALUE;
            }
        }

        bb &= bb - 1;
    }
    bb = board.BLACK_PAWN_BOARD;
    while (bb > 0) {
        bool bonusGranted = false;
        int idx = 63 - bitboard::LS1B(bb);
        if ((FILE_NEIGHBOUR[idx] & board.BLACK_PAWN_BOARD) == 0) {
            blackIsolated++;
            if (idx >= 40) { //if on 6th or 7th rank
                bonusGranted = true;
                int rate = idx / 8 - 4; //2 for rank 7, 1 for rank 6
                blackAdvanceBonus += rate * DEFAULT_ISOLATED_ADVANCED_PAWN_WEIGHT * DEFAULT_PAWN_VALUE;
            }
        }
        if ((BLACK_FRONT_SPANS[idx] & board.WHITE_PAWN_BOARD) == 0) {
            blackPassed++;
            if (idx >= 40 && !bonusGranted) { //if on 6th or 7th rank
                int rate = idx / 8 - 4; //2 for rank 7, 1 for rank 6
                blackAdvanceBonus += rate * DEFAULT_SUPPORTED_ADVANCED_PAWN_WEIGHT * DEFAULT_PAWN_VALUE;
            }
        }
        bb &= bb - 1;
    }
    int isolated = DEFAULT_ISOLATED_PAWN_PENALTY * (whiteIsolated - blackIsolated);
    int passed = DEFAULT_PASSED_PAWN_REWARD * (whitePassed - blackPassed);
    int bonus = whiteAdvanceBonus - blackAdvanceBonus;

    return board.side * (doubled + isolated + passed + bonus);

}

Evaluation BoardEvaulator::evaluateWhiteKingShield(const Board& board) {

    int result = 0;

    Bitboard mask = ~MASK_RANKS[board.whiteKing]; // nothing on king's rank

    //If king is kingside
    if (board.WHITE_KING_BOARD & (KINGSIDE_MASK & CLEAR_FILE_F)) {
        mask &= KINGSIDE_MASK;

        Bitboard perfect = (WPS_PERFECT << (63 - board.whiteKing)) & mask;
        Bitboard good = (WPS_GOOD_KINGSIDE << (63 - board.whiteKing)) & mask;
        Bitboard ok = (WPS_OK << (63 - board.whiteKing)) & mask;
        Bitboard poor = (WPS_GOOD_QUEENSIDE << (63 - board.whiteKing)) & mask;

        if (perfect == (perfect & board.WHITE_PAWN_BOARD)) {
            return result; //perfect pawn shield --> no penalty
        }

        if (good == (good & board.WHITE_PAWN_BOARD)) {
            return result; //good pawn shield --> no penalty
        }

        if (ok == (ok & board.WHITE_PAWN_BOARD)) {
            return result; //ok pawn shield --> no penalty
        }

        if (poor == (poor & board.WHITE_PAWN_BOARD)) {
            return DEFAULT_POOR_PAWN_SHIELD_PENALTY; //poor pawn shield
        }
        result += DEFAULT_PAWN_SHIELD_PENALTY_1;
        int closePawns = bitboard::getSparseBitCount(board.WHITE_PAWN_BOARD & OCCUPANCY_MASK_KING[board.whiteKing]);
        result += std::min(0, (2 - closePawns) * DEFAULT_PAWN_SHIELD_PENALTY_2);
        int times = 0;
        if ((MASK_FILES[board.whiteKing] & board.WHITE_PAWN_BOARD) == 0) {
            result += DEFAULT_PAWN_SHIELD_PENALTY_3;
            times++;
        }
        if ((board.whiteKing + 1) % 8 != 0) {
            if ((MASK_FILES[board.whiteKing + 1] & board.WHITE_PAWN_BOARD) == 0) {
                result += DEFAULT_PAWN_SHIELD_PENALTY_4;
                result += DEFAULT_PAWN_SHIELD_PENALTY_4 * times;
            }
        }
        if ((MASK_FILES[board.whiteKing - 1] & board.WHITE_PAWN_BOARD) == 0) {
            result += DEFAULT_PAWN_SHIELD_PENALTY_5;
            result += DEFAULT_PAWN_SHIELD_PENALTY_5 * times;
            times++;
        }

    }//if king is queenside
    else if (board.WHITE_KING_BOARD & QUEENSIDE_MASK) {

        mask &= QUEENSIDE_MASK;
        int idx = board.whiteKing;
        if (board.WHITE_KING_BOARD & MASK_FILE_C) {
            idx--;
        }
        Bitboard perfect = (WPS_PERFECT << (63 - idx)) & mask;
        Bitboard good = (WPS_GOOD_QUEENSIDE << (63 - idx)) & mask;
        Bitboard ok = (WPS_OK << (63 - idx)) & mask;
        Bitboard poor = (WPS_GOOD_KINGSIDE << (63 - idx)) & mask;

        if (perfect == (perfect & board.WHITE_PAWN_BOARD)) {
            return result; //perfect pawn shield --> no penalty
        }

        if (good == (good & board.WHITE_PAWN_BOARD)) {
            return result; //good pawn shield --> no penalty
        }

        if (ok == (ok & board.WHITE_PAWN_BOARD)) {
            return result; //ok pawn shield --> no penalty
        }

        if (poor == (poor & board.WHITE_PAWN_BOARD)) {
            return DEFAULT_POOR_PAWN_SHIELD_PENALTY; //poor pawn shield
        }
        result += DEFAULT_PAWN_SHIELD_PENALTY_1;
        int closePawns = bitboard::getSparseBitCount(board.WHITE_PAWN_BOARD &
                (OCCUPANCY_MASK_KING[board.whiteKing] | OCCUPANCY_MASK_KING[idx]));
        result += std::min(0, (2 - closePawns) * DEFAULT_PAWN_SHIELD_PENALTY_2);
        int times = 0;
        if ((MASK_FILES[board.whiteKing] & board.WHITE_PAWN_BOARD) == 0) {
            result += DEFAULT_PAWN_SHIELD_PENALTY_3;
            times++;
        }
        if ((board.whiteKing) % 8 != 0) {
            if ((MASK_FILES[board.whiteKing - 1] & board.WHITE_PAWN_BOARD) == 0) {
                result += DEFAULT_PAWN_SHIELD_PENALTY_4;
                result += DEFAULT_PAWN_SHIELD_PENALTY_4 * times;
                times++;
            }
        }

        if ((MASK_FILES[board.whiteKing + 1] & board.WHITE_PAWN_BOARD) == 0) {
            result += DEFAULT_PAWN_SHIELD_PENALTY_5;
            result += DEFAULT_PAWN_SHIELD_PENALTY_5 * times;
            times++;
        }



    }//king in center of board
    else {
        result += DEFAULT_CENTRAL_KING_PENALTY;
        Bitboard perfect = (WPS_PERFECT << (63 - board.whiteKing)) & mask;
        Bitboard poor1 = (WPS_GOOD_KINGSIDE << (63 - board.whiteKing)) & mask;
        Bitboard ok = (WPS_OK << (63 - board.whiteKing)) & mask;
        Bitboard poor2 = (WPS_GOOD_QUEENSIDE << (63 - board.whiteKing)) & mask;

        if (perfect == (perfect & board.WHITE_PAWN_BOARD)) {
            return result;
        }
        if (poor1 == (poor1 & board.WHITE_PAWN_BOARD)) {
            result += DEFAULT_PAWN_SHIELD_PENALTY_6;
            return result;
        }
        if (ok == (ok & board.WHITE_PAWN_BOARD)) {
            return result;
        }

        if (poor2 == (poor2 & board.WHITE_PAWN_BOARD)) {
            result += DEFAULT_PAWN_SHIELD_PENALTY_6;
            return result;
        }
        int closePawns = bitboard::getSparseBitCount(board.WHITE_PAWN_BOARD & OCCUPANCY_MASK_KING[board.whiteKing]);
        result += std::min(0, (1 - closePawns) * DEFAULT_PAWN_SHIELD_PENALTY_7);
        int times = 0;
        if ((MASK_FILES[board.whiteKing] & board.WHITE_PAWN_BOARD) == 0) {
            result += DEFAULT_PAWN_SHIELD_PENALTY_8;
            times++;
        }
        if ((MASK_FILES[board.whiteKing - 1] & board.WHITE_PAWN_BOARD) == 0) {
            result += DEFAULT_PAWN_SHIELD_PENALTY_9;
            result += DEFAULT_PAWN_SHIELD_PENALTY_9 * times;
            times++;
        }
        if ((MASK_FILES[board.whiteKing + 1] & board.WHITE_PAWN_BOARD) == 0) {

            result += DEFAULT_PAWN_SHIELD_PENALTY_9;
            result += DEFAULT_PAWN_SHIELD_PENALTY_9 * times;
        }
    }
    return result;
}

Evaluation BoardEvaulator::evaluateBlackKingShield(const Board& board) {

    int result = 0;
    Bitboard mask = ~MASK_RANKS[board.blackKing]; // nothing on king's rank

    //If king is kingside
    if (board.BLACK_KING_BOARD & (KINGSIDE_MASK & CLEAR_FILE_F)) {

        mask &= KINGSIDE_MASK;

        Bitboard perfect = (BPS_PERFECT >> (board.blackKing)) & mask;
        Bitboard good = (BPS_GOOD_KINGSIDE >> (board.blackKing)) & mask;
        Bitboard ok = (BPS_OK >> (board.blackKing)) & mask;
        Bitboard poor = (BPS_GOOD_QUEENSIDE >> (board.blackKing)) & mask;

        if (perfect == (perfect & board.BLACK_PAWN_BOARD)) {
            return result; //perfect pawn shield --> no penalty
        }

        if (good == (good & board.BLACK_PAWN_BOARD)) {
            return result; //good pawn shield --> no penalty
        }

        if (ok == (ok & board.BLACK_PAWN_BOARD)) {
            return result; //ok pawn shield --> no penalty
        }

        if (poor == (poor & board.BLACK_PAWN_BOARD)) {
            return DEFAULT_POOR_PAWN_SHIELD_PENALTY; //poor pawn shield
        }
        result += DEFAULT_PAWN_SHIELD_PENALTY_1;
        int closePawns = bitboard::getSparseBitCount(board.BLACK_PAWN_BOARD & OCCUPANCY_MASK_KING[board.blackKing]);
        result += std::min(0, (2 - closePawns) * DEFAULT_PAWN_SHIELD_PENALTY_2);
        int times = 0;
        if ((MASK_FILES[board.blackKing] & board.BLACK_PAWN_BOARD) == 0) {
            result += DEFAULT_PAWN_SHIELD_PENALTY_3;
            times++;
        }
        if ((board.blackKing + 1) % 8 != 0) {
            if ((MASK_FILES[board.blackKing + 1] & board.BLACK_PAWN_BOARD) == 0) {
                result += DEFAULT_PAWN_SHIELD_PENALTY_4;
                result += DEFAULT_PAWN_SHIELD_PENALTY_4 * times;
            }
        }
        if ((MASK_FILES[board.blackKing - 1] & board.BLACK_PAWN_BOARD) == 0) {
            result += DEFAULT_PAWN_SHIELD_PENALTY_5;
            result += DEFAULT_PAWN_SHIELD_PENALTY_5 * times;
            times++;
        }


    }//if king is queenside
    else if (board.BLACK_KING_BOARD & QUEENSIDE_MASK) {

        mask &= QUEENSIDE_MASK;
        int idx = board.blackKing;
        if (board.BLACK_KING_BOARD & MASK_FILE_C) {
            idx--;
        }
        Bitboard perfect = (BPS_PERFECT >> (idx)) & mask;
        Bitboard good = (BPS_GOOD_QUEENSIDE >> (idx)) & mask;
        Bitboard ok = (BPS_OK >> (idx)) & mask;
        Bitboard poor = (BPS_GOOD_KINGSIDE >> (idx)) & mask;

        if (perfect == (perfect & board.BLACK_PAWN_BOARD)) {
            return result; //perfect pawn shield --> no penalty
        }

        if (good == (good & board.BLACK_PAWN_BOARD)) {
            return result; //good pawn shield --> no penalty
        }

        if (ok == (ok & board.BLACK_PAWN_BOARD)) {
            return result; //ok pawn shield --> no penalty
        }

        if (poor == (poor & board.BLACK_PAWN_BOARD)) {
            return DEFAULT_POOR_PAWN_SHIELD_PENALTY; //poor pawn shield
        }
        result += DEFAULT_PAWN_SHIELD_PENALTY_1;
        int closePawns = bitboard::getSparseBitCount(board.BLACK_PAWN_BOARD & (OCCUPANCY_MASK_KING[board.blackKing] | OCCUPANCY_MASK_KING[idx]));
        result += std::min(0, (2 - closePawns) * DEFAULT_PAWN_SHIELD_PENALTY_2);
        int times = 0;
        if ((MASK_FILES[board.blackKing] & board.BLACK_PAWN_BOARD) == 0) {
            result += DEFAULT_PAWN_SHIELD_PENALTY_3;
            times++;
        }
        if ((board.blackKing) % 8 != 0) {
            if ((MASK_FILES[board.blackKing - 1] & board.BLACK_PAWN_BOARD) == 0) {
                result += DEFAULT_PAWN_SHIELD_PENALTY_4;
                result += DEFAULT_PAWN_SHIELD_PENALTY_4 * times;
            }
        }
        if ((MASK_FILES[board.blackKing + 1] & board.BLACK_PAWN_BOARD) == 0) {
            result += DEFAULT_PAWN_SHIELD_PENALTY_5;
            result += DEFAULT_PAWN_SHIELD_PENALTY_5 * times;
            times++;
        }


    }//king in center of board
    else {
        result += DEFAULT_CENTRAL_KING_PENALTY;

        Bitboard perfect = (BPS_PERFECT >> (board.blackKing)) & mask;
        Bitboard poor1 = (BPS_GOOD_KINGSIDE >> (board.blackKing)) & mask;
        Bitboard ok = (BPS_OK >> (board.blackKing)) & mask;
        Bitboard poor2 = (BPS_GOOD_QUEENSIDE >> (board.blackKing)) & mask;

        if (perfect == (perfect & board.BLACK_PAWN_BOARD)) {
            return result;
        }
        if (poor1 == (poor1 & board.BLACK_PAWN_BOARD)) {
            result += DEFAULT_PAWN_SHIELD_PENALTY_6;
            return result;
        }
        if (ok == (ok & board.BLACK_PAWN_BOARD)) {
            return result;
        }

        if (poor2 == (poor2 & board.BLACK_PAWN_BOARD)) {
            result += DEFAULT_PAWN_SHIELD_PENALTY_6;
            return result;
        }
        int closePawns = bitboard::getSparseBitCount(board.BLACK_PAWN_BOARD & OCCUPANCY_MASK_KING[board.blackKing]);
        result += std::min(0, (1 - closePawns) * DEFAULT_PAWN_SHIELD_PENALTY_7);
        int times = 0;
        if ((MASK_FILES[board.blackKing] & board.BLACK_PAWN_BOARD) == 0) {
            result += DEFAULT_PAWN_SHIELD_PENALTY_8;
            times++;
        }
        if ((MASK_FILES[board.blackKing - 1] & board.BLACK_PAWN_BOARD) == 0) {
            result += DEFAULT_PAWN_SHIELD_PENALTY_9;
            result += DEFAULT_PAWN_SHIELD_PENALTY_9 * times;
            times++;
        }
        if ((MASK_FILES[board.blackKing + 1] & board.BLACK_PAWN_BOARD) == 0) {

            result += DEFAULT_PAWN_SHIELD_PENALTY_9;
            result += DEFAULT_PAWN_SHIELD_PENALTY_9 * times;
        }
    }
    return result;

}
