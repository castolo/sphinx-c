/* 
 * File:   Test.h
 * Author: Steve James <SD.James@outlook.com>
 *
 * This file serves as an easy interface to C++ Simple Tests, as provided by
 * the Netbeans IDE. The wrapper class provides 3 methods: startSuite, endSuite
 * and testFunction, which are used to start and end test suites, and test individual
 * functions respectively. Also provided is the assert macro REQUIRE, which
 * accepts an optional message argument. 
 * 
 * One final thing to note is that the file defines the DEBUG flag, if it is not
 * already set, so that any assert calls in the code are executed. 
 * 
 * Example usage:
 * 
 * #include "Test.h"
 *
 * void test1(int n) {
 *      for (int i = 0; i < n; i++) {
 *          REQUIRE(i < 6, "Oops");
 *      }
 * }
 *
 * int main() {
 *      Test::startSuite("Suite");
 *      Test::testFunction("TestLoop", test1, 10);
 *      Test::endSuite();
 *      return 0;
 * }
 * 
 * Created on 18 October 2014, 12:12 PM
 */

#ifndef TEST_H
#define	TEST_H

#include <iostream>
#include <cstdint>
#include <chrono>
#include <sstream>
#include <exception>
#include <cstdio>
#include <algorithm>
#include <cctype>

#ifndef DEBUG
#define DEBUG
#endif


/*
 * Anonymous namespace to protect these variables and functions from the outside
 */
namespace {

    /*
     * Custom exception to be used only when an assert condition fails.
     */
    class AssertionException : public std::exception {
    };


    /*
     * The current test suite being run
     */
    std::string latestSuite;
    /*
     * The current function being tested
     */
    std::string latestFunction;

    /*
     * The time the test suite was started
     */
    int64_t suiteTimeStarted;

    /*
     * The time the function to be tested was started
     */
    int64_t functionTimeStarted;

    /**
     * The current time in milliseconds
     * @return the number of milliseconds that have elapsed since the epoch (1
     * January 1970)
     */
    inline int64_t getCurrentTime() {
        std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
        auto duration = now.time_since_epoch();
        return std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
    }

    /**
     * Function that alerts the Simple C++ Test framework that an assertion has failed
     * @param message the message to display
     */
    inline void fail(std::string message) {
        std::cout << message << std::endl;
        double time = (getCurrentTime() - functionTimeStarted) / 1000.0;
        std::cout << "%TEST_FAILED% time=" << time << " testname=" << latestFunction << " (" << latestSuite
                << ") message=" << message << std::endl;
    }

    /**
     * Helper function to display the assertion failure that occurred
     * @param expr the expression that failed
     * @param message the user-defined message to display
     * @param file the file in which the failure occurred
     * @param line the line on which the failure occurred
     */
    inline void __assertFailure__(std::string expr, std::string message,
            std::string file, int line) {
        std::stringstream ss;
        ss << "'" << expr << "' failed in file " << file <<
                " (line " << line << ")";
        if (!message.empty()) {
            ss << " : " << message << "\n";
        }
        fail(ss.str());
        throw AssertionException(); /*Exit possibly non-void function*/
    }

}


/*
 * Avoid using this macro directly. Instead, use REQUIRE(X)
 */
#define __REQUIRE1__(x) if (! (x))                                              \
                            do{                                                 \
                                __assertFailure__(#x, "", __FILE__, __LINE__);  \
                            }                                                   \
                            while(0)
/*
 * Avoid using this macro directly. Instead, use REQUIRE(X, Y)
 */
#define __REQUIRE2__(x, y) if (! (x))                                           \
                            do{                                                 \
                                __assertFailure__(#x, y, __FILE__, __LINE__);   \
                            }                                                   \
                            while(0)
/*
 * Avoid using this macro. This is simply used to overload the REQUIRE macro on
 * the number of arguments
 */
#define __GET_MACRO__(_1,_2,NAME,...) NAME

/*
 * Use this macro to test that a condition should be met. It can
 * be used in two ways, either directly, viz:
 * 
 * REQUIRE(foo == bar);
 * 
 * or in conjunction with a user-defined message
 * 
 * REQUIRE(foo == bar, "Foo was not equal to bar :("); 
 * 
 */
#define REQUIRE(...) __GET_MACRO__(__VA_ARGS__, __REQUIRE2__, __REQUIRE1__)(__VA_ARGS__)


namespace Test {

    /**
     * Indicates that a new test suite is about to be started
     * @param name the name of the test suite. The name may not have 
     * any spaces - this function will therefore strip any whitespace.
     */
    inline void startSuite(std::string name) {
        //name cannot contain spaces
        name.erase(std::remove_if(name.begin(), name.end(), ::isspace), name.end());
        latestSuite = name;
        std::cout << "%SUITE_STARTING% " << name << std::endl
                << "%SUITE_STARTED%" << std::endl;
        suiteTimeStarted = getCurrentTime();
    }

    /**
     * Indicates the end of the current test suite
     */
    inline void endSuite() {
        double time = (getCurrentTime() - suiteTimeStarted) / 1000.0;
        std::cout << "%SUITE_FINISHED% time=" << time << std::endl;
    }

    /**
     * Tests a function, which may or may not have any parameters.
     * @param name the name of the function being tested. The name may not have 
     * any spaces - this function will therefore strip any whitespace.
     * @param func the function to be tested
     * @param params the parameters, if any, to be passed to the function
     */
    template<typename Func, typename...Params>
    void testFunction(std::string name, Func func, Params... params);

}

template<typename Func, typename...Params>
void Test::testFunction(std::string name, Func func, Params... params) {
    //function name cannot contain spaces
    name.erase(std::remove_if(name.begin(), name.end(), ::isspace), name.end());
    latestFunction = name;
    std::cout << "%TEST_STARTED% " << name << " (" << latestSuite << ")" << std::endl;
    std::cout << latestSuite << " -> " << latestFunction << std::endl;
    functionTimeStarted = getCurrentTime();
    //We wrap the function call in a try-catch so that, if the test fails, we
    //can exit the function immediately by throwing an exception.
    try {
        func(params...);
    } catch (AssertionException& e) {
        //we failed an assertion test. We've already failed, so do nothing
    } catch (int e) {
        fail("Function threw exception with error code " + std::to_string(e));
    } catch (std::exception& e) {
        fail("Function threw exception " + std::string(e.what()));
    }

    double time = (getCurrentTime() - functionTimeStarted) / 1000.0;
    std::cout << "%TEST_FINISHED% time=" << time << " " << name << " (" << latestSuite << ")" << std::endl;
}

#endif	/* TEST_H */
