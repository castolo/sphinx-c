/* 
 * File:   Move.h
 * Author: Steve James <SD.James@outlook.com>
 *
 * This header file contains some methods pertaining to moves, such as extracting
 * information from them, or converting them to human-readable form. 
 * 
 * Created on 27 November 2013, 9:27 AM
 *  
 */

#ifndef MOVE_H
#define	MOVE_H

#include <string>
#include "Constants.h"


namespace Utils {

    /*
     * Encoded (L2R) as: check (1 bit) flags (3 bits) to (6 bits) from (6 bits)
     */
    inline Move createMove(int from, int to, int flags = MOVE_NORMAL, bool check = false) {
        Move move = check ? 1 : 0;
        return (((((move << 3) | flags) << 6) | to) << 6) | from;
    }

    /**
     * Gets the square that the move started on
     * @param move the move
     */
    inline int getSource(Move move) {
        return move & 0b111111;
    }

    /**
     * Gets the destination square of the move
     * @param move the move
     */
    inline int getDestination(Move move) {
        return (move >> 6) & 0b111111;
    }

    /**
     * Gets the flags set for this move (see the MoveType enum)
     * @param move the move
     */
    inline int getFlags(Move move) {
        return (move >> 12) & 0b111;
    }

    /**
     * False does not necessarily mean the move does NOT give
     * check (e.g. discovered check), it simply has yet to be calculated. True means true though
     * @return whether or not the move causes check
     */
    inline bool isDirectCheck(Move move) {
        return move >> 15;
    }

    /**
     * Converts the move to long algebraic notation
     * @param move the move
     * @return an unambiguous string representation of the move
     */
    inline std::string toInputNotation(Move move) {
        int source = getSource(move);
        int dest = getDestination(move);
        int type = getFlags(move);

        std::string ans;
        ans.push_back('a' + source % 8);
        ans.push_back('0' + 8 - source / 8);
        ans.push_back('a' + dest % 8);
        ans.push_back('0' + 8 - dest / 8);

        switch (type) {
            case MoveType::MOVE_PROMOTION_BISHOP:
                ans.push_back('b');
                break;
            case MOVE_PROMOTION_KNIGHT:
                ans.push_back('n');
                break;
            case MOVE_PROMOTION_ROOK:
                ans.push_back('r');
                break;
            case MOVE_PROMOTION_QUEEN:
                ans.push_back('q');
                break;
        }
        return ans;
    }
}

#endif	/* MOVE_H */

