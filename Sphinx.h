/* 
 * File:   Sphinx.h
 * Author: Steve James <SD.James@outlook.com>
 *
 * This class represents the actual chess engine that is responsible for 
 * calculating and playing moves.
 * 
 * Created on 11 April 2013, 8:37 AM
 *  
 */

#ifndef SPHINX_H
#define	SPHINX_H

#include "KillerHeuristic.h"
#include "TranspositionTable.h"
#include "StaggeredMoves.h"
#include "Line.h"
#include <vector>
#include "Board.h"
#include <string>

#include "PolyGlotBook.h"
#include "BoardEvaluator.h"
#include "Option.h"
#include "AlternateAPI.h"

/*
 * This class represents the actual chess engine that is responsible for 
 * calculating and playing moves.
 */
class Sphinx {
    //Allow utility function to call our quiescence search directly
    friend class AlternateAPI;

public:


    static const std::string NAME;
    static const std::string VERSION;
    static const std::string AUTHOR;
    static const std::string DATE;

    /**
     * Creates a new chess engine with the given name
     * @param name the name of the chess engine
     */
    Sphinx(std::string name = NAME) : name(name) {
        std::fill(options.begin(), options.end(), true);
    }

    //PREVENT COPY + ASSIGNMENT
    Sphinx(const Sphinx&) = delete;
    Sphinx& operator=(const Sphinx&) = delete;

    /**
     * Sets the path to the opening book, if opening books are allowed to be used.
     * If this is the case, the opening book is recreated with the given path
     * @param path that path to the Polyglot book
     */
    inline void setBookPath(std::string const&path) {
        if (options[Option::OPENING_BOOK]) {
            openingBook = std::make_unique<PolyGlotBook>(path);
        }
    }

    /**
     * Sets a particular option
     * @param option the option to set
     * @param val the value of the option
     */
    void setOption(Option option, bool val) {
        if (option < NUM_OPTIONS) {
            options[option] = val;
        }
    }

    /**
     * Returns the side to play
     */
    inline Colour getSide() const {
        return board.getSide();
    }

    /**
     * Sets the maximum depth the search can make
     * @param maxDepth the maximum depth
     */
    inline void setMaxDepth(int maxDepth) {
        MAX_DEPTH = maxDepth;
    }

    /**
     * Gets the name of the engine
     */
    inline std::string getName() const {
        return NAME;
    }

    /**
     * The name of the engine's author
     * @return the name of the author
     */
    inline std::string getAuthor() const {
        return AUTHOR;
    }

    /**
     * The player's version number
     * @return the version of the player
     */
    inline std::string getVersion() const {
        return VERSION;
    }

    /**
     * Returns whether the engine is currently thinking
     * @return true if the engine is thinking, false otherwise
     */
    inline bool isThinking() const {
        return thinking;
    }

    /**
     * Returns whether the engine was interrupted in its previous search
     */
    inline bool isInterrupted() const {
        return interrupted;
    }

    /**
     * Interrupts the engine if it currently calculating
     */
    inline void interrupt() {
        if (thinking) {
            interrupted = true;
        }
    }

    /**
     * Sets the engine's position
     * @param fen the position
     */
    inline void setPosition(std::string fen) {
        board.setPosition(fen);
    }

    /**
     * Plays a move on the board if valid
     * @param move the move to be played
     * @return the move played, or NULL_MOVE if no move matched
     */
    Move playMove(std::string move);

    Line search(Time white, Time black);

    /**
     * Resets the engine to its default state
     */
    void reset();

    /**
     * Sets the minimum depth the search can make
     * @param minDepth the minimum depth
     */
    inline void setMinDepth(int minDepth) {
        MIN_DEPTH = minDepth;
    }

    /**
     * TODO calculate optimal time controls
     * Calculates the time that should be allocated to the current calculation
     * @param timeLeft the total time remaining in the game
     * @return the time allocated to deciding on this move
     */
    inline Time calculateTime(Time timeLeft) const {
        return timeLeft / 30;
    }

protected:

    Board board;

    /**
     * Evaluates the current position, from the current side's perspective
     * @return the evaluation of the current position, in centipawns
     */
    inline virtual Evaluation evaluatePosition() const {
        return BoardEvaulator::evaluate(board);
    }

private:

    std::string name = NAME;
    Time searchStarted = 0;
    Time timeAllowed = 0;
    int MAX_DEPTH = DEFAULT_MAX_DEPTH;
    int MIN_DEPTH = DEFAULT_MIN_DEPTH;
    bool thinking = false;
    bool interrupted = false;
    Line previousLine;
    long int nodes = 0;
    bool useLastMove = false;
    int depth = 0;
    int sequenceNum = 0;
    Move globalBestMove = NULL_MOVE;
    KillerHeuristic killerHeuristic;
    std::array<bool, Option::NUM_OPTIONS> options;
    std::unique_ptr<TranspositionTable> table = nullptr;
    std::unique_ptr<PolyGlotBook> openingBook = nullptr;

    /**
     * \deprecated{
     * Compares two moves to determine which of the two is noisier
     * @param A the first move
     * @param B the second move
     * 
     * @return true if the first move is at least as noisy as the second. False
     * otherwise. 
     * }
     */
    bool compare(Move A, Move B);

    /**
     * \deprecated{
     * Performs a quicksort on the move list
     * @param moves the list to sort
     * @param p the left index
     * @param r the right index
     * }
     */
    void quicksort(MoveList& moves, int p, int r);
    /**
     * \deprecated{
     * Performs a partitioning as per QuickSort
     * }
     */
    int partition(MoveList& moves, int p, int r);

    /**
     * Performs an alpha-beta search from the current position
     * @param ply the current ply
     * @param alpha the value of alpha
     * @param beta the value of beta
     * @param nullMovesAllowed whether null moves are allowed at this depth
     * @return the evaluation score for the current position
     */
    int alphaBeta(int ply, int alpha, int beta, bool nullMovesAllowed);

    /**
     * Function called when the game is over and the score is to be calculated
     * @param ply the current ply of our search, which allows us to weight earlier
     * checkmates more greatly than later ones
     * @return the evaluation of the final position
     */
    inline Evaluation completionCheck(int ply)  {
        if (board.isInCheck()) {
            return -(MATE_VALUE + ply);
        }
        return -(DRAW_VALUE); // + CONTEMPT_VALUE);    
    }

    /**
     * Performs a quiescence search from the current position
     * @param alpha the alpha bound
     * @param beta the beta bound
     * @return the score according to the quiescence search
     */
    int quiescenceSearch(int alpha, int beta);

    /**
     * Gets a list of noisy (e.g. captures and checks) at the current position
     */
    MoveList getNoisyMoves();

    /**
     * Ranks a list of moves in order of those that should be played first
     * @param moves the list of moves
     * @param ply the current ply
     * @return the ordered moves
     */
    StaggeredMoves<12> rankMoves(const MoveList& moves, int ply) const;

    /**
     * Determines if the time allocated to the move has elapsed
     * @return whether there is still time remaining
     */
    inline bool isTimeLeft() const {
        Time mtime = Utils::getCurrentTime() - searchStarted;
        return (mtime < timeAllowed) || (previousLine.empty()) //Allow extra time if no move found
                || (previousLine.getDepth() < MIN_DEPTH);
    }

    /**
     * Determines whether the move can be reduced (see Late Move Reduction)
     * @param ply the current ply
     * @param moveNum the index of the move being played
     * @param move the move itself
     * @return whether we can apply LMR to the move
     */
    bool canReduceMove(int ply, int moveNum, Move move);

};

#endif	/* SPHINX_H */

