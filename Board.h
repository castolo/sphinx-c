/* 
 * File:   Board.h
 * Author: Steve James <SD.James@outlook.com>
 *
 * This class represents a chess board and contains the bulk of the engines knowledge.
 * The primary functionality that the board offers is the move generator, make and unmake 
 * move functions.
 * 
 * Created on 11 April 2013, 8:37 AM
 */



#ifndef BOARD_H
#define	BOARD_H

#include "HashBoard.h"
#include "Move.h"
#include "PieceListBoard.h"
#include <string>
#include <vector>
#include <stack>
#include <cstdlib>
#include <stdio.h>
#include <sstream>
#include <iostream>
#include <array>

#define WHITE_PAWN_BOARD WHITE_BITBOARDS[PAWN]
#define WHITE_KNIGHT_BOARD WHITE_BITBOARDS[KNIGHT]
#define WHITE_BISHOP_BOARD WHITE_BITBOARDS[BISHOP]
#define WHITE_ROOK_BOARD WHITE_BITBOARDS[ROOK]
#define WHITE_QUEEN_BOARD WHITE_BITBOARDS[QUEEN]
#define WHITE_KING_BOARD WHITE_BITBOARDS[KING]

#define BLACK_PAWN_BOARD BLACK_BITBOARDS[PAWN]
#define BLACK_KNIGHT_BOARD BLACK_BITBOARDS[KNIGHT]
#define BLACK_BISHOP_BOARD BLACK_BITBOARDS[BISHOP]
#define BLACK_ROOK_BOARD BLACK_BITBOARDS[ROOK]
#define BLACK_QUEEN_BOARD BLACK_BITBOARDS[QUEEN]
#define BLACK_KING_BOARD BLACK_BITBOARDS[KING]

// <editor-fold defaultstate="collapsed" desc="History POD type">

/**
 * POD type that stores information about previous positions, required by the 
 * {@link #undoMove(Move) undoMove} method. It consists of 50 move rule, 
 * numbers of played moves, previous <i>en-passant</i> ,white castling rights and black
 * castling rights.
 */
struct History {

    History() : capturedPiece(Piece::EMPTY_SQUARE), blackCastle(BLACK_CASTLE_NONE),
    whiteCastle(WHITE_CASTLE_NONE), enPassant(-1), fiftyMove(0) {
    }
    Piece capturedPiece;
    CastlingRights blackCastle;
    CastlingRights whiteCastle;
    int enPassant;
    int fiftyMove;
};
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Bitboard Operations">

namespace bitboard {

    /**
     * Prints the bitboard to standard output in an 8x8 grid. Used for <b>debugging</b>
     * purposes only
     * @param bitboard the bitboard to be displayed
     */
    void show(Bitboard bitboard);

    /**
     * Converts the bitboard to a 8x8 grid 
     * @param bitboard the bitboard to be converted
     * @return a string consisting of the 8x8 grid
     */
    std::string toString(Bitboard bitboard);

    /**
     * Returns index of least-significant bit (i.e. first bit from the right).
     *  <b>To reference in board array, use <code>63-temp</code></b>
     * @param bb the bitboard
     * @return  the index of the least-significant bit. Note that if the bitboard 
     * has no bits set, this method returns 0.
     */
    inline int LS1B(Bitboard bb) {
        return LS1B_INDEX[((bb & -bb) * DEBRUIJN) >> 58];
    }

    /**
     * Get the bitboard that represents a square at a given index (where 
     * 0 = a8 and 63 = h1)
     * @param idx the index to be represented
     * @return a bitboard whose only set bit occurs at position <code>idx</code>
     */
    inline Bitboard getBitBoard(int idx) {
        return 1ull << idx;
    }

    /**
     * Counts the number of bits set in a bitboard. 
     * @param bb the bitboard
     * @return the number of bits set
     */
    inline int getSparseBitCount(Bitboard bb) {
        int count;
        for (count = 0; bb; count++) {
            bb &= bb - 1;
        }
        return count;
    }

    /**
     * Counts the number of bits set in a bitboard. In practice, there is little difference
     * between {@link #getSparseBitCount(Bitboard) getSparseBitCount} and 
     * {@link #getDenseBitCount(Bitboard) getDenseBitCount}
     * @param bb the bitboard
     * @return the number of bits set
     */
    inline int getDenseBitCount(Bitboard bb) {
        bb -= (bb >> 1) & 0x5555555555555555ull;
        bb = (bb & 0x3333333333333333ull) + ((bb >> 2) & 0x3333333333333333ull);
        bb = (bb + (bb >> 4)) & 0x0f0f0f0f0f0f0f0full;
        return (bb * 0x0101010101010101ull) >> 56;
    }

    Bitboard getInterposed(int from, int to, Direction direction);

}
// </editor-fold>

/**
 * This class represents a chess board and contains the bulk of the engines knowledge.
 * The primary functionality that the board offers is the move generator, make and unmake 
 * move functions.
 */
class Board : public HashBoard, public PieceListBoard {
    friend class BoardEvaulator;

public:

    /**
     * Creates a new Board objects and sets the position to the starting one. 
     * 
     */
    Board() {
        setPosition(START_POSITION);
    }

    /**
     * Accesses the piece at a certain square
     * @param i the square (a8 = 0, h1 = 63)
     */
    inline Piece operator[](int i) const {
        return static_cast<Piece> (board[i]);
    }

    /**
     * Calculates the moving piece. Since only the source and destination squares are 
     * stored in the move, the board class is responsible for calculating which piece is
     * moving (as opposed to the other move-related functions located in the Move.h header file).
     * 
     * <b>NB: this function only works if the move has NOT yet been played. If it has been played,
     * the moving piece no longer resides on the source square, in which case, the piece that
     * currently resides on the move's destination square is actually the moving piece. </b>
     * 
     * @param move the move that was made
     * @return the piece moved
     */
    inline int getMovingPiece(Move move) const {
        int source = Utils::getSource(move);
        return board[source];
    }

    /**
     * Access the castling rights for white
     * @see Constants#WHITE_CASTLE_NONE
     * @see Constants#WHITE_CASTLE_SHORT
     * @see Constants#WHITE_CASTLE_LONG
     * @see Constants#WHITE_CASTLE_BOTH
     */
    inline CastlingRights getWhiteCastlingRights() const {
        return whiteCastle;
    }

    /**
     * Access the castling rights for black
     * @see Constants#BLACK_CASTLE_NONE
     * @see Constants#BLACK_CASTLE_SHORT
     * @see Constants#BLACK_CASTLE_LONG
     * @see Constants#BLACK_CASTLE_BOTH
     */
    inline CastlingRights getBlackCastlingRights() const {
        return blackCastle;
    }

    /**
     * Returns the square on which <i>en-passant</i> can occur
     * @return -1 if no <i>en-passant</i> capture is possible, or the destination 
     * square otherwise (a8 = 0, h1 = 63).
     */
    inline Square getEnPassant() const {
        return static_cast<Square> (enPassant);
    }

    /**
     * Access the white pawn bitboard 
     */
    inline Bitboard getWhitePawns() const {
        return WHITE_PAWN_BOARD;
    }

    /**
     * Access the black pawn bitboard 
     */
    inline Bitboard getBlackPawns() const {
        return BLACK_PAWN_BOARD;
    }

    /**
     * Return the piece captured during the previous move, if any. If no move has 
     * yet been made, EMPTY_SQUARE is returned.
     * @return the piece captured by the previous move (or empty square, 
     * if none was made).
     * 
     * @see Constants#EMPTY_SQUARE
     */
    inline Piece getLastCapturedPiece()const {
        if (histIndex == 0) {
            return Piece::EMPTY_SQUARE;
        }
        return history[histIndex - 1].capturedPiece;
    }

    /**
     * Calculates the captured piece. Since only the source and destination squares are 
     * stored in the move, the board class is responsible for calculating which piece is
     * captured (as opposed to the other move-related functions located in the Move.h header file).
     * 
     * <b>NB: this function only works if the move has NOT yet been played. If it has been played,
     * the captured piece no longer resides on the destination square (the moving piece does).
     * In this case, {@link #getLastCapturedPiece()} should be used. </b>
     * 
     * <b>NNB: <i>En-passant</i> captures always return a pawn of the opposite colour, regardless
     * of whether the move has been played or not (as described above). </b>
     * 
     * @param move the move that was made
     * @return the piece captured, or EMPTY_SQUARE if no piece was captured
     * 
     * @see Constants#EMPTY_SQUARE
     * 
     */
    inline Piece getCapturedPiece(Move move) const {
        int dest = Utils::getDestination(move);
        if (Utils::getFlags(move) == MOVE_CAPTURE_EN_PASSANT) {
            if (dest < 32) {
                return BLACK_PAWN;
            } else {
                return WHITE_PAWN;
            }
        }
        return static_cast<Piece> (board[dest]);
    }

    /**
     * Returns the square of the king of the current side to move
     */
    inline Square getKingPosition() const {
        return side == WHITE ? whiteKing : blackKing;
    }

    /**
     * Accesses the number of moves since the last capture or pawn move. Used in 
     * determining draws by fifty move rule. 
     */
    inline int getFiftyRule() const {
        return fiftyRule;
    }

    /**
     * Returns a FEN-string representation of the current position.
     * 
     * Note that this method is not efficient and should not be used in any functions that
     * requires fast code, such as making and un-making moves.
     */
    std::string getPosition() const;

    /**
     * Sets up a given position on the board. Note that no error checking is performed,
     * so the input parameter must be of the correct format to prevent unexpected 
     * behaviour. 
     * @param pos a FEN-string representation of the position
     */
    void setPosition(std::string pos);

    /**
     * Executes a pseudo-legal move.
     * @param move the move to be played
     * @return whether the move played is legal.
     */
    bool makeMove(Move move, bool isLegal = false);

    /**
     * Reverses the a move that has been made
     * @param move the move to be unmade
     */
    void undoMove(Move move);

    /**
     * Changes the side to move
     */
    inline void swapSides() {
        side = static_cast<Colour> (-side);
        updateHash(sideHash);
    }

    /**
     * Returns the side to move
     * 
     * @see Constants#WHITE
     * @see Constants#BLACK
     */
    inline Colour getSide() const {
        return side;
    }

    /**
     * Generates pseudo-legal moves for a given position
     * 
     * The entry following the final move is 0, which allows for the array to be
     * traversed as follows:
     * 
     * * <pre>
     * {@code
     * MoveList moves = board.generatePseudoMoves();
     * for (int i=0; Move move = moves[i]; ++i){
     *  //Do something with move e.g. board.makeMove(move);
     * }
     * }
     * </pre> 
     * 
     * @return an array of pseudo-legal moves.
     */
    MoveList generatePseudoMoves() const;
    /*
     * This move-generation function is currently only used by the divide procedure
     */
    MoveList generatePseudoMoves(bool&) const;

    // <editor-fold defaultstate="collapsed" desc="Unused movegen functions ATM">

    MoveList generateCaptures() const;
    MoveList generateCheckingMoves() const;
    void generateCheckEvasions(Bitboard, int, MoveList&, int&) const;

    inline bool isLegalKingMove(int to, int oppSide, int king, Direction dir1, Direction dir2, int pos1, int pos2) const {

        if (attacksTo(to, oppSide)) {
            return false;
        }
        if (DIRECTIONS[king][to] == dir1) {
            return (to == pos1);
        }
        if (DIRECTIONS[king][to] == dir2) {
            return (to == pos2);
        }
        return true;
    }


    /**
     * A special move generator that generates only noisy moves (captures, promotions 
     * and checks).
     * @return an array of pseudo-legal noisy moves.
     * @see #generatePseudoMoves()
     */
    MoveList generateNoisyPseudoMoves() const;
    // </editor-fold>

    /**
     * @deprecated
     * Determines if moving the piece at the source square results in a discovered check
     * @param source the moving piece
     * @param destination the moving piece
     * @param king the bitboard of the opponent's king
     * @param diagonal the diagonal to check
     * @return 
     */
    bool isDiscoveredCheckOnDiagonal(Bitboard source, Bitboard destination, Bitboard king, Bitboard diagonal) const;


    /**
     * @deprecated
     * Determines if moving the piece at the source square results in a discovered check
     * @param source the moving piece
     * @param destination the moving piece
     * @param king the bitboard of the opponent's king
     * @param rank the horizontal to check
     */
    bool isDiscoveredCheckOnHorizontal(Bitboard source, Bitboard destination, Bitboard king, Bitboard horizintal) const;

    /**
     * Prints the board to standard output (for debugging purposes)
     */
    void show() const;

    friend std::ostream& operator<<(std::ostream& out, const Board& board);

    /**
     * Determines whether there is enough material on either side to mate.
     */
    inline bool hasSufficientMaterial() const {
        if (whitePieceListCount[PAWN] + blackPieceListCount[PAWN] + whitePieceListCount[ROOK] + blackPieceListCount[ROOK] + whitePieceListCount[QUEEN] + blackPieceListCount[QUEEN] == 0) {
            if (((whitePieceListCount[KNIGHT] + whitePieceListCount[BISHOP]) <= 1) && ((blackPieceListCount[KNIGHT] + blackPieceListCount[BISHOP]) <= 1)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Returns the number of times the current position has occurred.
     */
    short getRepetitions() const;

    /**
     * Determines whether a null move can be made in the position.  
     * A null move can be made unless:
     * 
     * <ul>
     * <li> The current side is in check, or</li>
     * <li> there are no major pieces left and there are 0 or 1 knight/bishop remaining</li>
     * </ul>
     * 
     */
    bool canMakeNullMove();

    /**
     * Returns the number of full-moves (i.e. a move by white and black is one full
     * move) 
     */
    inline short getMoves() const {
        return moves;
    }

    /**
     * Returns whether the side to move is currently in check. 
     */
    inline bool isInCheck() const {
        return isSquareAttacked(getKingPosition());
    }

    /**
     * Determines whether the given square is under attack by the opponent (the side not
     * to move)
     * @param idx the square
     * @return whether the square is threatened
     */
    inline bool isSquareAttacked(int idx) const {
        return isSquareAttacked(idx, -side);
    }

    /**
     * Determine whether multiple squares are attacked by the opposing side
     * @param bb a bitboard representing the squares to check
     * @return true if <b> any</b> of the squares are threatened, false otherwise
     * 
     * @see #isSquareAttacked
     */
    inline bool areSquaresAttacked(Bitboard defend) const {
        return areSquaresAttacked(defend, -side);
    }

    /**
     * Returns a bitboard of pieces that attack a square (including the king)
     * @param pos the square
     * @param bySide the side doing the attacking
     */
    Bitboard attacksTo(int pos, int bySide) const;

    /**
     * Returns an array containing a count of the number of white pieces. 
     * These can be accessed in the following way:
     * 
     * <pre>
     * {@code
     * std::array<int,7> A = board.getWhitePiecesList();
     * int p = A[PAWN]; //get number of white pawns
     * int r = A[ROOK]; //get number of white rooks
     * 
     * for (int piece = PAWN; piece <= KING; ++piece) {
     * int c = A[piece];
     * }
     * 
     * }
     * </pre> 
     * 
     * <b>Note that the first element contains garbage </b>
     */
    inline const PieceListCount& getWhitePiecesList() const {
        return whitePieceListCount;
    }

    /**
     * Returns an array containing a count of the number of black pieces. 
     * These can be accessed in the following way:
     * 
     * <pre>
     * {@code
     * std::array<int,7> B = board.getBlackPiecesList();
     * int p = B[PAWN]; //get number of black pawns
     * int r = B[ROOK]; //get number of black rooks
     * 
     * for (int piece = PAWN; piece <= KING; ++piece) {
     * int c = B[piece];
     * }
     * 
     * }
     * </pre> 
     * 
     * <b>Note that the first element contains garbage </b>
     *
     */
    inline const PieceListCount& getBlackPiecesList() const {
        return blackPieceListCount;
    }

    /**
     * 
     * @deprecated This function is used for DEBUGGING PURPOSES ONLY. Do not use it
     * during normal execution. Instead use {@link #getWhitePiecesList()} and 
     * {@link #getBlackPiecesList()}.
     * 
     * @param side the side whose pieces are to be counted
     * @return an array containing the pieces remaining for a side 
     */
    PieceListCount getPieces(int side);

    /**
     * Computes a Zobrist hash function that considers material only (ignoring kings)
     * @return the hash
     */
    Hash getMaterialKey() const;

    // <editor-fold defaultstate="collapsed" desc="Attack sets for magic number generation">

    static Bitboard getRookAttackSet(int square, Bitboard mask) {

        Bitboard rightMoves = RIGHT_BOARD[square] & mask;
        rightMoves = (rightMoves >> 1) | (rightMoves >> 2) | (rightMoves >> 3) | (rightMoves >> 4) | (rightMoves >> 5) | (rightMoves >> 6);
        rightMoves &= RIGHT_BOARD[square];
        rightMoves ^= RIGHT_BOARD[square];

        Bitboard leftMoves = LEFT_BOARD[square] & mask;
        leftMoves = (leftMoves << 1) | (leftMoves << 2) | (leftMoves << 3) | (leftMoves << 4) | (leftMoves << 5) | (leftMoves << 6);
        leftMoves &= LEFT_BOARD[square];
        leftMoves ^= LEFT_BOARD[square];

        Bitboard upMoves = UP_BOARD[square] & mask;
        upMoves = (upMoves << 8) | (upMoves << 16) | (upMoves << 24) | (upMoves << 32) | (upMoves << 40) | (upMoves << 48);
        upMoves &= UP_BOARD[square];
        upMoves ^= UP_BOARD[square];

        Bitboard downMoves = DOWN_BOARD[square] & mask;
        downMoves = (downMoves >> 8) | (downMoves >> 16) | (downMoves >> 24) | (downMoves >> 32) | (downMoves >> 40) | (downMoves >> 48);
        downMoves &= DOWN_BOARD[square];
        downMoves ^= DOWN_BOARD[square];


        return rightMoves | leftMoves | upMoves | downMoves;

    }

    static Bitboard getBishopAttackSet(int square, Bitboard mask) {
        Bitboard urMoves = NE_BOARD[square] & mask;
        urMoves = (urMoves << 7) | (urMoves << 14) | (urMoves << 21) | (urMoves << 28) | (urMoves << 35) | (urMoves << 42);
        urMoves &= NE_BOARD[square];
        urMoves ^= NE_BOARD[square];

        Bitboard ulMoves = NW_BOARD[square] & mask;
        ulMoves = (ulMoves << 9) | (ulMoves << 18) | (ulMoves << 27) | (ulMoves << 36) | (ulMoves << 45) | (ulMoves << 54);
        ulMoves &= NW_BOARD[square];
        ulMoves ^= NW_BOARD[square];

        Bitboard drMoves = SE_BOARD[square] & mask;
        drMoves = (drMoves >> 9) | (drMoves >> 18) | (drMoves >> 27) | (drMoves >> 36) | (drMoves >> 45) | (drMoves >> 54);
        drMoves &= SE_BOARD[square];
        drMoves ^= SE_BOARD[square];

        Bitboard dlMoves = SW_BOARD[square] & mask;
        dlMoves = (dlMoves >> 7) | (dlMoves >> 14) | (dlMoves >> 21) | (dlMoves >> 28) | (dlMoves >> 35) | (dlMoves >> 42);
        dlMoves &= SW_BOARD[square];
        dlMoves ^= SW_BOARD[square];

        return urMoves | ulMoves | drMoves | dlMoves;
    }
    // </editor-fold>

private:

    // <editor-fold defaultstate="collapsed" desc="UndoPromotion Helper">

    inline void undoPromotion(int source, int dest, int promPiece, int cp, int idx) {

        updateHash(piecesHash[PAWN - 1][idx][source]);
        updateHash(piecesHash[promPiece - 1][idx][dest]);

        if (cp == EMPTY_SQUARE) {
            removePiece(promPiece*side, dest);
            addPiece(PAWN*side, source);
        } else {
            updateHash(piecesHash[abs(cp) - 1][abs(idx - 1)][dest]);
            removePiece(promPiece*side, dest);
            addPiece(PAWN*side, source);
            addPiece(cp, dest);
        }
    }
    // </editor-fold>

    /**
     * Determines whether the given square is under attack by the given side
     * @param idx the square
     * @param bySide the attacking side
     * @return whether the square is threatened
     */
    bool isSquareAttacked(int idx, int bySide) const;

    /**
     * Determine whether multiple squares are attacked by the given side
     * @param defend a bitboard representing the squares to check
     * @param bySide the side attacking
     * @return true if <b> any</b> of the squares are threatened, false otherwise
     * 
     * @see #isSquareAttacked
     */
    inline bool areSquaresAttacked(Bitboard defend, int bySide) const {
        while (defend) {
            if (isSquareAttacked(63 - bitboard::LS1B(defend), bySide)) {
                return true;
            }
            defend &= defend - 1;
        }
        return false;
    }


    // <editor-fold defaultstate="collapsed" desc="Move Generation">

    /**
     * Magic rook move generation
     * @param pos the square on which the rook resides
     * @param friendly the bitboard of the side to move's pieces
     * @return a bitboard representing the possible rook moves
     */
    inline Bitboard getRookMoves(int pos, Bitboard friendly) const {
        int idx = static_cast<int> (((ALL_PIECES_BOARD & OCCUPANCY_MASK_ROOK[pos]) * ROOK_MAGICS[pos]) >> (MAGIC_ROOK_SHIFTS[pos]));
        return MAGIC_ROOK_MOVES[pos][idx] & (~friendly);
    }

    inline static Bitboard getGeneralMagicRookMoves(Bitboard occupancy, int pos, Bitboard friendly) {
        int idx = static_cast<int> (((occupancy & OCCUPANCY_MASK_ROOK[pos]) * ROOK_MAGICS[pos]) >> (MAGIC_ROOK_SHIFTS[pos]));
        return MAGIC_ROOK_MOVES[pos][idx] & (~friendly);
    }

    inline Bitboard getMagicRookMoves(int pos, Bitboard friendly, Bitboard blockers) const {
        int idx = static_cast<int> (((blockers & OCCUPANCY_MASK_ROOK[pos]) * ROOK_MAGICS[pos]) >> (MAGIC_ROOK_SHIFTS[pos]));
        return MAGIC_ROOK_MOVES[pos][idx] & (~friendly);
    }

    /**
     * Magic bishop move generation
     * @param pos the square on which the bishop resides
     * @param friendly the bitboard of the side to move's pieces
     * @return a bitboard representing the possible bishop moves
     */
    inline Bitboard getBishopMoves(int pos, Bitboard friendly) const {
        int idx = static_cast<int> (((ALL_PIECES_BOARD & OCCUPANCY_MASK_BISHOP[pos]) * BISHOP_MAGICS[pos]) >> (MAGIC_BISHOP_SHIFTS[pos]));
        return MAGIC_BISHOP_MOVES[pos][idx] & (~friendly);
    }

    inline static Bitboard getGeneralMagicBishopMoves(Bitboard occupancy, int pos, Bitboard friendly) {
        int idx = static_cast<int> (((occupancy & OCCUPANCY_MASK_BISHOP[pos]) * BISHOP_MAGICS[pos]) >> (MAGIC_BISHOP_SHIFTS[pos]));
        return MAGIC_BISHOP_MOVES[pos][idx] & (~friendly);
    }

    inline Bitboard getMagicBishopMoves(int pos, Bitboard friendly, Bitboard blockers) const {
        int idx = static_cast<int> (((blockers & OCCUPANCY_MASK_BISHOP[pos]) * BISHOP_MAGICS[pos]) >> (MAGIC_BISHOP_SHIFTS[pos]));
        return MAGIC_BISHOP_MOVES[pos][idx] & (~friendly);
    }

    void generateCastlingMoves(Move*, int&) const;
    void generateKnightMoves(Move*, int&) const;
    void generateKingMoves(Move*, int&) const;
    void generatePawnMoves(Move*, int&) const;
    void generateRookMoves(Move*, int&) const;
    void generateBishopMoves(Move*, int&) const;
    void generateQueenMoves(Move*, int&) const;
    void generateEnPassantMoves(Move*, int&) const;

    void generateNoisyKingMoves(Move*, int&) const;
    void generateNoisyKnightMoves(Move*, int&) const;
    void generateNoisyPawnMoves(Move*, int&) const;
    void generateNoisyRookMoves(Move*, int&) const;
    void generateNoisyBishopMoves(Move*, int&) const;
    void generateNoisyQueenMoves(Move*, int&) const;
    // </editor-fold>


    bool isPinnedOnKing(int, int, Bitboard, Bitboard) const;

    bool isPinnedOnKing(int, int) const;

    inline void shiftPieceBoards(int N) {
        for (int i = PAWN; i <= KING; ++i) {
            WHITE_BITBOARDS[i] <<= N;
            BLACK_BITBOARDS[i] <<= N;
        }
    }

    inline void orBitboards(Bitboard bb, int piece) {
        if (piece) { //piece could be 0
            if (piece > 0) {
                WHITE_BITBOARDS[piece] |= bb;
                WHITE_PIECES_BOARD |= bb;
            } else {
                BLACK_BITBOARDS[-piece] |= bb;
                BLACK_PIECES_BOARD |= bb;
            }
            ALL_PIECES_BOARD = BLACK_PIECES_BOARD | WHITE_PIECES_BOARD;
            EMPTY_BOARD = ~ALL_PIECES_BOARD;
        }
    }

    inline void andBitboards(Bitboard bb, int piece) {
        if (piece) { //piece could be 0
            if (piece > 0) {
                WHITE_BITBOARDS[piece] &= bb;
                WHITE_PIECES_BOARD &= bb;
            } else {
                BLACK_BITBOARDS[-piece] &= bb;
                BLACK_PIECES_BOARD &= bb;
            }
            ALL_PIECES_BOARD = BLACK_PIECES_BOARD | WHITE_PIECES_BOARD;
            EMPTY_BOARD = ~ALL_PIECES_BOARD;
        }
    }

    /**
     * <b> Note the order of parameters! First destination, then source! <b>
     * @param piece
     * @param dest
     * @param source
     */
    inline void movePiece(int piece, int dest, int source) {
        PieceListBoard::movePiece(piece, dest, source);
        removePiece(piece, source, false);
        addPiece(piece, dest, false);
    }

    inline void addPiece(int piece, int idx, bool callSuper = true) {
        if (callSuper) {
            PieceListBoard::addPiece(piece, idx);
        }
        board[idx] = piece;
        Bitboard bb = bitboard::getBitBoard(63 - idx);
        orBitboards(bb, piece);
    }

    inline void removePiece(int piece, int idx, bool callSuper = true) {
        if (callSuper) {
            PieceListBoard::removePiece(piece, idx);
        }
        board[idx] = EMPTY_SQUARE;
        Bitboard bb = ~bitboard::getBitBoard(63 - idx);
        andBitboards(bb, piece);
    }


    Colour side;
    CastlingRights whiteCastle;
    CastlingRights blackCastle;
    short enPassant;
    short fiftyRule;
    short moves;

    /*
     * Bitboards
     */
    Bitboard WHITE_PIECES_BOARD;
    Bitboard BLACK_PIECES_BOARD;
    Bitboard ALL_PIECES_BOARD;
    Bitboard EMPTY_BOARD;

    /**
     * Piece Bitboards
     */
    std::array<Bitboard, 7> BLACK_BITBOARDS;
    std::array<Bitboard, 7> WHITE_BITBOARDS;

    std::array<int, 64> board;
    std::vector<Move> movesPlayed;
    std::vector<Hash> repetitions;

    std::array<History, 1000> history;
    //stores 50 move rule numbers of played moves, prevEp,whiteCastle,BlackCastle;
    int histIndex; //histIndex points to the first free entry


};

#endif	/* BOARD_H */

