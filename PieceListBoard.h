/* 
 * File:   PieceList.h
 * Author: Steve James <SD.James@outlook.com>
 * 
 * This class represents the board using a piece-list oriented technique.
 * See https://chessprogramming.wikispaces.com/Piece-Lists for more details
 *
 * Created on 16 January 2014, 7:21 PM
 */

#ifndef PIECELISTBOARD_H
#define	PIECELISTBOARD_H

#include <array>
#include "Utils.h"
#include "Constants.h"

/*
 * Represents the array used to store the number of pieces for a single side
 */
using PieceListCount = std::array<int, 7>;

/*
 * Represents the arrays used to store the location of every piece for a single side
 */
using PieceList = std::array<std::array<int, 10>, 7>;

/**
 * This class represents the board using a piece-list oriented technique.
 * See https://chessprogramming.wikispaces.com/Piece-Lists for more details
 */
class PieceListBoard {
protected:

    /**
     * Creates the new piece-list data structure
     */
    PieceListBoard() {
    }

    /**
     * Clears the piece lists
     */
    inline void clear() {
        whitePieceListCount.fill(0);
        blackPieceListCount.fill(0);
        for (int i = PAWN; i <= QUEEN; ++i) {
            whitePieceList[i].fill(0);
            blackPieceList[i].fill(0);
        }
        whiteIndexBoard.fill(-1);
        blackIndexBoard.fill(-1);
        whiteKing = blackKing = Square::NUL;
    }

    /**
     * Adds a piece at the given square
     * @param piece the piece
     * @param pos the square
     */
    inline void addPiece(int piece, int pos) {
        switch (Utils::sign(piece)) {
            case WHITE:
                addWhitePiece(piece, pos);
                break;
            case BLACK:
                addBlackPiece(-piece, pos);
                break;
        }
    }

    /**
     * Adds a white piece at the given square
     * @param piece the piece
     * @param pos the square
     */
    inline void addWhitePiece(int piece, int pos) {
        assert(piece > 0);
        if (piece == KING) {
            ++whitePieceListCount[KING];
            whiteKing = static_cast<Square> (pos);
        } else {
            int idx = whitePieceListCount[piece]++;
            whiteIndexBoard[pos] = idx;
            whitePieceList[piece][idx] = pos;
        }
    }

    /**
     * Adds a black piece at the given square
     * @param piece the piece
     * @param pos the square
     */
    inline void addBlackPiece(int piece, int pos) {
        assert(piece > 0);
        if (piece == KING) {
            ++blackPieceListCount[KING];
            blackKing = static_cast<Square> (pos);
        } else {
            int idx = blackPieceListCount[piece]++;
            blackIndexBoard[pos] = idx;
            blackPieceList[piece][idx] = pos;
        }
    }

    /**
     * Move a piece from one square to another
     * @param piece the piece to move 
     * @param dest the destination square
     * @param source the source square
     */
    inline virtual void movePiece(int piece, int dest, int source) {
        assert(piece > 0);
        int idx;
        switch (Utils::sign(piece)) {
            case WHITE:
                if (piece == WHITE_KING) {
                    whiteKing = static_cast<Square> (dest);
                } else {
                    idx = whiteIndexBoard[source];
                    assert(idx != -1);
                    whiteIndexBoard[dest] = idx;
                    whitePieceList[piece][idx] = dest;
                }
                break;
            case BLACK:
                if (piece == BLACK_KING) {
                    blackKing = static_cast<Square> (dest);
                } else {
                    idx = blackIndexBoard[source];
                    assert(idx != -1);
                    blackIndexBoard[dest] = idx;
                    blackPieceList[-piece][idx] = dest;
                }
                break;
        }
    }

    /**
     * Removes a piece from the board <b>completely</b>
     * @param piece the piece to remove
     * @param pos the square the piece is on
     */
    inline void removePiece(int piece, int pos) {
        assert(piece > 0);
        int count, idx, square;
        switch (Utils::sign(piece)) {
            case WHITE:
                count = --whitePieceListCount[piece];
                idx = whiteIndexBoard[pos];
                assert(idx != -1);
                //  replace removed piece with the last piece in the list
                square = whitePieceList[piece][count];
                whitePieceList[piece][idx] = square;
                whiteIndexBoard[square] = idx;
                break;
            case BLACK:
                count = --blackPieceListCount[-piece];
                idx = blackIndexBoard[pos];
                assert(idx != -1);
                //  replace removed piece with the last piece in the list
                square = blackPieceList[-piece][count];
                blackPieceList[-piece][idx] = square;
                blackIndexBoard[square] = idx;
                break;
        }
    }

    /*
     * The number of each white piece
     */
    PieceListCount whitePieceListCount; // note that WPL[0] contains garbage

    /*
     * The number of each black piece
     */
    PieceListCount blackPieceListCount; // note that BPL[0] contains garbage  

    /*
     * The location of each white piece
     */
    PieceList whitePieceList; //[7][10]

    /*
     * The location of each black piece
     */
    PieceList blackPieceList; //[7][10]

    /*
     * A convenient way of accessing the position of the white king
     */
    Square whiteKing;

    /*
     * A convenient way of accessing the position of the black king
     */
    Square blackKing;

private:

    /*
     * Index board used for calculating and updating the white piece lists 
     */
    std::array<short, 64> whiteIndexBoard;

    /*
     * Index board used for calculating and updating the black piece lists 
     */
    std::array<short, 64> blackIndexBoard;

};
#endif	/* PIECELISTBOARD_H */

