/* 
 * File:   Setup.h
 * Author: Steve James <SD.James@outlook.com>
 *
 * Namespace used to setup data structures when the program first starts
 * 
 * Created on 25 October 2014, 1:22 PM
 */

#ifndef SETUP_H
#define	SETUP_H

#include "GenerateMagics.h"

/**
 * Namespace used to setup data structures when the program first starts
 */
namespace Setup {

    /**
     * Creates the direction table which specifies the direction between 
     * a given pair of squares
     * @return the direction table
     */
    std::array<std::array<Direction, 64>, 64> generateDirectionTable();

    /**
     * Generates the magic rook moves
     * @return the magic rook moves
     */
    std::array<std::array<Bitboard, 4096>, 64> generateRookMoves();

    /**
     * Generates the magic bishop moves
     * @return the magic bishop moves
     */
    std::array<std::array<Bitboard, 512>, 64> generateBishopMoves();
}


#endif	/* SETUP_H */

