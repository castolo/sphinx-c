
/* 
 * File:   Board.cpp
 * Author: 475531
 * 
 * Created on 11 April 2013, 8:37 AM
 */

#include "Board.h"
#include <cstdarg>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <algorithm>
#include <iterator>
#include <queue>
#include "Utils.h"
#include "StaggeredMoves.h"

std::array<int, 7> Board::getPieces(int side) {
    std::array<int, 7> pieces;
    pieces[0] = 0;
    if (side == WHITE) {
        pieces[KING] = bitboard::getSparseBitCount(WHITE_KING_BOARD);
        pieces[QUEEN] = bitboard::getSparseBitCount(WHITE_QUEEN_BOARD);
        pieces[ROOK] = bitboard::getSparseBitCount(WHITE_ROOK_BOARD);
        pieces[BISHOP] = bitboard::getSparseBitCount(WHITE_BISHOP_BOARD);
        pieces[KNIGHT] = bitboard::getSparseBitCount(WHITE_KNIGHT_BOARD);
        pieces[PAWN] = bitboard::getSparseBitCount(WHITE_PAWN_BOARD);

    } else {
        pieces[KING] = bitboard::getSparseBitCount(BLACK_KING_BOARD);
        pieces[QUEEN] = bitboard::getSparseBitCount(BLACK_QUEEN_BOARD);
        pieces[ROOK] = bitboard::getSparseBitCount(BLACK_ROOK_BOARD);
        pieces[BISHOP] = bitboard::getSparseBitCount(BLACK_BISHOP_BOARD);
        pieces[KNIGHT] = bitboard::getSparseBitCount(BLACK_KNIGHT_BOARD);
        pieces[PAWN] = bitboard::getSparseBitCount(BLACK_PAWN_BOARD);
    }
    return pieces;
}

Hash Board::getMaterialKey() const {
    Hash hash = 0;
    // todo fix this
    //    Hash** pieces = new Hash*[2];
    //    pieces[0] = getBitboards(WHITE);
    //    pieces[1] = getBitboards(BLACK);
    //
    //    for (int i = 0; i < 2; i++) {
    //        for (int j = 5; j > 0; j--) { //from pawn to queen
    //            while (pieces[i][j]) {
    //                int pos = bitboard::LS1B(pieces[i][j]);
    //                hash ^= piecesHash[5 - j][i][63 - pos]
    //                pieces[i][j] &= pieces[i][j] - 1;
    //            }
    //        }
    //    }
    //    for (int i = 0; i < 2; ++i) {
    //        delete [] pieces[i];
    //    }
    //    delete [] pieces;
    return hash;
}

Bitboard Board::attacksTo(int pos, int bySide) const {
    if (bySide == WHITE) {
        return (OCCUPANCY_MASK_KING[pos] & WHITE_KING_BOARD) |
                (OCCUPANCY_MASK_KNIGHT[pos] & WHITE_KNIGHT_BOARD) |
                (WHITE_PAWN_BOARD & BLACK_PAWN_ATTACKS[pos]) |
                (getRookMoves(pos, BLACK_PIECES_BOARD) & (WHITE_ROOK_BOARD | WHITE_QUEEN_BOARD)) |
                (getBishopMoves(pos, BLACK_PIECES_BOARD) & (WHITE_BISHOP_BOARD | WHITE_QUEEN_BOARD));
    } else {
        return (OCCUPANCY_MASK_KING[pos] & BLACK_KING_BOARD) |
                (OCCUPANCY_MASK_KNIGHT[pos] & BLACK_KNIGHT_BOARD) |
                (BLACK_PAWN_BOARD & WHITE_PAWN_ATTACKS[pos]) |
                (getRookMoves(pos, WHITE_PIECES_BOARD) & (BLACK_ROOK_BOARD | BLACK_QUEEN_BOARD)) |
                (getBishopMoves(pos, WHITE_PIECES_BOARD) & (BLACK_BISHOP_BOARD | BLACK_QUEEN_BOARD));
    }
}

bool Board::isSquareAttacked(int idx, int bySide) const {

    assert(std::abs(bySide) == 1);

    Bitboard knight, pawn, king, pawnAttack, rook, bishop, friendly;
    if (bySide == WHITE) {
        friendly = BLACK_PIECES_BOARD;
        knight = WHITE_KNIGHT_BOARD;
        pawn = WHITE_PAWN_BOARD;
        king = WHITE_KING_BOARD;
        rook = WHITE_ROOK_BOARD | WHITE_QUEEN_BOARD;
        bishop = WHITE_BISHOP_BOARD | WHITE_QUEEN_BOARD;
        pawnAttack = BLACK_PAWN_ATTACKS[idx];
    } else {
        friendly = WHITE_PIECES_BOARD;
        knight = BLACK_KNIGHT_BOARD;
        pawn = BLACK_PAWN_BOARD;
        king = BLACK_KING_BOARD;
        rook = BLACK_ROOK_BOARD | BLACK_QUEEN_BOARD;
        bishop = BLACK_BISHOP_BOARD | BLACK_QUEEN_BOARD;
        pawnAttack = WHITE_PAWN_ATTACKS[idx];
    }

    // <editor-fold defaultstate="collapsed" desc="Rook+Queen Attacks">
    if (getRookMoves(idx, friendly) & rook) {
        return true;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Bishop+Queen Attacks">
    if (getBishopMoves(idx, friendly) & bishop) {
        return true;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Knight Attacks">
    if (OCCUPANCY_MASK_KNIGHT[idx] & knight) {
        return true;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Pawn Attacks">
    if (pawn & pawnAttack) {
        return true;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="King Attacks">
    if (OCCUPANCY_MASK_KING[idx] & king) {
        return true;
    }
    // </editor-fold>

    return false;
}

bool Board::canMakeNullMove() {

    if (isInCheck()) {
        return false;
    }

    if (side == WHITE) {
        if (whitePieceListCount[ROOK] + whitePieceListCount[QUEEN] > 0) {
            return true;
        }
        return whitePieceListCount[KNIGHT] + whitePieceListCount[BISHOP] > 1;
    } else {
        if (blackPieceListCount[ROOK] + blackPieceListCount[QUEEN] > 0) {
            return true;
        }
        return blackPieceListCount[KNIGHT] + blackPieceListCount[BISHOP] > 1;
    }
}

short Board::getRepetitions() const {

    const Hash hash = getHash();
    short count = 0;
    for (size_t i = 0; i < repetitions.size(); ++i) {
        if (repetitions[i] == hash) {
            count++;
        }
    }
    return count;

}

// <editor-fold defaultstate="collapsed" desc="Make Move Function">

bool Board::makeMove(Move move, bool isLegal) {

    History currentBoardInfo;
    currentBoardInfo.blackCastle = blackCastle;
    currentBoardInfo.whiteCastle = whiteCastle;
    currentBoardInfo.enPassant = enPassant;
    currentBoardInfo.fiftyMove = fiftyRule;


    int idx = (side == WHITE) ? 0 : 1;
    if (enPassant != -1) {
        updateHash(enPassantHash[enPassant]);
    }

    enPassant = -1;
    if (move == NULL_MOVE) {
        swapSides();
        history[histIndex++] = currentBoardInfo;
        return true;
    }
    int source = Utils::getSource(move);
    int dest = Utils::getDestination(move);
    int cp = board[dest];
    int mp = board[source];
    int origSide = side;

    if (abs(cp) == KING) {
        currentBoardInfo.capturedPiece = static_cast<Piece> (cp);
        swapSides();
        history[histIndex++] = currentBoardInfo;
        return false;
    }

    updateHash(whiteCastleHash[whiteCastle - WHITE_CASTLE_NONE]);
    updateHash(blackCastleHash[blackCastle - BLACK_CASTLE_NONE]);

    Bitboard defend;
    if ((abs(mp) != PAWN) && (cp == EMPTY_SQUARE)) {
        fiftyRule++;
    } else {
        fiftyRule = 0;
    }
    if (mp < 0) {
        moves++;
    }
    int type = Utils::getFlags(move);

    if (type == MOVE_NORMAL && cp == EMPTY_SQUARE) {
        updateHash(piecesHash[abs(mp) - 1][idx][source]);
        updateHash(piecesHash[abs(mp) - 1][idx][dest]);
    }

    switch (type) {
        case MOVE_NORMAL:
            if (cp != EMPTY_SQUARE) {
                updateHash(piecesHash[abs(mp) - 1][idx][source]);
                updateHash(piecesHash[abs(cp) - 1][abs(idx - 1)][dest]);
                updateHash(piecesHash[abs(mp) - 1][idx][dest]);
                removePiece(cp, dest);

            }
            movePiece(mp, dest, source);
            if (abs(mp) == PAWN && abs(dest - source) == 16) {
                enPassant = (source + dest) / 2;
            }
            break;
        case MOVE_SHORT_CASTLE:
            updateHash(piecesHash[abs(mp) - 1][idx][source]);
            updateHash(piecesHash[abs(mp) - 1][idx][dest]);
            if (mp == WHITE_KING) {
                updateHash(piecesHash[ROOK - 1][idx][63]);
                updateHash(piecesHash[ROOK - 1][idx][61]);
                movePiece(WHITE_ROOK, F1, H1);
                movePiece(WHITE_KING, G1, E1);
                whiteCastle = WHITE_CASTLE_NONE;
            } else if (mp == BLACK_KING) {


                updateHash(piecesHash[ROOK - 1][idx][7]);
                updateHash(piecesHash[ROOK - 1][idx][5]);

                movePiece(BLACK_ROOK, F8, H8);
                movePiece(BLACK_KING, G8, E8);

                blackCastle = BLACK_CASTLE_NONE;
            }
            break;


        case MOVE_LONG_CASTLE:

            updateHash(piecesHash[abs(mp) - 1][idx][source]);
            updateHash(piecesHash[abs(mp) - 1][idx][dest]);

            if (mp == WHITE_KING) {

                updateHash(piecesHash[ROOK - 1][idx][56]);
                updateHash(piecesHash[ROOK - 1][idx][59]);

                movePiece(WHITE_ROOK, D1, A1);
                movePiece(WHITE_KING, C1, E1);

                whiteCastle = WHITE_CASTLE_NONE;
            } else if (mp == BLACK_KING) {

                updateHash(piecesHash[ROOK - 1][idx][0]);
                updateHash(piecesHash[ROOK - 1][idx][3]);

                movePiece(BLACK_ROOK, D8, A8);
                movePiece(BLACK_KING, C8, E8);
                blackCastle = BLACK_CASTLE_NONE;

            }
            break;

        case MOVE_CAPTURE_EN_PASSANT:

            updateHash(piecesHash[abs(mp) - 1][idx][source]);
            updateHash(piecesHash[abs(mp) - 1][idx][dest]);
            movePiece(mp, dest, source);
            if (mp == WHITE_PAWN) {
                removePiece(BLACK_PAWN, dest + 8);
                //Remove black pawn
                updateHash(piecesHash[PAWN - 1][abs(idx - 1)][dest + 8]);

            } else {
                removePiece(WHITE_PAWN, dest - 8);
                updateHash(piecesHash[PAWN - 1][abs(idx - 1)][dest - 8]);

            }
            break;
        default:
            //PROMOTION
            updateHash(piecesHash[PAWN - 1][idx][source]);
            if (cp != EMPTY_SQUARE) {
                removePiece(cp, dest);
                updateHash(piecesHash[abs(cp) - 1][abs(idx - 1)][dest]);

            }

            removePiece(mp, source);
            switch (type) {
                case MOVE_PROMOTION_QUEEN:
                    updateHash(piecesHash[QUEEN - 1][idx][dest]);
                    if (side == WHITE) {
                        WHITE_QUEEN_BOARD |= bitboard::getBitBoard(63 - dest);
                        WHITE_PIECES_BOARD |= WHITE_QUEEN_BOARD;
                        PieceListBoard::addWhitePiece(QUEEN, dest);
                    } else {
                        BLACK_QUEEN_BOARD |= bitboard::getBitBoard(63 - dest);
                        BLACK_PIECES_BOARD |= BLACK_QUEEN_BOARD;
                        PieceListBoard::addBlackPiece(QUEEN, dest);
                    }
                    board[dest] = QUEEN * (side);
                    break;
                case MOVE_PROMOTION_ROOK:
                    updateHash(piecesHash[ROOK - 1][idx][dest]);
                    if (side == WHITE) {
                        WHITE_ROOK_BOARD |= bitboard::getBitBoard(63 - dest);
                        WHITE_PIECES_BOARD |= WHITE_ROOK_BOARD;
                        PieceListBoard::addWhitePiece(ROOK, dest);
                    } else {
                        BLACK_ROOK_BOARD |= bitboard::getBitBoard(63 - dest);
                        BLACK_PIECES_BOARD |= BLACK_ROOK_BOARD;
                        PieceListBoard::addBlackPiece(ROOK, dest);
                    }
                    board[dest] = ROOK * (side);
                    break;
                case MOVE_PROMOTION_BISHOP:
                    updateHash(piecesHash[BISHOP - 1][idx][dest]);
                    if (side == WHITE) {
                        WHITE_BISHOP_BOARD |= bitboard::getBitBoard(63 - dest);
                        WHITE_PIECES_BOARD |= WHITE_BISHOP_BOARD;
                        PieceListBoard::addWhitePiece(BISHOP, dest);

                    } else {
                        BLACK_BISHOP_BOARD |= bitboard::getBitBoard(63 - dest);
                        BLACK_PIECES_BOARD |= BLACK_BISHOP_BOARD;
                        PieceListBoard::addBlackPiece(BISHOP, dest);
                    }
                    board[dest] = BISHOP * (side);
                    break;
                case MOVE_PROMOTION_KNIGHT:
                    updateHash(piecesHash[KNIGHT - 1][idx][dest]);
                    if (side == WHITE) {
                        WHITE_KNIGHT_BOARD |= bitboard::getBitBoard(63 - dest);
                        WHITE_PIECES_BOARD |= WHITE_KNIGHT_BOARD;
                        PieceListBoard::addWhitePiece(KNIGHT, dest);
                    } else {
                        BLACK_KNIGHT_BOARD |= bitboard::getBitBoard(63 - dest);
                        BLACK_PIECES_BOARD |= BLACK_KNIGHT_BOARD;
                        PieceListBoard::addBlackPiece(KNIGHT, dest);
                    }
                    board[dest] = KNIGHT * (side);
                    break;
            }
            break;
    }

    // <editor-fold defaultstate="collapsed" desc="Determine Castling Rights">

    if (whiteCastle != WHITE_CASTLE_NONE
            || blackCastle != BLACK_CASTLE_NONE) {

        if (board[E1] != WHITE_KING) {
            whiteCastle = WHITE_CASTLE_NONE;
        }
        if (board[E8] != BLACK_KING) {
            blackCastle = BLACK_CASTLE_NONE;
        }
        if (board[A1] != WHITE_ROOK) {
            if (whiteCastle == WHITE_CASTLE_BOTH
                    || whiteCastle == WHITE_CASTLE_SHORT) {
                whiteCastle = WHITE_CASTLE_SHORT;
            } else {
                whiteCastle = WHITE_CASTLE_NONE;
            }
        }
        if (board[H1] != WHITE_ROOK) {
            if (whiteCastle == WHITE_CASTLE_BOTH
                    || whiteCastle == WHITE_CASTLE_LONG) {
                whiteCastle = WHITE_CASTLE_LONG;
            } else {
                whiteCastle = WHITE_CASTLE_NONE;
            }
        }

        if (board[A8] != BLACK_ROOK) {
            if (blackCastle == BLACK_CASTLE_BOTH
                    || blackCastle == BLACK_CASTLE_SHORT) {
                blackCastle = BLACK_CASTLE_SHORT;
            } else {
                blackCastle = BLACK_CASTLE_NONE;
            }
        }

        if (board[H8] != BLACK_ROOK) {
            if (blackCastle == BLACK_CASTLE_BOTH
                    || blackCastle == BLACK_CASTLE_LONG) {
                blackCastle = BLACK_CASTLE_LONG;
            } else {
                blackCastle = BLACK_CASTLE_NONE;
            }
        }
    }

    // </editor-fold>

    updateHash(whiteCastleHash[whiteCastle - WHITE_CASTLE_NONE]);
    updateHash(blackCastleHash[blackCastle - BLACK_CASTLE_NONE]);

    if (enPassant != -1) {
        updateHash(enPassantHash[enPassant]);
    }

    currentBoardInfo.capturedPiece = static_cast<Piece> (cp);
    swapSides();
    history[histIndex++] = currentBoardInfo;
    repetitions.push_back(HashBoard::getHash());
    ALL_PIECES_BOARD = BLACK_PIECES_BOARD | WHITE_PIECES_BOARD;
    EMPTY_BOARD = ~ALL_PIECES_BOARD;

    if (isLegal) {
        return true;
    }
    // <editor-fold defaultstate="collapsed" desc="Determine Move Legality">

    if (type == MOVE_SHORT_CASTLE) {
        if (origSide == WHITE) {
            if (((BLACK_PAWN_BOARD & PREVENT_WHITE_SHORT_CASTLE))) {
                return false;
            }
        } else {
            if (((WHITE_PAWN_BOARD & PREVENT_BLACK_SHORT_CASTLE))) {
                return false;
            }
        }
        defend = (origSide == WHITE) ? E1F1G1_MASK : E8F8G8_MASK;

        return !areSquaresAttacked(defend, -origSide);

    } else if (type == MOVE_LONG_CASTLE) {
        if (origSide == WHITE) {
            if ((BLACK_PAWN_BOARD & PREVENT_WHITE_LONG_CASTLE)) {
                return false;
            }
        } else {
            if ((WHITE_PAWN_BOARD & PREVENT_BLACK_LONG_CASTLE)) {
                return false;
            }
        }
        defend = (origSide == WHITE) ? C1D1E1_MASK : C8D8E8_MASK;
        return !areSquaresAttacked(defend, -origSide);
    }

    return (origSide == WHITE) ? !isSquareAttacked(whiteKing, -origSide) : !isSquareAttacked(blackKing, -origSide);
    // </editor-fold>

}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Undo Move Function">

void Board::undoMove(Move move) {
    History previousBoardInfo = history[--histIndex];
    if ((move == NULL_MOVE) || (abs(previousBoardInfo.capturedPiece)) == KING) {
        enPassant = previousBoardInfo.enPassant;
        whiteCastle = previousBoardInfo.whiteCastle;
        blackCastle = previousBoardInfo.blackCastle;
        fiftyRule = previousBoardInfo.fiftyMove;
        if (enPassant != -1) {
            updateHash(enPassantHash[enPassant]);
        }
        swapSides();
        return;
    }

    repetitions.pop_back();
    if (enPassant != -1) {
        updateHash(enPassantHash[enPassant]);
    }

    updateHash(whiteCastleHash[whiteCastle - WHITE_CASTLE_NONE]);
    updateHash(blackCastleHash[blackCastle - BLACK_CASTLE_NONE]);
    enPassant = previousBoardInfo.enPassant;
    whiteCastle = previousBoardInfo.whiteCastle;
    blackCastle = previousBoardInfo.blackCastle;
    fiftyRule = previousBoardInfo.fiftyMove;

    if (enPassant != -1) {
        updateHash(enPassantHash[enPassant]);
    }

    updateHash(whiteCastleHash[whiteCastle - WHITE_CASTLE_NONE]);
    updateHash(blackCastleHash[blackCastle - BLACK_CASTLE_NONE]);

    swapSides();

    int idx = side == WHITE ? 0 : 1;
    int source = Utils::getSource(move);
    int dest = Utils::getDestination(move);
    int movingPiece = board[dest];
    int type = Utils::getFlags(move);

    if (movingPiece < 0) {
        --moves;
    }

    switch (type) {
        case MOVE_SHORT_CASTLE:

            updateHash(piecesHash[abs(movingPiece) - 1][idx][source]);
            updateHash(piecesHash[abs(movingPiece) - 1][idx][dest]);

            if (movingPiece == WHITE_KING) {

                updateHash(piecesHash[ROOK - 1][idx][F1]);
                updateHash(piecesHash[ROOK - 1][idx][H1]);
                movePiece(WHITE_KING, E1, G1);
                movePiece(WHITE_ROOK, H1, F1);
            } else if (movingPiece == BLACK_KING) {

                updateHash(piecesHash[ROOK - 1][idx][F8]);
                updateHash(piecesHash[ROOK - 1][idx][H8]);
                movePiece(BLACK_KING, E8, G8);
                movePiece(BLACK_ROOK, H8, F8);
            }
            break;
        case MOVE_LONG_CASTLE:

            updateHash(piecesHash[abs(movingPiece) - 1][idx][source]);
            updateHash(piecesHash[abs(movingPiece) - 1][idx][dest]);

            if (movingPiece == WHITE_KING) {


                updateHash(piecesHash[ROOK - 1][idx][A1]);
                updateHash(piecesHash[ROOK - 1][idx][D1]);

                movePiece(WHITE_KING, E1, C1);
                movePiece(WHITE_ROOK, A1, D1);

            } else if (movingPiece == BLACK_KING) {

                updateHash(piecesHash[ROOK - 1][idx][D8]);
                updateHash(piecesHash[ROOK - 1][idx][A8]);

                movePiece(BLACK_KING, E8, C8);
                movePiece(BLACK_ROOK, A8, D8);
            }
            break;
        case MOVE_CAPTURE_EN_PASSANT:

            movePiece(movingPiece, source, dest);

            updateHash(piecesHash[abs(movingPiece) - 1][idx][source]);
            updateHash(piecesHash[abs(movingPiece) - 1][idx][dest]);

            if (side == WHITE) {

                addPiece(BLACK_PAWN, dest + 8);
                updateHash(piecesHash[PAWN - 1][abs(idx - 1)][dest + 8]);
            } else {

                addPiece(WHITE_PAWN, dest - 8);
                updateHash(piecesHash[PAWN - 1][abs(idx - 1)][dest - 8 ]);
            }
            break;

        default:
        {
            //HERE! Cant work out what captured piece was without having saved it in history       int cp = board[];
            int capturedPiece = previousBoardInfo.capturedPiece;
            switch (type) {
                case MOVE_NORMAL:
                    updateHash(piecesHash[abs(movingPiece) - 1][idx][source]);
                    updateHash(piecesHash[abs(movingPiece) - 1][idx][dest]);

                    if (capturedPiece == EMPTY_SQUARE) {
                        movePiece(movingPiece, source, dest);
                    } else {
                        updateHash(piecesHash[abs(capturedPiece) - 1][abs(idx - 1)][dest]);
                        movePiece(movingPiece, source, dest);
                        addPiece(capturedPiece, dest);
                    }
                    break;

                case MOVE_PROMOTION_QUEEN:
                    undoPromotion(source, dest, QUEEN, capturedPiece, idx);
                    break;
                case MOVE_PROMOTION_ROOK:
                    undoPromotion(source, dest, ROOK, capturedPiece, idx);
                    break;
                case MOVE_PROMOTION_BISHOP:
                    undoPromotion(source, dest, BISHOP, capturedPiece, idx);
                    break;
                case MOVE_PROMOTION_KNIGHT:
                    undoPromotion(source, dest, KNIGHT, capturedPiece, idx);
                    break;
            }
        }
    }

    ALL_PIECES_BOARD = BLACK_PIECES_BOARD | WHITE_PIECES_BOARD;
    EMPTY_BOARD = ~ALL_PIECES_BOARD;
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Return FEN-string">

std::string Board::getPosition() const {

    std::string fen = "";
    int count = 0;
    int spaces;
    for (int i = 0; i < 8; i++) {
        spaces = 0;
        for (int j = 0; j < 8; j++) {
            switch (board[count]) {
                case WHITE_KING:
                    if (spaces > 0) {
                        fen += std::to_string(spaces);
                        spaces = 0;
                    }
                    fen += "K";
                    break;
                case BLACK_KING:
                    if (spaces > 0) {
                        fen += std::to_string(spaces);
                        spaces = 0;
                    }
                    fen += "k";
                    break;
                case WHITE_QUEEN:
                    if (spaces > 0) {
                        fen += std::to_string(spaces);
                        spaces = 0;
                    }
                    fen += "Q";
                    break;
                case BLACK_QUEEN:
                    if (spaces > 0) {
                        fen += std::to_string(spaces);
                        spaces = 0;
                    }
                    fen += "q";
                    break;
                case WHITE_ROOK:
                    if (spaces > 0) {
                        fen += std::to_string(spaces);
                        spaces = 0;
                    }
                    fen += "R";
                    break;
                case BLACK_ROOK:
                    if (spaces > 0) {
                        fen += std::to_string(spaces);
                        spaces = 0;
                    }
                    fen += "r";
                    break;
                case WHITE_BISHOP:
                    if (spaces > 0) {
                        fen += std::to_string(spaces);
                        spaces = 0;
                    }
                    fen += "B";
                    break;
                case BLACK_BISHOP:
                    if (spaces > 0) {
                        fen += std::to_string(spaces);
                        spaces = 0;
                    }
                    fen += "b";
                    break;
                case WHITE_KNIGHT:
                    if (spaces > 0) {
                        fen += std::to_string(spaces);
                        spaces = 0;
                    }
                    fen += "N";
                    break;
                case BLACK_KNIGHT:
                    if (spaces > 0) {
                        fen += std::to_string(spaces);
                        spaces = 0;
                    }
                    fen += "n";
                    break;
                case WHITE_PAWN:
                    if (spaces > 0) {
                        fen += std::to_string(spaces);
                        spaces = 0;
                    }
                    fen += "P";
                    break;
                case BLACK_PAWN:
                    if (spaces > 0) {
                        fen += std::to_string(spaces);
                        spaces = 0;
                    }
                    fen += "p";
                    break;
                default:
                    spaces++;
            }
            count++;
        }
        if (spaces > 0) {
            fen += std::to_string(spaces);
            spaces = 0;
        }
        if (count < 63) {
            fen += "/";
        }
    }
    fen += " ";

    fen += (side == WHITE) ? "w " : "b ";

    if ((whiteCastle == WHITE_CASTLE_NONE) && (blackCastle == BLACK_CASTLE_NONE)) {
        fen += "-";
    } else {
        if (whiteCastle == WHITE_CASTLE_BOTH) {
            fen += "KQ";
        } else if (whiteCastle == WHITE_CASTLE_SHORT) {
            fen += "K";
        } else if (whiteCastle == WHITE_CASTLE_LONG) {
            fen += "Q";
        }
        if (blackCastle == BLACK_CASTLE_BOTH) {
            fen += "kq";
        } else if (blackCastle == BLACK_CASTLE_SHORT) {
            fen += "k";
        } else if (blackCastle == BLACK_CASTLE_LONG) {
            fen += "q";
        }
    }
    fen += " ";

    if (enPassant == -1) {
        fen += "-";
    } else {
        char c = 'a' + (enPassant % 8);
        fen += c;

        c = '8' - (enPassant / 8);
        fen += c;
    }
    fen += " " + std::to_string(fiftyRule) + " " + std::to_string(moves);

    return fen;
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="FEN-string setup">

void Board::setPosition(std::string fen) {
    std::vector<std::string> arr;
    std::istringstream iss(fen);
    std::copy(std::istream_iterator<std::string > (iss),
            std::istream_iterator<std::string > (),
            std::back_inserter<std::vector<std::string> >(arr));

    WHITE_PAWN_BOARD = BLACK_PAWN_BOARD = WHITE_KNIGHT_BOARD = BLACK_KNIGHT_BOARD =
            WHITE_BISHOP_BOARD = BLACK_BISHOP_BOARD = WHITE_ROOK_BOARD = BLACK_ROOK_BOARD =
            WHITE_QUEEN_BOARD = BLACK_QUEEN_BOARD = WHITE_KING_BOARD = BLACK_KING_BOARD =
            WHITE_PIECES_BOARD = BLACK_PIECES_BOARD = ALL_PIECES_BOARD = 0;

    whiteCastle = WHITE_CASTLE_NONE;
    blackCastle = BLACK_CASTLE_NONE;

    hash = 0;
    std::string position = arr[0];
    side = (arr[1] == "w") ? WHITE : BLACK;
    std::string castling = arr[2];
    std::string ep = arr[3];
    fiftyRule = atoi(arr[4].c_str());
    moves = atoi(arr[5].c_str());

    PieceListBoard::clear();

    board.fill(0);

    int pos = 0;
    for (std::string::size_type i = 0; i < position.size(); i++) {


        if (position[i] != '/') {
            shiftPieceBoards(1);
            switch (position[i]) {
                case 'K':
                    PieceListBoard::addWhitePiece(KING, pos);
                    board[pos] = WHITE_KING;
                    WHITE_KING_BOARD |= 1;
                    updateHash(piecesHash[KING - 1][0][pos]);
                    break;
                case 'k':
                    PieceListBoard::addBlackPiece(KING, pos);
                    board[pos] = BLACK_KING;
                    BLACK_KING_BOARD |= 1;
                    updateHash(piecesHash[KING - 1][1][pos]);
                    break;
                case 'Q':
                    PieceListBoard::addWhitePiece(QUEEN, pos);
                    board[pos] = WHITE_QUEEN;
                    WHITE_QUEEN_BOARD |= 1;
                    updateHash(piecesHash[QUEEN - 1][0][pos]);
                    break;
                case 'q':
                    PieceListBoard::addBlackPiece(QUEEN, pos);
                    board[pos] = BLACK_QUEEN;
                    BLACK_QUEEN_BOARD |= 1;
                    updateHash(piecesHash[QUEEN - 1][1][pos]);
                    break;
                case 'R':
                    PieceListBoard::addWhitePiece(ROOK, pos);
                    board[pos] = WHITE_ROOK;
                    WHITE_ROOK_BOARD |= 1;
                    updateHash(piecesHash[ROOK - 1][0][pos]);
                    break;
                case 'r':
                    PieceListBoard::addBlackPiece(ROOK, pos);
                    board[pos] = BLACK_ROOK;
                    BLACK_ROOK_BOARD |= 1;
                    updateHash(piecesHash[ROOK - 1][1][pos]);
                    break;
                case 'B':
                    PieceListBoard::addWhitePiece(BISHOP, pos);
                    board[pos] = WHITE_BISHOP;
                    WHITE_BISHOP_BOARD |= 1;
                    updateHash(piecesHash[BISHOP - 1][0][pos]);
                    break;
                case 'b':
                    PieceListBoard::addBlackPiece(BISHOP, pos);
                    board[pos] = BLACK_BISHOP;
                    BLACK_BISHOP_BOARD |= 1;
                    updateHash(piecesHash[BISHOP - 1][1][pos]);
                    break;
                case 'N':
                    PieceListBoard::addWhitePiece(KNIGHT, pos);
                    board[pos] = WHITE_KNIGHT;
                    WHITE_KNIGHT_BOARD |= 1;
                    updateHash(piecesHash[KNIGHT - 1][0][pos]);
                    break;
                case 'n':
                    PieceListBoard::addBlackPiece(KNIGHT, pos);
                    board[pos] = BLACK_KNIGHT;
                    BLACK_KNIGHT_BOARD |= 1;
                    updateHash(piecesHash[KNIGHT - 1][1][pos]);
                    break;
                case 'P':
                    PieceListBoard::addWhitePiece(PAWN, pos);
                    board[pos] = WHITE_PAWN;
                    WHITE_PAWN_BOARD |= 1;
                    updateHash(piecesHash[PAWN - 1][0][pos]);
                    break;
                case 'p':
                    PieceListBoard::addBlackPiece(PAWN, pos);
                    board[pos] = BLACK_PAWN;
                    BLACK_PAWN_BOARD |= 1;
                    updateHash(piecesHash[PAWN - 1][1][pos]);
                    break;
                default:
                    pos += (position[i] - '0') - 1;
                    shiftPieceBoards((position[i] - '0') - 1);
                    break;
            }
            pos++;
        }
    }
    WHITE_PIECES_BOARD = WHITE_PAWN_BOARD | WHITE_KNIGHT_BOARD | WHITE_BISHOP_BOARD
            | WHITE_ROOK_BOARD | WHITE_QUEEN_BOARD | WHITE_KING_BOARD;
    BLACK_PIECES_BOARD = BLACK_PAWN_BOARD | BLACK_KNIGHT_BOARD | BLACK_BISHOP_BOARD
            | BLACK_ROOK_BOARD | BLACK_QUEEN_BOARD | BLACK_KING_BOARD;
    ALL_PIECES_BOARD = WHITE_PIECES_BOARD | BLACK_PIECES_BOARD;
    EMPTY_BOARD = ~ALL_PIECES_BOARD;

    for (std::string::size_type i = 0; i < castling.size(); i++) {

        switch (castling[i]) {

            case 'K':
                whiteCastle = WHITE_CASTLE_SHORT;
                break;
            case 'Q':
                if (whiteCastle == WHITE_CASTLE_SHORT) {
                    whiteCastle = WHITE_CASTLE_BOTH;
                } else {
                    whiteCastle = WHITE_CASTLE_LONG;
                }
                break;
            case 'k':
                blackCastle = BLACK_CASTLE_SHORT;
                break;
            case 'q':
                if (blackCastle == BLACK_CASTLE_SHORT) {
                    blackCastle = BLACK_CASTLE_BOTH;
                } else {
                    blackCastle = BLACK_CASTLE_LONG;
                }
                break;
        }
    }

    if (ep == "-") {
        enPassant = -1;
    } else {
        enPassant = (ep[0] - 'a') + RANKS * (RANKS - (ep[1] - '0'));
    }

    updateHash(whiteCastleHash[whiteCastle - WHITE_CASTLE_NONE]);
    updateHash(blackCastleHash[blackCastle - BLACK_CASTLE_NONE]);

    if (enPassant != -1) {
        updateHash(enPassantHash[enPassant]);
    }

    if (side == BLACK) {
        updateHash(sideHash);
    }

    movesPlayed.clear();
    repetitions.clear();
    repetitions.push_back(getHash());
    histIndex = 0;
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Draws the board">

std::ostream& operator<<(std::ostream& out, const Board& board) {

    for (int i = 0; i < 64; i++) {
        if (i != 0 && i % 8 == 0) {
            out << "\n";
        }
        short c = board[i];

        char k;

        switch (c) {
            case WHITE_PAWN:
                k = 'P';
                break;
            case WHITE_KNIGHT:
                k = 'N';
                break;
            case WHITE_BISHOP:
                k = 'B';
                break;
            case WHITE_KING:
                k = 'K';
                break;
            case WHITE_ROOK:
                k = 'R';
                break;
            case WHITE_QUEEN:
                k = 'Q';
                break;
            case BLACK_PAWN:
                k = 'p';
                break;
            case BLACK_KNIGHT:
                k = 'n';
                break;
            case BLACK_BISHOP:
                k = 'b';
                break;
            case BLACK_KING:
                k = 'k';
                break;
            case BLACK_ROOK:
                k = 'r';
                break;
            case BLACK_QUEEN:
                k = 'q';
                break;
            default:
                k = '.';
        }
        out << k << " ";
    }
    out << "\n";

    return out;


}

void Board::show() const {

    std::cout << (*this) << std::endl;
    return;
    std::cout << "White King: \n";
    bitboard::show(WHITE_KING_BOARD);

    std::cout << "White Queen: \n";
    bitboard::show(WHITE_QUEEN_BOARD);

    std::cout << "White Bishop: \n";
    bitboard::show(WHITE_BISHOP_BOARD);

    std::cout << "White Knight: \n";
    bitboard::show(WHITE_KNIGHT_BOARD);

    std::cout << "White Rook: \n";
    bitboard::show(WHITE_ROOK_BOARD);

    std::cout << "White Pawn: \n";
    bitboard::show(WHITE_PAWN_BOARD);

    std::cout << "Black King: \n";
    bitboard::show(BLACK_KING_BOARD);

    std::cout << "Black All: \n";
    bitboard::show(BLACK_PIECES_BOARD);

    std::cout << "Black Queen: \n";
    bitboard::show(BLACK_QUEEN_BOARD);

    std::cout << "Black Bishop: \n";
    bitboard::show(BLACK_BISHOP_BOARD);

    std::cout << "Black Knight: \n";
    bitboard::show(BLACK_KNIGHT_BOARD);

    std::cout << "Black Rook: \n";
    bitboard::show(BLACK_ROOK_BOARD);

    std::cout << "Black Pawn: \n";
    bitboard::show(BLACK_PAWN_BOARD);



}
// </editor-fold>


// <editor-fold defaultstate="collapsed" desc="Discovered Checks (DEPRECATED)">

//bool Board::isDiscoveredCheckOnHorizontal(Bitboard source, Bitboard destination, Bitboard king, Bitboard line) const {
//    //    Bitboard attack, target;
//    //    if (side == WHITE) {
//    //        attack = WHITE_ROOK_BOARD | WHITE_QUEEN_BOARD;
//    //        target = ~WHITE_PIECES_BOARD | source | (~destination);
//    //    } else {
//    //        attack = BLACK_ROOK_BOARD | BLACK_QUEEN_BOARD;
//    //        target = ~BLACK_PIECES_BOARD | source | (~destination);
//    //    }
//    //    target &= line;
//    //    attack &= line;
//    //    if (attack == 0) {
//    //        return false;
//    //    }
//    //
//    //    bool capture = (destination & ALL_PIECES_BOARD) > 0;
//    //
//    //    while (attack > 0) {
//    //        int from = 63 - bitboard::LS1B(attack); //using 63 since I indexed a8 as 0
//    //
//    //        Bitboard temp = ALL_PIECES_BOARD;
//    //
//    //        temp &= ~source;
//    //        if (!capture)
//    //            temp |= destination;
//    //        Bitboard moves = getRookMoves(from, target, temp);
//    //        if (moves & king) {
//    //
//    //            return true;
//    //        }
//    //        attack &= attack - 1;
//    //    }
//
//    return false;
//}
//
//bool Board::isDiscoveredCheckOnDiagonal(Bitboard source, Bitboard destination, Bitboard king, Bitboard diagonal) const {
//    //    Bitboard attack, target;
//    //    if (side == WHITE) {
//    //        attack = WHITE_BISHOP_BOARD | WHITE_QUEEN_BOARD;
//    //        target = ~WHITE_PIECES_BOARD | source | (~destination);
//    //    } else {
//    //        attack = BLACK_BISHOP_BOARD | BLACK_QUEEN_BOARD;
//    //        target = ~BLACK_PIECES_BOARD | source | (~destination);
//    //    }
//    //    target &= diagonal;
//    //    attack &= diagonal;
//    //    if (attack == 0) {
//    //        return false;
//    //    }
//    //
//    //    bool capture = (destination & ALL_PIECES_BOARD) > 0;
//    //    while (attack > 0) {
//    //        int from = 63 - bitboard::LS1B(attack); //using 63 since I indexed a8 as 0
//    //        Bitboard temp = ALL_PIECES_BOARD;
//    //        temp &= ~source;
//    //        if (!capture)
//    //            temp |= destination;
//    //        Bitboard moves = getBishopMoves(from, target, temp);
//    //        if (moves & king) {
//    //
//    //            return true;
//    //        }
//    //        attack &= attack - 1;
//    //    }
//
//    return false;
//}
// </editor-fold>


/*******************************************************************************/

// <editor-fold defaultstate="collapsed" desc="Bitboard Operations">

void bitboard::show(Bitboard n) {
    std::cout << toString(n) << "\n\n";
}

std::string bitboard::toString(Bitboard n) {
    const size_t size = sizeof (n)*8;
    std::string res;
    bool s = 0;
    for (size_t a = 0; a < size; ++a) {
        bool bit = n >> (size - 1);
        if (bit)
            s = 1;
        if (s)
            res.push_back(bit + '0');
        n <<= 1;
    }
    if (!res.size())
        res.push_back('0');
    while (res.size() < size) {
        res = "0" + res;
    }
    std::string ans = "";
    for (std::string::size_type i = 0; i < res.size(); i++) {

        if (i != 0 && i % 8 == 0) {

            ans += "\n";
        }
        ans += res[i];

    }
    return ans;
}

Bitboard bitboard::getInterposed(int from, int to, Direction direction) {
    int A = std::min(from, to);
    int B = std::max(from, to);
    switch (direction) {
        case Direction::NONE:
            return 0ull;
        case Direction::HORIZONTAL:
            return RIGHT_BOARD[A] & LEFT_BOARD[B];
        case Direction::VERTICAL:
            return DOWN_BOARD[A] & UP_BOARD[B];
        case Direction::LOWER_LEFT_TOP_RIGHT:
            return SW_BOARD[A] & NE_BOARD[B];
        case Direction::LOWER_RIGHT_TOP_LEFT:
            return SE_BOARD[A] & NW_BOARD[B];
    }
    return 0ull;
}

// </editor-fold>
