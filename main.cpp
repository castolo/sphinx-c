/* 
 * File:   main.cpp
 * Author: Steve James
 *
 * Created on 11 April 2013, 8:00 AM
 */


#include <cstdlib>
#include <stdio.h>
#include <string>
#include<fstream>
#include<sstream>
#include <ctime>
#include <vector>
#include <algorithm> 
#include <functional> 
#include <cctype>
#include <locale>
#include <unordered_map>
#include <stdlib.h>
#include <iostream>
#include <thread>
#include "Board.h"
#include "Sphinx.h"
#include "Tablebase.h"
#include "StaggeredMoves.h"
#include "Setup.h"
#include "PolyGlotBook.h"
#include <signal.h>
#include <chrono>
#include <memory>
#include "BoardEvaluator.h"
#include "UCIWrapper.h"



const std::string Sphinx::VERSION = "1.0";
const std::string Sphinx::AUTHOR = "Steve James";
const std::string Sphinx::NAME = "Sphinx";
const std::string Sphinx::DATE = "(2013 - 2015)";

//Move gen failed on 8/5bk1/8/2Pp4/8/1K6/8/8 w - d6 0 1 at depth 7
//Expected = 824064, actual = 5580696
int main() {

    std::cout << Sphinx::NAME << ' ' << Sphinx::VERSION << '\n' << Sphinx::AUTHOR <<
            ' ' << Sphinx::DATE << '\n' << std::endl;
    std::ifstream file("input.txt");
    //UCIWrapper wrapper() = See 'Most Vexing Parse' :smh:
    UCIWrapper wrapper;
    wrapper.start();
    return 0;
}

