/* 
 * File:   AlternateAPI.h
 * Author: Steve James <SD.James@outlook.com>
 *
 * This class contain alternate (non-standard) functionality, such as perft and 
 * divide procedures.
 * 
 * Created on 23 December 2014, 3:07 PM
 */

#ifndef ALTERNATEAPI_H
#define	ALTERNATEAPI_H

#include "Board.h"
#include "Sphinx.h"
/**
 * This class contain alternate (non-standard) functionality, such as perft and 
 * divide procedures.
 */
class AlternateAPI {
public:

    /**
     * Performs a divide procedure to the given depth
     * @param board the board
     * @param depth the requested depth
     * @param first whether this is the first call to the function
     * @return the number of nodes found
     */
    static uint64_t divide(Board& board, int depth, bool first = true);

    /**
     * Performs a perft run on the given board
     * @param board the board
     * @param depth the requested depth
     */
    static void perft(Board& board, int depth = 4);

    /**
     * Performs a perft speed test on the given position
     * @param board the board
     * @param depth the requested perft depth
     */
    static void perftSpeed(Board& board, int depth);

    /**
     * Show the opening book's moves for the given board
     * @param board the board
     */
    static void showOpeningBook(const Board& board);

    /**
     * Perform a quiescence search at the given position
     * @param board the current board
     */
    static void qSearch(const Board& board);

private:

    /**
     * Performs a minimax on the given board
     * @param board the board
     * @param depth the requested depth
     * @return the number of nodes encountered by the minimax search
     */
    static uint64_t minimax(Board& board, int depth);



};

#endif	/* ALTERNATEAPI_H */

