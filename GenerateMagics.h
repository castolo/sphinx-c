/* 
 * File:   GenerateMagics.h
 * Author: Workstation
 *
 * Created on 28 November 2013, 9:42 PM
 */

#ifndef GENERATEMAGICS_H
#define	GENERATEMAGICS_H

#include "Constants.h"

#include "Board.h"

namespace {

    struct Entry {
        uint64_t andMask, attackMask;
    };

    // <editor-fold defaultstate="collapsed" desc="Forward Declarations">

    uint64_t unsetBit(uint64_t, int);
    uint64_t setBit(uint64_t, int);
    uint64_t indexTo64(uint64_t, int);
    void createAllBishopMasks(int, std::vector<Entry>&);
    void createAllRookMasks(int, std::vector<Entry>&);
    int mapBishop(uint64_t, uint64_t, int);
    int mapRook(uint64_t, uint64_t, int);
    bool isValidBishop(uint64_t, int, const std::vector<Entry>&);
    bool isValidRook(uint64_t, int, const std::vector<Entry>&);
    bool isValidBishop(uint64_t, int);
    bool isValidRook(uint64_t, int);
    uint64_t* getBishopMagics();
    uint64_t* getRookMagics();
    int getMax();
    void printBishopMoves();
    void printRookMoves();
    uint64_t getBishopAttacks(int, uint64_t);
    uint64_t getRookAttacks(int, uint64_t);
}

// </editor-fold>

namespace {

    uint64_t unsetBit(uint64_t b, int i) {
        return b & (~(1ull << i));
    }

    uint64_t setBit(uint64_t b, int i) {
        return b | (1ull << i);
    }

    uint64_t indexTo64(uint64_t bb, int idx) {
        uint64_t ans = 0;
        int N = bitboard::getSparseBitCount(bb);
        for (int i = 0; i < N; i++) {

            int b = bitboard::LS1B(bb);
            bb &= bb - 1;

            if (idx & (1ull << i)) {
                ans = setBit(ans, b);
            }
        }
        return ans;
    }

    uint64_t getBishopAttacks(int square, uint64_t mask) {
        return Board::getBishopAttackSet(square, mask);
    }

    uint64_t getRookAttacks(int square, uint64_t mask) {
        return Board::getRookAttackSet(square, mask);
    }

    void createAllBishopMasks(int square, std::vector<Entry>& ans) {
        uint64_t A = OCCUPANCY_MASK_BISHOP[square];
        int N = bitboard::getSparseBitCount(A);
        int length = 1 << N;
        for (int i = 0; i < length; i++) {
            Entry e;
            e.andMask = indexTo64(A, i);
            e.attackMask = getBishopAttacks(square, e.andMask);
            ans.push_back(e);
        }
    }

    void createAllRookMasks(int square, std::vector<Entry>& ans) {
        uint64_t A = OCCUPANCY_MASK_ROOK[square];
        int N = bitboard::getSparseBitCount(A);
        int length = 1 << N;
        for (int i = 0; i < length; i++) {
            Entry e;
            e.andMask = indexTo64(A, i);
            e.attackMask = getRookAttacks(square, e.andMask);
            ans.push_back(e);
        }
    }

    int mapBishop(uint64_t A, uint64_t B, int pos) {
        return static_cast<int> ((A * B) >> (MAGIC_BISHOP_SHIFTS[pos]));
    }

    int mapRook(uint64_t A, uint64_t B, int pos) {
        return static_cast<int> ((A * B) >> (MAGIC_ROOK_SHIFTS[pos]));
    }

    bool isValidBishop(uint64_t magic, int pos, const std::vector<Entry> &t) {

        const int N = 4096;
        std::array<uint64_t, N> row{0};

        for (int i = 0; i < t.size(); i++) {
            Entry e = t[i];

            int x = mapBishop(e.andMask, magic, pos);
            assert(x < N);
            if (row[x] == 0) {
                row[x] = e.attackMask;
            } else if (row[x] != e.attackMask) {
                return false;
            }
        }
        return true;

    }

    bool isValidRook(uint64_t magic, int pos, const std::vector<Entry> &t) {

        const int N = 4096;
        std::array<uint64_t, N> row{0};

        for (int i = 0; i < t.size(); i++) {
            Entry e = t[i];

            int x = mapRook(e.andMask, magic, pos);
            assert(x < N);
            if (row[x] == 0) {
                row[x] = e.attackMask;
            } else if (row[x] != e.attackMask) {
                return false;
            }
        }
        return true;

    }

    bool isValidBishop(uint64_t magic, int pos) {

        std::vector<Entry> t;
        createAllBishopMasks(pos, t);
        return isValidBishop(magic, pos, t);

    }

    bool isValidRook(uint64_t magic, int pos) {

        std::vector<Entry> t;
        createAllRookMasks(pos, t);
        return isValidRook(magic, pos, t);

    }

    uint64_t* getBishopMagics() {

        uint64_t *magics = new uint64_t[64];
        for (int i = 0; i < 64; i++) {
            std::vector<Entry> t;
            createAllBishopMasks(i, t);
            bool fail = true;
            uint64_t candidate = 0;
            while (fail) {
                candidate = Utils::random<uint64_t>() & Utils::random<uint64_t>() &
                        Utils::random<uint64_t>();
                if (isValidBishop(candidate, i, t)) {
                    fail = false;
                }
            }
            magics[i] = candidate;
        }
        return magics;
    }

    uint64_t* getRookMagics() {

        uint64_t *magics = new uint64_t[64];
        for (int i = 0; i < 64; i++) {
            std::vector<Entry> t;
            createAllRookMasks(i, t);
            bool fail = true;
            uint64_t candidate = 0;
            while (fail) {
                candidate = Utils::random<uint64_t>() & Utils::random<uint64_t>()
                        & Utils::random<uint64_t>();
                if (isValidRook(candidate, i, t)) {
                    fail = false;
                }
            }
            magics[i] = candidate;
        }
        return magics;
    }

    int getMax() {
        int max = -1;
        for (int i = 0; i < 64; i++) {

            std::vector<Entry> t;
            createAllBishopMasks(i, t);

            for (int j = 0; j < t.size(); j++) {
                Entry e = t[j];
                int x = mapBishop(e.andMask, BISHOP_MAGICS[i], i);
                if (x > max) max = x;

            }


        }
        return max;
    }

    void printBishopMoves() {

        static uint64_t moves[64][512];

        for (int i = 0; i < 64; i++) {
            for (int j = 0; j < 512; j++) {
                moves[i][j] = 0;
            }
        }

        for (int i = 0; i < 64; i++) {
            std::vector<Entry> t;
            createAllBishopMasks(i, t);
            for (int j = 0; j < t.size(); j++) {
                Entry e = t[j];
                int x = mapBishop(e.andMask, BISHOP_MAGICS[i], i);
                moves[i][x] = e.attackMask;
            }
        }

        for (int i = 0; i < 64; i++) {
            std::cout << "{";
            for (int j = 0; j < 512; j++) {

                std::cout << moves[i][j] << "ull";
                if (j != 511) {
                    std::cout << ", ";
                }
            }
            std::cout << "}";
            if (i != 63) {
                std::cout << "," << std::endl;
            }
        }


        std::cout << std::endl;

    }

    void printRookMoves() {

        static uint64_t moves[64][4096];

        for (int i = 0; i < 64; i++) {
            for (int j = 0; j < 4096; j++) {
                moves[i][j] = 0;
            }
        }

        for (int i = 0; i < 64; i++) {
            std::vector<Entry> t;
            createAllRookMasks(i, t);
            for (int j = 0; j < t.size(); j++) {
                Entry e = t[j];
                int x = mapRook(e.andMask, ROOK_MAGICS[i], i);
                moves[i][x] = e.attackMask;
            }
        }

        for (int i = 0; i < 64; i++) {
            std::cout << "{";
            for (int j = 0; j < 4096; j++) {

                std::cout << moves[i][j] << "ull";
                if (j != 4095) {
                    std::cout << ", ";
                }
            }
            std::cout << "}";
            if (i != 63) {
                std::cout << "," << std::endl;
            }
        }
        std::cout << std::endl;

    }
}


#endif	/* GENERATEMAGICS_H */