/* 
 * File:   Option.h
 * Author: Steve James <SD.James@outlook.com>
 *
 * Enum representing different engine options
 * 
 * Created on 25 December 2014, 5:37 PM
 */

#ifndef OPTION_H
#define	OPTION_H

/*
 * Enum representing different engine options
 */
enum Option : int {
    /*
     * An option specifying whether opening books are to be used
     */
    OPENING_BOOK,
    /*
     * An option specifying whether the engine should produce verbose output
     */
    VERBOSE,
    /*
     * An option specifying whether endgame tablebases are to be used
     */
    ENDGAME_TABLEBASE,
    /*
     * The number of options supported at present
     */
    NUM_OPTIONS,
};

#endif	/* OPTION_H */

