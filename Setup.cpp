#include "Setup.h"

namespace Setup {

    std::array<std::array<Direction, 64>, 64> generateDirectionTable() {
        std::array<std::array<Direction, 64>, 64> directions;
        for (int from = 0; from < 64; ++from) {
            for (int to = 0; to < 64; ++to) {
                Bitboard bb = bitboard::getBitBoard(63 - to);
                if (MASK_RANKS[from] & bb) {
                    directions[from][to] = Direction::HORIZONTAL;
                } else if (MASK_FILES[from] & bb) {
                    directions[from][to] = Direction::VERTICAL;
                } else if (MASK_A1_H8_DIAG[from] & bb) {
                    directions[from][to] = Direction::LOWER_LEFT_TOP_RIGHT;
                } else if (MASK_A8_H1_DIAG[from] & bb) {
                    directions[from][to] = Direction::LOWER_RIGHT_TOP_LEFT;
                } else {
                    directions[from][to] = Direction::NONE;
                }
            }
        }
        return directions;

    }

    std::array<std::array<Bitboard, 4096>, 64> generateRookMoves() {
        std::array<std::array<Bitboard, 4096>, 64> rookMoves;
        for (int i = 0; i < 64; ++i) {
            std::vector<Entry> t;
            createAllRookMasks(i, t);
            for (int j = 0; j < t.size(); j++) {
                Entry e = t[j];
                int x = mapRook(e.andMask, ROOK_MAGICS[i], i);
                rookMoves[i][x] = e.attackMask;
            }
        }
        return rookMoves;
    }

    std::array<std::array<Bitboard, 512>, 64> generateBishopMoves() {
        std::array<std::array<Bitboard, 512>, 64> bishopMoves;
        for (int i = 0; i < 64; ++i) {
            std::vector<Entry> s;
            createAllBishopMasks(i, s);
            for (int j = 0; j < s.size(); j++) {
                Entry e = s[j];
                int x = mapBishop(e.andMask, BISHOP_MAGICS[i], i);
                bishopMoves[i][x] = e.attackMask;
            }
        }
        return bishopMoves;
    }
}