/* 
 * File:   BoardEvaluator.h
 * Author: Steve James <SD.James@outlook.com>
 *
 * A collection of functions responsible for evaluating a board's position. 
 * Aside from implementing the evaluation function, this class also performs 
 * procedures such as SEE.
 * 
 * Created on 20 December 2014, 9:03 PM
 */

#ifndef BOARDEVALUATOR_H
#define	BOARDEVALUATOR_H

#include "Constants.h"
#include "Board.h"

class BoardEvaulator {
public:
    /**
     * Evaluates the current position, from the current side's perspective
     * @param board the board to evaluate
     * @return an evaluation of the position in centipawns.
     */
    static Evaluation evaluate(const Board& board);


    /**
     * Perform a static evaluation to calculate the outcome of a particular move
     * @param board the board
     * @param the move to be checked
     * @return the gain or loss made by playing that move
     */
    static Evaluation staticExchangeEvaluation(const Board& board, Move move);

private:

    // <editor-fold defaultstate="collapsed" desc="SEE Helpers">

    static Bitboard attacksTo(const Board& board, int square);

    static inline Bitboard getLeastValuablePiece(const Board& board, Bitboard attackers, int side, int &piece) {
        for (piece = PAWN; piece <= KING; ++piece) {
            Bitboard bb = (side == WHITE) ? board.WHITE_BITBOARDS[piece] : board.BLACK_BITBOARDS[piece];
            bb &= attackers;
            if (bb) {
                return bb & -bb; //lowest bit
            }
        }
        return 0; // empty set
    }

    static inline Bitboard considerXRays(Bitboard dest, Bitboard from,
            Bitboard all, Bitboard rookSliders, Bitboard bishopSliders) {

        int fromSquare = 63 - bitboard::LS1B(from);

        if (MASK_RANKS[fromSquare] & dest) {
            Bitboard tMoves = Board::getGeneralMagicRookMoves(all, fromSquare, dest);
            return tMoves & MASK_RANKS[fromSquare] & rookSliders;
        } else if (MASK_FILES[fromSquare] & dest) {
            Bitboard tMoves = Board::getGeneralMagicRookMoves(all, fromSquare, dest);
            return tMoves & MASK_FILES[fromSquare] & rookSliders;
        } else if (MASK_A1_H8_DIAG[fromSquare] & dest) {
            Bitboard tMoves = Board::getGeneralMagicBishopMoves(all, fromSquare, dest);
            return tMoves & MASK_A1_H8_DIAG[fromSquare] & bishopSliders;
        } else {
            Bitboard tMoves = Board::getGeneralMagicBishopMoves(all, fromSquare, dest);
            return tMoves & MASK_A8_H1_DIAG[fromSquare] & bishopSliders;
        }
    }
    // </editor-fold>


    /**
     * Evaluate the pawn structure of the board. This method should not
     * take into account material.
     * @param board the board
     * @return the difference between white and black's pawn structures
     */
    static Evaluation evaluatePawnStructure(const Board& board);

    /**
     * Evaluates the mobility and king safety of each player, scaled by the amount
     * of material on the board
     * @param board the board
     * @param whiteMaterial white's material
     * @param blackMaterial black's material
     * @return the difference between white and black's scores
     */
    static Evaluation evaluateMobilityandKingSafety(const Board& board,
            Evaluation whiteMaterial, Evaluation blackMaterial);

    /**
     * Scales the attack value by the number of attackers
     * @param attackValue the attack value
     * @param attackers the number of attackers
     * @return the scaled score
     */
    static inline Evaluation scale(int attackValue, int attackers) {
        switch (attackers) {
            case 0: return 0;
            case 1: return attackValue / 2;
            case 2: return attackValue;
            case 3: return (attackValue * 4) / 3;
            case 4: return (attackValue * 3) / 2;
            default: return attackValue * 2;
        }
    }

    /**
     * Evaluates white's king shield
     * @param board the board
     * @return the evaluation of white's king shield
     */
    static Evaluation evaluateWhiteKingShield(const Board& board);

    /**
     * Evaluates black's king shield
     * @param board the board
     * @return the evaluation of black's king shield
     */
    static Evaluation evaluateBlackKingShield(const Board& board);


};

#endif	/* BOARDEVALUATOR_H */

