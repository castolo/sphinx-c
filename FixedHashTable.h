/* 
 * File:   FixedHashTable.h
 * Author: Workstation
 *
 * Created on 09 February 2014, 12:59 PM
 */

#ifndef FIXEDHASHTABLE_H
#define	FIXEDHASHTABLE_H

#include "Utils.h"
#include <vector>
#include <tuple>

template <class K, class T>
class FixedHashTable {
public:

    FixedHashTable(int N) {
        size = 0;
        N *= 1 / LOAD; //add extra for load balancing        
        M = Utils::lg(N);
        capacity = 1 << M;
        slots.reserve(capacity);
        for (int i = 0; i < capacity; ++i) {
            slots.push_back(std::make_tuple(0, NULL, 0));
        }
        maxSize = LOAD * capacity;
    }

    inline unsigned int size() {
        return size;
    }

    inline unsigned int capacity() {
        return capacity;
    }

    
    
    
    inline void clear() {
        for (int i = 0; i < capacity; ++i) {
            delete std::get<2>(slots[i]);
            slots[i] = std::make_tuple(0, NULL, 0);
        }
        size = 0;
    }

    bool put(const K key, const T* value) {
        int32_t hc = std::hash(key);
        int32_t curr = hash1(hc);
        int32_t next = hash2(hc);
        if (size == maxSize) {
            //overwrite first entry
            slots[curr] = std::make_tuple(hc, value, next);
            return true;
        }
        for (int i = 0; i < 10; ++i) {
            std::tuple<int32_t, T*, int32_t>& elem = slots[curr];
            if (!std::get<1>(elem) || (next == -1)) {
                slots[curr] = std::make_tuple(hc, value, next);
                return true;
            } else {
                slots[curr] = std::make_tuple(hc, value, next);
                curr = std::get<3>(elem);
                value = std::get<2>(elem);
                next = -1;
            }
        }
        return false;
    }

    T* get(const K& key) const {
        int32_t hc = std::hash(key);
        int32_t h1 = hash1(hc);
        std::tuple<int32_t, T*, int32_t > val = slots[h1];
        if (std::get<1>(val) == hc) {
            return std::get<2>(val);
        }
        int32_t h2 = hash2(hc);
        val = slots[h2];
        if (std::get<1>(val) == hc) {
            return std::get<2>(val);
        }
        return NULL;
    }

private:

    const float LOAD = 0.8f;
    unsigned int size;
    unsigned int capacity;
    unsigned int maxSize;
    unsigned int M;

    std::vector<std::tuple<int32_t, T*, int32_t> > slots;

    inline int hash1(int32_t key) const {
        int mask = (1 << M) - 1;
        return key & mask;
    }

    inline int hash2(int32_t key) const {
        // const unsigned int P = 8589934583; //prime
        return hash1(2 * key + 17);
    }
};


#endif	/* FIXEDHASHTABLE_H */

