#include "UCIWrapper.h"
#include <thread>
#include "Option.h"
#include "Line.h"

void UCIWrapper::start() {

    std::string input;
    bool flag = true;
    while (flag) {
        std::getline(inputStream, input);
        std::vector<std::string> args;
        args.reserve(20);
        std::string head;
        bool valid = parseCommand(input, head, args);
        if (!valid) {
            //empty line
            continue;
        }
        //act on id, head, args accordingly
        auto it = commands.find(head);
        if (it != commands.end()) { //if supported command

            UCI uci = it->second;
            switch (uci) {

                case UCI::UCI_SUPPORT:
                {
                    identify();
                    std::cout << "uciok" << std::endl;
                    break;
                }
                case UCI::READY:
                {
                    //todo setup options not done yet
                    //no options => return immeidately
                    std::cout << "readyok" << std::endl;
                    break;
                }
                case UCI::SET_OPTION:
                {
                    assert(args.size() > 1);
                    std::string opt = args[1];
                    std::string val = "";
                    for (int i = 3; i < args.size(); ++i) {
                        val += args[i] + ' ';
                    }
                    val = Utils::trim(val);
                    if (opt == "OwnBook") {
                        engine->setOption(Option::OPENING_BOOK, (val == "true") ? true : false);
                    } else if (opt == "BookFile") {
                        engine->setBookPath(val);
                    } else {
                        std::cerr << "No option " << opt << " exists" << std::endl;
                    }
                    break;
                }
                case UCI::NEW_GAME:
                {
                    engine->reset();
                    engine->setPosition(START_POSITION);
                    board.setPosition(START_POSITION);
                    break;
                }
                case UCI::POSITION:
                {
                    int i = 2;
                    assert(!args.empty());
                    if (args[0] == "startpos") {
                        engine->setPosition(START_POSITION);
                        board.setPosition(START_POSITION);
                    } else if (args[0] == "fine70") {
                        engine->setPosition(FINE70_POSITION);
                        board.setPosition(FINE70_POSITION);
                    } else if (args[0] == "greek") {
                        engine->setPosition(GREEK_GIFT);
                        board.setPosition(GREEK_GIFT);
                    } else if (args[0] == "fen") {
                        i = 2;
                        std::string fen = args[1];
                        while (i < args.size() && args[i] != "moves") {
                            fen += " " + args[i++];
                        }
                        ++i;
                        engine->setPosition(fen);
                        board.setPosition(fen);
                    }
                    for (; i < args.size(); ++i) {
                        Move valid = engine->playMove(args[i]);
                        if (valid) {
                            board.makeMove(valid);
                        } else {
                            std::cerr << "Invalid move " << args[i] << std::endl;
                            break;
                        }

                    }
                    break;
                }
                case UCI::GO:
                {
                    Time white = 30000;
                    Time black = 30000;
                    int depth = DEFAULT_MAX_DEPTH;
                    for (int i = 0; i < args.size(); ++i) {
                        std::string arg = args[i];
                        if (arg == "wtime") {
                            white = std::stol(args[++i]);
                        } else if (arg == "btime") {
                            black = std::stol(args[++i]);
                        } else if (arg == "depth") {
                            depth = std::stoi(args[++i]);
                            white = black = std::numeric_limits<Time>::max();
                        }
                    }

                    auto l = [this, white, black, depth]() {
                        mode = Mode::THINKING;
                        engine->setMaxDepth(depth);
                        Line line = engine->search(white, black);
                        Optional<Move> move = line.getBestMove();
                        if (move && mode != Mode::QUITTING) {
                            std::cout << "bestmove " << Utils::toInputNotation(move.value()) << std::endl;
                        }
                        mode = Mode::WAITING;
                    };
                    if (inputStream == std::cin) {
                        std::thread(l).detach();
                    } else {
                        l();
                    }

                    break;
                }
                case UCI::STOP:
                {
                    if (mode == Mode::THINKING) {
                        engine->interrupt();
                    } else if (mode == Mode::DEBUGGING) {
                        std::cerr << "Can't stop non-standard functions" << std::endl;
                    }
                    break;
                }
                case UCI::PONDER_HIT:
                {
                    std::cerr << "ponderhit not yet implemented" << std::endl;
                    break;
                }
                case UCI::QUIT:
                {
                    if (mode == Mode::THINKING) {
                        mode = Mode::QUITTING;
                        engine->interrupt();
                    }
                    flag = false;
                    break;
                }
                case UCI::NS_DIVIDE:
                {
                    int depth = args.empty() ? 1 : std::stoi(args[0]);
                    auto l = [this, depth]() {
                        mode = Mode::DEBUGGING;
                        std::cout << "Nodes:" << AlternateAPI::divide(board, depth) << std::endl;
                        mode = Mode::WAITING;
                    };
                    if (inputStream == std::cin) {
                        std::thread(l).detach();
                    } else {
                        l();
                    }
                    break;
                }
                case UCI::NS_PERFT:
                {
                    int depth = args.empty() ? 4 : std::stoi(args[0]);
                    auto l = [this, depth]() {
                        mode = Mode::DEBUGGING;
                        AlternateAPI::perft(this->board, depth);
                        mode = Mode::WAITING;
                    };
                    if (inputStream == std::cin) {
                        std::thread(l).detach();
                    } else {
                        l();
                    }
                    break;
                }
                case UCI::NS_PERFT_SPEED:
                {
                    int depth = args.empty() ? 5 : std::stoi(args[0]);
                    auto l = [this, depth]() {
                        mode = Mode::DEBUGGING;
                        this->board.setPosition("r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq - 0 1");
                        AlternateAPI::perftSpeed(this->board, depth);
                        mode = Mode::WAITING;
                    };
                    if (inputStream == std::cin) {
                        std::thread(l).detach();
                    } else {
                        l();
                    }
                    break;
                }
                case UCI::NS_SHOW_BOARD:
                {
                    std::cout << board << std::endl;
                    break;
                }
                case UCI::NS_SHOW_BOOK:
                {
                    auto l = [this]() {
                        mode = Mode::DEBUGGING;
                        AlternateAPI::showOpeningBook(this->board);
                        mode = Mode::WAITING;
                    };
                    if (inputStream == std::cin) {
                        std::thread(l).detach();
                    } else {
                        l();
                    }
                    break;
                }
                case UCI::NS_QSEARCH:
                {
                    auto l = [this]() {
                        mode = Mode::DEBUGGING;
                        AlternateAPI::qSearch(board);
                        mode = Mode::WAITING;
                    };
                    if (inputStream == std::cin) {
                        std::thread(l).detach();
                    } else {
                        l();
                    }
                    break;
                }
            }

        }
    }
}

bool UCIWrapper::parseCommand(std::string command, std::string& instruction,
        std::vector<std::string>& args) const {

    std::vector<std::string> tokens = Utils::split(Utils::trim(command), ' ');
    if (tokens.empty()) {
        return false;
    }

    instruction = tokens[0];
    for (int i = 1; i < tokens.size(); ++i) {
        args.push_back(tokens[i]);
    }
    return true;
}