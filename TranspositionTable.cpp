/* 
 * File:   TranspositionTable.cpp
 * Author: Steve James
 * 
 * Created on 19 April 2013, 11:19 AM
 */

#include "TranspositionTable.h"
#include <stdlib.h>
#include <string>
#include<string.h>

std::vector<Move> TranspositionTable::extractPrincipalVariation(Board& board, int depth) {

    std::vector<Move> ans;
    Hash hash = board.getHash();
    Optional<HashEntry> entry = get(hash);

    for (; (depth > 0) && (entry) && (entry->move > 0); --depth) {
        Move move = entry->move;
        
        //check that we're not in a hash collision
        if (!board.makeMove(move)){
            //not the actual board position, but a hash collision, so exit
            board.undoMove(move);
            break;
        }        
        ans.push_back(move);
        entry = get(board.getHash());
    }
    for (int i = ans.size() - 1; i >= 0; --i) {
        board.undoMove(ans[i]);
    }
    return ans;

}