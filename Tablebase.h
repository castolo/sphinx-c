///* 
// * File:   Tablebase.h
// * Author: Workstation
// *
// * Created on 26 November 2013, 4:50 PM
// */
//
//#ifndef TABLEBASE_H
//#define	TABLEBASE_H
//
//#include "Board.h"
//
//class Tablebase {
//public:
//    /**
//     * Initialize a new Sygyzy Tablebase
//     */
//    Tablebase();
//    virtual ~Tablebase();
//    /**
//     * Use the DTZ tables to filter out moves that don't preserve the win or draw.
//     * If the position is lost, but DTZ is fairly high, only keep moves that maximise DTZ.
//     * @param position the current position
//     * @return a value of 0 indicates that not all probes were successful and 
//     * that no moves were filtered out.
//     */
//    int rootProbe(Board &position);
//
//    /**
//     * Probe the WDL table for a particular position.
//     * @param position the position
//     * @param status if *status != 0, the probe was successful.
//     * @return  The return value is from the point of view of the side to move:
//     * <ul>
//     * <li> -2 = loss </li>
//     * <li> -1 = loss, but draw under 50-move rule </li>
//     * <li> 0 = draw </li>
//     * <li> 1 = win, but draw under 50-move rule </li>
//     * <li> 2 = win </li>
//     * </ul> 
//     */
//    int probeWDL(Board &position, int *status);
//    /**
//     * Probe the DTZ table for a particular position.  The return value n can be
//     *  off by 1: a return value -n can mean a loss in n+1 ply and a return value 
//     * +n can mean a win in n+1 ply. This cannot happen for tables with positions 
//     * exactly on the "edge" of the 50-move rule.
//     * 
//     * This implies that if dtz > 0 is returned, the position is certainly
//     *  a win if dtz + 50-move-counter <= 99. Care must be taken that the engine
//     *  picks moves that preserve dtz + 50-move-counter <= 99.
//     * 
//     *  If n = 100 immediately after a capture or pawn move, then the position
//     *  is also certainly a win, and during the whole phase until the next
//     *  capture or pawn move, the inequality to be preserved is
//     *  dtz + 50-movecounter <= 100.
//     * 
//     *  In short, if a move is available resulting in dtz + 50-move-counter <= 99,
//     *  then do not accept moves leading to dtz + 50-move-counter == 100.
//     * 
//     * @param position the current position
//     * @param status If *status != 0, the probe was successful.
//     * @return The return value is from the point of view of the side to move:
//     * <ul>
//     * <li> n < -100  = </li>
//     * <li> -100 <= n < -1  = loss in n ply (assuming 50-move counter == 0)</li>
//     * <li> 0  = draw</li>
//     * <li> 1 < n <= 100 < -100  = win in n ply (assuming 50-move counter == 0)</li>
//     * <li> n < 100  = win, but draw under 50-move rule</li>
//     * </ul>
//     */
//    int probeDTZ(Board &position, int *status);
//
//private:
//
//    /**
//     * Given a position with 6 or fewer pieces, produce a text string
//     * of the form KQPvKRP, where "KQP" represents the white pieces if
//     * mirror == false and the black pieces if mirror == true.
//     * @param position the current position
//     * @param mirror the mirror
//     * @return the string representation of the position
//     */
//    std::string getPositionString(Board &position, bool mirror = false);
//    int probeWDLTable(Board&, int*);
//    int probeDTZTable(Board&, int, int*);
//    int probeAlphaBeta(Board&, int, int, int*);
//    bool hasRepeated(Board&);
//    static const int wdl_to_dtz[];
//
//};
//
//#endif	/* TABLEBASE_H */