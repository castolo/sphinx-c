/**
 * File:   Board.h
 * Author: Steve James <SD.James@outlook.com>
 * 
 * This class is responsible for implementing a Zobrist hashing scheme. It allows 
 * any inheriting class to focus on move generation and board 
 * representation, without having to also worry about its hash function.
 * The child class only has to concern itself with updating the hash when a move
 * is made or unmade.
 *
 * @since 25/12/12
 */

#ifndef HASHBOARD_H
#define	HASHBOARD_H
#include "Constants.h"
#include <array>

/*
 * This class is responsible for implementing a Zobrist hashing scheme. It allows 
 * any inheriting class to focus on move generation and board 
 * representation, without having to also worry about its hash function.
 * The child class only has to concern itself with updating the hash when a move
 * is made or unmade.
 */
class HashBoard {
    
public:
    /**
     * Creates a new HashBoard. This method loads the random number to be used
     * in Zobrist hashing and populates the arrays.
     */
    HashBoard();

    /**
     * Updates (XORs) the hash with the new value
     *
     * @param val the new value to be included in the hash
     */
    inline void updateHash(Hash val) {
        hash ^= val;
    }

    /**
     * Returns the Zobrist hash of the board
     *
     * @return the hash of the board
     */
    inline Hash getHash() const {
        return hash;
    }

protected:

    /**
     * The actual hash value of the board
     */
    Hash hash;

    /**
     * These values are used in calculating the hash when there is a possibility
     * of an en passant capture
     */
    std::array<Hash, 64> enPassantHash;
    /**
     * These values correspond to [piece_type][side][board_position]. For
     * example, if x = piecesHash[2][0][7], then x will be used in the hash if a
     * white knight is located at position 7
     */
    std::array<std::array<std::array<Hash, 64>, 2>, 6> piecesHash; // [6][2][64];
    /**
     * Values representing white's different abilities to castle - kingside,
     * queenside, both, none.
     */
    std::array<Hash, 4> whiteCastleHash;
    /**
     * Values representing black's different abilities to castle - kingside,
     * queenside, both, none.
     */
    std::array<Hash, 4> blackCastleHash;
    /**
     * Random number used only when black is playing
     */
    Hash sideHash;

};

#endif	/* HASHBOARD_H */
