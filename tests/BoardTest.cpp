#include "Test.h"
#include "Utils.h"
#include "Board.h"
#include "Setup.h"
#include <iostream>

using namespace std;
using namespace Utils;

namespace {

    Board board;

    // <editor-fold defaultstate="collapsed" desc="Test In Check Method">

    bool confirmCheck() {
        board.swapSides();
        MoveList moves = board.generatePseudoMoves();
        board.swapSides();
        for (int i = 0; Move move = moves[i]; i++) {
            if (abs(board.getCapturedPiece(move)) == KING) {
                return true;
            }
        }
        return false;
    }

    void testInCheck() {
        for (int k = 0; k < 10000; k++) {
            board.setPosition(START_POSITION);
            for (int i = 0; i < 1000; i++) {
                MoveList moves = board.generatePseudoMoves();
                int N = 0;
                for (int j = 0; moves[j]; j++) {
                    N++;
                }
                int p = 0;
                while (p < N) {
                    int next = moves[Utils::random(0, N - 1)];
                    if (board.makeMove(next)) {
                        bool A = board.isInCheck();
                        bool B = confirmCheck();
                        REQUIRE(A == B, "Claimed " + to_string(A) + " but is " + to_string(B) +
                                " in position " + board.getPosition());
                        break;
                    } else {
                        board.undoMove(next);
                    }
                    p++;
                }
            }
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Test Piece Lists">

    void testPieceList() {
        for (int k = 0; k < 1000; k++) {
            board.setPosition("qrbk2nr/3ppP2/7b/p1P1n1pP/NpP2Q2/7P/PP3P2/R1BK1BNR b - - 0 17");
            Hash start = board.getHash();
            std::stack<int> mov;
            for (int i = 0; i < 100; i++) {
                bool forward = (board.getHash() == start) || (Utils::random(0, 1) == 0);

                if (forward || mov.empty()) {

                    MoveList moves = board.generatePseudoMoves();
                    int N = 0;
                    for (int j = 0; moves[j]; j++) {
                        N++;
                    }
                    int p = 0;
                    while (p < N) {
                        int next = moves[rand() % N];
                        if (board.makeMove(next)) {
                            mov.push(next);
                            break;
                        } else {
                            board.undoMove(next);
                        }
                        p++;
                    }
                } else {

                    int back = mov.top();
                    mov.pop();
                    board.undoMove(back);

                    std::array<int, 7> a = board.getPieces(board.getSide());
                    std::array<int, 7> b;
                    if (board.getSide() == WHITE) {
                        b = board.getWhitePiecesList();
                    } else {
                        b = board.getBlackPiecesList();
                    }

                    for (int piece = PAWN; piece <= KING; piece++) {

                        stringstream err;
                        err << "COUNT ERROR\n";
                        err << "Position " << board.getPosition() << " and move "
                                << toInputNotation(back) << endl;
                        err << "Type " << piece << ": " << a[piece] << " vs " << b[piece] << endl;
                        REQUIRE(a[piece] == b[piece], err.str());
                    }
                }
            }
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Test Zobrist Hashing">

    void testZobristHash() {
        for (int k = 0; k < 10000; k++) {
            board.setPosition(START_POSITION);
            Hash start = board.getHash();
            std::stack<Hash> history, h2;
            std::stack<Move> mov;
            h2.push(board.getHash());
            history.push(start);
            for (int i = 0; i < 100; i++) {
                bool forward = (board.getHash() == start) || (Utils::random(0, 1) == 0);
                if (forward || mov.empty()) {
                    MoveList moves = board.generatePseudoMoves();
                    int N = 0;
                    for (int j = 0; moves[j]; j++) {
                        N++;
                    }
                    int p = 0;
                    while (p < N) {
                        Move next = moves[Utils::random(0, N - 1)];
                        if (board.makeMove(next)) {
                            mov.push(next);
                            h2.push(board.getHash());
                            history.push(board.getHash());
                            Board temp = board;
                            temp.setPosition(board.getPosition());
                            REQUIRE(temp.getHash() == board.getHash(), board.getPosition());
                            break;
                        } else board.undoMove(next);
                        p++;
                    }
                } else {

                    Move back = mov.top();
                    mov.pop();
                    board.undoMove(back);
                    h2.pop();
                    history.pop();
                    REQUIRE(board.getHash() == history.top());
                    REQUIRE(board.getHash() == h2.top());

                    Board temp = board;
                    temp.setPosition(board.getPosition());
                    REQUIRE(temp.getHash() == board.getHash());
                }
            }
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Perft Test">

    uint64_t miniMax2(int depth) {
        if (depth == 0) {
            return 1;
        }
        uint64_t nodes = 0;
        MoveList moves = board.generatePseudoMoves();
        for (int i = 0; Move move = moves[i]; i++) {
            bool legal = board.makeMove(move);
            if (legal) {
                nodes += miniMax2(depth - 1);
            }
            board.undoMove(move);
        }
        return nodes;
    }

    void testPerftAccuracy(int ceiling) {
        cout << "START" << endl;
        string path = "perftsuite.epd";
        ifstream file(path.c_str());
        string temp;
        REQUIRE(file.good(), "perftsuite.epd not found!");

        for (int i = 0; file.good(); i++) {
            getline(file, temp);

            vector<string> vec = Utils::split(temp, ';');
            string fen = vec[0];
            board.setPosition(fen);
            for (int j = 1; j < vec.size() && j <= ceiling; j++) {
                stringstream sstr(vec[j]);
                uint64_t expected;
                sstr >> expected;
                if (expected > 0) {
                    uint64_t actual = miniMax2(j);
                    stringstream err;
                    err << "Move gen failed on " << fen << " at depth " << j <<
                            "\nExpected = " << expected << ", actual = " << actual;
                    REQUIRE(actual == expected, err.str());
                }
            }
        }
    }
    // </editor-fold>
}

int main() {
    Test::startSuite("BoardTest");
    Test::testFunction("TestInCheckMethod", testInCheck);
    Test::testFunction("TestPieceList", testPieceList);
    //Test::testFunction("TestZobristHashing", testZobristHash);
    Test::testFunction("TestPerftAccuracy", testPerftAccuracy, 4);
    Test::endSuite();

    return 0;
}

