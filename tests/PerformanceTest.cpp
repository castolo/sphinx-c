
#include "Setup.h"


#include "Test.h"
#include "Utils.h"
#include "Board.h"
#include "Setup.h"
#include "Sphinx.h"

#include <iostream>
#include <memory>
using namespace std;
using namespace Utils;

namespace{
Board board;

// <editor-fold defaultstate="collapsed" desc="MoveGen Speed Test">

uint64_t miniMax(int depth, int64_t &gentime) {
    if (depth == 0) {
        return 1;
    }
    uint64_t nodes = 0;
    bool flg = false;
    int64_t start = Utils::getCurrentTime<std::chrono::nanoseconds>();
    MoveList moves = board.generatePseudoMoves(flg);
    int64_t end = Utils::getCurrentTime<std::chrono::nanoseconds>();
    gentime += (end - start);
    for (int i = 0; Move move = moves[i]; i++) {

        bool legal = board.makeMove(move, flg);
        if (legal) {
            nodes += miniMax(depth - 1, gentime);
        } else {
            ++nodes;
        }
        board.undoMove(move);
    }
    return nodes;
}

void movegenSpeedTest(int ceiling) {

    cout << "Starting" << endl;
    string path = "perftsuite.epd";
    ifstream file(path.c_str());
    string temp;
    long long count = 0ll;
    double time = 0;
    REQUIRE(file.good(), "perftsuite.epd not found!");

    for (int i = 0; file.good(); i++) {
        getline(file, temp);

        vector<string> vec = Utils::split(temp, ';');
        string fen = vec[0];
        board.setPosition(fen);
        for (int j = 1; j < vec.size() && j <= ceiling; j++) {

            stringstream sstr(vec[j]);
            uint64_t expected;
            sstr >> expected;
            if (expected > 0) {
                int64_t xtime = 0;
                uint64_t actual = miniMax(j, xtime);
                double e = double(xtime) / 1000000000;
                time += e;
                count += actual;
            }
        }
        cout << "\r" << i << " done";
    }
    cout << endl;
    cout << count << " moves generated" << " in " << time << "s at " << (count / time) / 1000000.0 << " mnps" << endl;

}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Perft Speed Test">

uint64_t miniMax(int depth) {
    if (depth == 0) {
        return 1;
    }
    uint64_t nodes = 0;
    MoveList moves = board.generatePseudoMoves();
    for (int i = 0; Move move = moves[i]; i++) {
        bool legal = board.makeMove(move);
        if (legal) {
            nodes += miniMax(depth - 1);
        }
        board.undoMove(move);
    }
    return nodes;
}

/**
 * Perft speed for default case currently at 37.727 seconds - must get down to 4!!!
 * @param pos
 * @param K
 */
void perftSpeedTest(string pos, int K) {
    board.setPosition(pos);
    cout << "START" << endl;
    int64_t begin = Utils::getCurrentTime();
    uint64_t actual = miniMax(K);
    int64_t end = Utils::getCurrentTime();
    double time = double(end - begin) / 1000;
    cout << actual << " nodes evaluated" << " in " << time << " at " << (actual / time) / 1000.0 << " knps" << endl;
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="ECM Test">

void ecmTest() {
    cout << "START (this could take a while)" << endl;
    string path = "ecmx.epd";
    ifstream file(path.c_str());
    string temp;
    REQUIRE(file.good(), "ecmx.epd not found!");

    int err = 0;
    int i;
    for (i = 0; file.good() && i < 10; i++) {

        unique_ptr<Sphinx> engine(new Sphinx);
        engine->setBookPath("performance.bin");

        getline(file, temp);
        vector<string> vec = Utils::split(temp, ';');

        vector<string> a = Utils::split(vec[0], ' ');

        string id = Utils::split(vec[1], ' ')[2];
        string fen = a[0] + " " + a[1] + " " + a[2] + " " + a[3] + " 0 1";
        string m = a[5];


        engine->setOption(Option::VERBOSE, false);
        engine->setPosition(fen);
        Line line = engine->search(10000 * 30,10000 * 30);
        string move = toInputNotation(line.getBestMove().value_or(0));
        if (m == move) {
            cout << id << " correct" << " (" << a[5] << ")" << endl;
        } else {
            cout << id << " incorrect" << " (" << move << " instead of " << a[5] << ")" << endl;
            err++;
        }
    }
    cout << (i - err) << " out of " << i << " correct (" << 100 * (i - err) / double(i) << "%)" << endl;
}
// </editor-fold>
}
int main() {
//    Startup::startup();
//    Test::startSuite("PerformanceTest");
//  //  Test::testFunction("MoveGenSpeedTest", movegenSpeedTest, 5);
//        Test::testFunction("PerftSpeedTest", perftSpeedTest,
//                "r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq - 0 1", 5);
//    //    Test::testFunction("ECMTest", ecmTest);
//    Test::endSuite();

    return 0;
}