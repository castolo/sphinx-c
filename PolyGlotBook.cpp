/* 
 * File:   PolyglotBook.cpp
 * Author: Workstation
 * 
 * Created on 03 December 2013, 2:43 PM
 */


#include "PolyGlotBook.h"
#include "Constants.h"
#include "Board.h"
#include <stdlib.h>
#include <iostream>
#include "Utils.h"
#include <time.h>

namespace {

    /**
     * Converts a PolyGlot move representation to our Move representation
     * @param board the board
     * @param pMove the PolyGlotMove
     * @return a move
     */
    Move convert(const Board &board, uint16_t pMove) {

        int toFile = pMove & 7;
        int toRow = (pMove >> 3) & 7;
        int fromFile = (pMove >> 6) & 7;
        int fromRow = (pMove >> 9) & 7;
        int prom = (pMove >> 12) & 7;
        int from = (7 - fromRow) * 8 + fromFile;
        int to = (7 - toRow) * 8 + toFile;
        int type = MOVE_NORMAL;

        switch (prom) {
            case 0:
                if ((to == board.getEnPassant()) && (abs(board[from]) == PAWN)) {
                    type = MOVE_CAPTURE_EN_PASSANT;
                } else {
                    if (board.getSide() == WHITE) {
                        if ((from == E1) && (board[from] == WHITE_KING)) {
                            if (to == H1) { //PolyGlot represents castling as e1h1 e1a1
                                to = G1;
                                type = MOVE_SHORT_CASTLE;
                            } else if (to == A1) {
                                to = C1;
                                type = MOVE_LONG_CASTLE;
                            }
                        }
                    } else {
                        if ((from == E8) && (board[from] == BLACK_KING)) {
                            if (to == H8) {
                                to = G8;
                                type = MOVE_SHORT_CASTLE;
                            } else if (to == A8) {
                                to = C8;
                                type = MOVE_LONG_CASTLE;
                            }
                        }
                    }
                }
                break;
            case 1:
                type = MOVE_PROMOTION_KNIGHT;
                break;
            case 2:
                type = MOVE_PROMOTION_BISHOP;
                break;
            case 3:
                type = MOVE_PROMOTION_ROOK;
                break;
            case 4:
                type = MOVE_PROMOTION_QUEEN;
                break;

        }
        return Utils::createMove(from, to, type, false);
    }
}

PolyGlotBook::PolyGlotBook(const std::string &path) : file(), mSize(0),
randomSide(POLYGLOT_RANDOM[780]), randomPiece(POLYGLOT_RANDOM),
randomCastle(POLYGLOT_RANDOM + 768), randomEnPassant(POLYGLOT_RANDOM + 772) {
    file.open(path.c_str(), std::ios::in | std::ios::binary);
    if (file.is_open()) {
        file.seekg(-16, file.end);
        mSize = file.tellg() / 16;
    } else {
        std::cerr << "Unable to open opening book " << path << std::endl;
    }

}

Optional<Move> PolyGlotBook::probe(const Board& board) {
    if (!isOpen()) {
        return std::experimental::nullopt;
    }
    Hash key = getKey(board);
    std::pair<std::vector<PolyGlotEntry>, int> temp = getEntries(key);
    const std::vector<PolyGlotEntry>& entries = temp.first;
    int sum = temp.second;
    if (!sum) {
        return std::experimental::nullopt;
    }
    int r = Utils::random(0, sum - 1);
    sum = 0;
    int i = 0;
    for (const PolyGlotEntry& entry : entries) {
        sum += entry.weight;
        if (r < sum) {
            return std::experimental::make_optional(convert(board, entry.move));
        }
        ++i;
    }
    return std::experimental::make_optional(convert(board, entries[i - 1].move));
}

PolyGlotEntry PolyGlotBook::readEntry(int idx) {

    file.seekg(idx * 16);
    Hash key = readInteger(8);
    uint16_t move = readInteger(2);
    uint16_t weight = readInteger(2);
    uint32_t learn = readInteger(4);
    return PolyGlotEntry{key, move, weight, learn};
}

inline Hash PolyGlotBook::getEnPassantHash(const Board& board) const {

    Hash ans = 0ull;
    int ep = board.getEnPassant();
    if (ep != -1) {
        if (board.getSide() == WHITE) {
            if (BLACK_PAWN_ATTACKS[ep] & board.getWhitePawns()) {
                ans = randomEnPassant[ep % 8];
            }
        } else {
            if (WHITE_PAWN_ATTACKS[ep] & board.getBlackPawns()) {
                ans = randomEnPassant[ep % 8];
            }
        }
    }
    return ans;

}

Hash PolyGlotBook::getCastleHash(const Board& board) const {
    Hash castle = 0;
    int wc = board.getWhiteCastlingRights();

    switch (wc) {
        case WHITE_CASTLE_BOTH:
            castle ^= randomCastle[0];
            castle ^= randomCastle[1];
            break;
        case WHITE_CASTLE_SHORT:
            castle ^= randomCastle[0];
            break;
        case WHITE_CASTLE_LONG:
            castle ^= randomCastle[1];
            break;
    }
    int bc = board.getBlackCastlingRights();
    switch (bc) {
        case BLACK_CASTLE_BOTH:
            castle ^= randomCastle[2];
            castle ^= randomCastle[3];
            break;
        case BLACK_CASTLE_SHORT:
            castle ^= randomCastle[2];
            break;
        case BLACK_CASTLE_LONG:
            castle ^= randomCastle[3];
            break;
    }
    return castle;

}

Hash PolyGlotBook::getPieceHash(const Board& board) const {
    Hash piece = 0;
    for (int i = 0; i < 64; i++) {
        if (board[i] != EMPTY_SQUARE) {
            int file = i % 8;
            int row = 7 - (i / 8);
            int pieceOffset = -1;
            switch (board[i]) {
                case EMPTY_SQUARE:
                    break;
                case BLACK_PAWN:
                    pieceOffset = getPieceOffset(0, row, file);
                    break;
                case WHITE_PAWN:
                    pieceOffset = getPieceOffset(1, row, file);
                    break;
                case BLACK_KNIGHT:
                    pieceOffset = getPieceOffset(2, row, file);
                    break;
                case WHITE_KNIGHT:
                    pieceOffset = getPieceOffset(3, row, file);
                    break;
                case BLACK_BISHOP:
                    pieceOffset = getPieceOffset(4, row, file);
                    break;
                case WHITE_BISHOP:
                    pieceOffset = getPieceOffset(5, row, file);
                    break;
                case BLACK_ROOK:
                    pieceOffset = getPieceOffset(6, row, file);
                    break;
                case WHITE_ROOK:
                    pieceOffset = getPieceOffset(7, row, file);
                    break;
                case BLACK_QUEEN:
                    pieceOffset = getPieceOffset(8, row, file);
                    break;
                case WHITE_QUEEN:
                    pieceOffset = getPieceOffset(9, row, file);
                    break;
                case BLACK_KING:
                    pieceOffset = getPieceOffset(10, row, file);
                    break;
                case WHITE_KING:
                    pieceOffset = getPieceOffset(11, row, file);
                    break;
            }
            piece ^= randomPiece[pieceOffset];
        }
    }
    return piece;
}

Hash PolyGlotBook::getKey(const Board & board) const {
    Hash pieces = getPieceHash(board);
    Hash castle = getCastleHash(board);
    Hash ep = getEnPassantHash(board);
    Hash side = board.getSide() == WHITE ? randomSide : 0ull;
    return pieces ^ castle ^ ep ^ side;
}

void PolyGlotBook::showMoves(const Board &board) {

    std::vector<PolyGlotEntry> entries = getEntries(board);
    Hash key = getKey(board);
    std::cout << "FEN: " << board.getPosition() << std::endl;
    std::cout << "Key: " << key << std::endl;
    for (size_t i = 0; i < entries.size(); ++i) {
        PolyGlotEntry e = entries[i];
        std::cout << Utils::toInputNotation(convert(board, e.move)) << " " <<
                e.weight << " " << e.learn << std::endl;
    }
}

std::pair<std::vector<PolyGlotEntry>, int> PolyGlotBook::getEntries(const Hash key) {
    std::vector<PolyGlotEntry> entries;
    int low = 0;
    int high = mSize;
    int mid = 0;
    PolyGlotEntry entry;
    while (low < high) {
        mid = (low + high) / 2;
        entry = readEntry(mid);
        if (entry.key < key) {
            low = mid + 1;
        } else if (entry.key > key) {
            high = mid - 1;
        } else {
            while (entry.key == key) {
                entry = (readEntry(--mid));
            }
            entry = readEntry(++mid);
            break;
        }
    }

    if (entry.key != key) {
        return std::make_pair(entries, 0);
    }
    entries.reserve(256);
    int i = mid;
    int sum = 0;
    bool flag = true;
    while (flag) {
        entries.push_back(entry);
        sum += entry.weight;
        i++;
        if (i == mSize) {
            flag = false;
        } else {
            entry = readEntry(i);
            if (entry.key != key) {
                flag = false;
            }
        }
    }
    return std::make_pair(entries, sum);
}
