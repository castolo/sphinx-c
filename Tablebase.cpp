///* 
// * File:   Tablebase.cpp
// * Author: Workstation
// * 
// * Created on 26 November 2013, 4:50 PM
// */
//
//#include "Tablebase.h"
//
//const int Tablebase::wdl_to_dtz[] = {
//    -1, -101, 0, 101, 1
//};
//
//Tablebase::Tablebase() {
//}
//
//Tablebase::~Tablebase() {
//}
//
//
//int Tablebase::rootProbe(Board &position);
//
//int Tablebase::probeWDL(Board &board, int *status) {
//    struct TBEntry *ptr;
//    struct TBHashEntry *ptr2;
//    uint64 idx;
//    uint64 key;
//    int i;
//    ubyte res;
//    int p[TBPIECES];
//
//    key = board.getMaterialKey();
//
//    if (!key) return 0;
//
//    ptr2 = TB_hash[key >> (64 - TBHASHBITS)];
//    for (i = 0; i < HSHMAX; i++)
//        if (ptr2[i].key == key) break;
//    if (i == HSHMAX) {
//        *status = 0;
//        return 0;
//    }
//
//    ptr = ptr2[i].ptr;
//    if (!ptr->ready) {
//        LOCK(TB_mutex);
//        if (!ptr->ready) {
//            char str[16];
//            prt_str(pos, str, ptr->key != key);
//            if (!init_table_wdl(ptr, str)) {
//                ptr->data = NULL;
//                ptr2[i].key = 0ULL;
//                *success = 0;
//                return 0;
//            }
//            ptr->ready = 1;
//        }
//        UNLOCK(TB_mutex);
//    }
//
//    int bside, mirror, cmirror;
//    if (!ptr->symmetric) {
//        if (key != ptr->key) {
//            cmirror = 8;
//            mirror = 0x38;
//            bside = (pos.side_to_move() == WHITE);
//        } else {
//            cmirror = mirror = 0;
//            bside = !(pos.side_to_move() == WHITE);
//        }
//    } else {
//        cmirror = pos.side_to_move() == WHITE ? 0 : 8;
//        mirror = pos.side_to_move() == WHITE ? 0 : 0x38;
//        bside = 0;
//    }
//
//    // p[i] is to contain the square 0-63 (A1-H8) for a piece of type
//    // pc[i] ^ cmirror, where 1 = white pawn, ..., 14 = black king.
//    // Pieces of the same type are guaranteed to be consecutive.
//    if (!ptr->has_pawns) {
//        struct TBEntry_piece *entry = (struct TBEntry_piece *) ptr;
//        ubyte *pc = entry->pieces[bside];
//        for (i = 0; i < entry->num;) {
//            Bitboard bb = pos.pieces((Color) ((pc[i] ^ cmirror) >> 3),
//                    (PieceType) (pc[i] & 0x07));
//            do {
//                p[i++] = pop_lsb(&bb);
//            } while (bb);
//        }
//        idx = encode_piece(entry, entry->norm[bside], p, entry->factor[bside]);
//        res = decompress_pairs(entry->precomp[bside], idx);
//    } else {
//        struct TBEntry_pawn *entry = (struct TBEntry_pawn *) ptr;
//        int k = entry->file[0].pieces[0][0] ^ cmirror;
//        Bitboard bb = pos.pieces((Color) (k >> 3), (PieceType) (k & 0x07));
//        i = 0;
//        do {
//            p[i++] = pop_lsb(&bb) ^ mirror;
//        } while (bb);
//        int f = pawn_file(entry, p);
//        ubyte *pc = entry->file[f].pieces[bside];
//        for (; i < entry->num;) {
//            bb = pos.pieces((Color) ((pc[i] ^ cmirror) >> 3),
//                    (PieceType) (pc[i] & 0x07));
//            do {
//                p[i++] = pop_lsb(&bb) ^ mirror;
//            } while (bb);
//        }
//        idx = encode_pawn(entry, entry->file[f].norm[bside], p, entry->file[f].factor[bside]);
//        res = decompress_pairs(entry->file[f].precomp[bside], idx);
//    }
//
//    return ((int) res) - 2;
//}
//
//int Tablebase::probeDTZ(Board &position, int *status);
//
//std::string Tablebase::getPositionString(Board &position, bool mirror) {
//    std::string chars = "KQRBNP";
//    std::string ans;
//
//    int *whitePieces = position.getPieces(WHITE);
//    int *blackPieces = position.getPieces(BLACK);
//    int pieces [2][6];
//    if (!mirror) {
//        pieces[0] = whitePieces;
//        pieces[1] = blackPieces;
//    } else {
//        pieces[1] = whitePieces;
//        pieces[0] = blackPieces;
//    }
//
//    for (int i = 0; i < 2; i++) {
//        for (int j = 0; j < 6; j++) {
//            while (pieces[i][j]--) {
//                ans.push_back(chars[j]);
//            }
//        }
//        ans.push_back('v');
//    }
//    delete[] whitePieces;
//    delete[] blackPieces;
//    return ans;
//}
//int Tablebase::probeWDLTable(Board&, int*);
//int Tablebase::probeDTZTable(Board&, int, int*);
//int Tablebase::probeAlphaBeta(Board&, int, int, int*);
//bool Tablebase::hasRepeated(Board&);