/* 
 * File:   Line.h
 * Author: Steve James <SD.James@outlook.com>
 *
 * This class represents a line or variation of moves. This is generally to be 
 * used for displaying the 'thoughts' of the engine and the line it believes 
 * will be followed.
 * 
 * Created on 19 April 2013, 11:19 AM
 * 
 */

#ifndef LINE_H
#define	LINE_H

#include <vector>
#include <string>
#include <iostream>
#include "Move.h"
#include "Optional.h"

/*
 * This class represents a line or variation of moves. This is generally to be 
 * used for displaying the 'thoughts' of the engine and the line it believes 
 * will be followed.
 */
class Line {
public:

    /**
     * Creates an empty line. 
     */
    Line() {
    }

    /**
     * Creates a line with the given parameters
     * @param moves the moves to be played in the line
     * @param eval the predicted score of playing the line, in centipawns
     * @param depth the depth of the line (replacements in the transposition table
     * mean that this value is not necessarily the length of the move vector)
     * @param nodes the number of nodes of the game tree examined by the agent
     * @param time the time spent calculating, in milliseconds
     */
    Line(std::vector<Move> moves, int eval, int depth, long int nodes, long int time) :
    line(moves), evaluation(eval), depth(depth), nodes(nodes), time(time) {
    }

    /**
     * Returns the move to play, according to the line (this will be the first 
     * move to play). If the line contains no moves, the optional that is returned
     * will be disengaged. 
     */
    inline Optional<Move> getBestMove() const {
        if (line.size() == 0) {
            return std::experimental::nullopt;
        } else {
            return std::experimental::make_optional(line[0]);
        }
    }

    /**
     * Add a move to the line
     * @param move the move to add
     */
    inline void addMove(Move move) {
        line.push_back(move);
    }

    /**
     * Returns true if the line is empty.
     */
    inline bool empty() const {
        return line.empty();
    }

    /**
     * Gets the number of moves in the line
     */
    inline size_t size() const {
        return line.size();
    }

    /**
     * Returns the score of the line, in centipawns 
     */
    inline int getEvaluation() const {
        return evaluation;
    }

    /**
     * Gets the depth of the search that led to this line being produced. 
     */
    inline int getDepth() const {
        return depth;
    }

    /**
     * Returns the number of nodes examined by the agent in producing this line 
     */
    inline int getNodes()const {
        return nodes;
    }

    /**
     * Returns the time spent calculating in order to produce this line 
     */
    inline long int getTime() const {
        return time;
    }

    /*
     * Gets the moves of the line
     */
    inline const std::vector<Move> getMoves() const {
        return line;
    }

    /*
     * Stream operator for displaying the line in the proper way.
     */
    friend std::ostream& operator<<(std::ostream& out, const Line& line) {
        float t = line.time / 1000.0;
        long long int nps = (t == 0) ? 0 : line.nodes / t;
        out << "info depth " << line.depth << " score cp " << line.evaluation <<
                " time " << (line.time) << " nodes " << line.nodes << " nps " << nps << " pv";
        for (size_t i = 0; i < line.size(); ++i) {
            out << " " << Utils::toInputNotation(line.line[i]);
        }
        return out;
    }

private:

    std::vector<Move> line;
    int evaluation = 0;
    int depth = 0;
    long int nodes = 0;
    long int time = 0;

};

#endif	/* LINE_H */
