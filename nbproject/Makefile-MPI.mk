#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=mpic++
CXX=mpic++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=MPI-Windows
CND_DLIB_EXT=dll
CND_CONF=MPI
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/Board.o \
	${OBJECTDIR}/Constants.o \
	${OBJECTDIR}/HashBoard.o \
	${OBJECTDIR}/KillerHeuristic.o \
	${OBJECTDIR}/Line.o \
	${OBJECTDIR}/MoveGenerator.o \
	${OBJECTDIR}/PolyGlotBook.o \
	${OBJECTDIR}/Sphinx.o \
	${OBJECTDIR}/Tablebase.o \
	${OBJECTDIR}/TranspositionTable.o \
	${OBJECTDIR}/main.o \
	${OBJECTDIR}/tbcore.o \
	${OBJECTDIR}/tbprobe.o

# Test Directory
TESTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}/tests

# Test Files
TESTFILES= \
	${TESTDIR}/TestFiles/f1 \
	${TESTDIR}/TestFiles/f2

# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=-std=c++0x
CXXFLAGS=-std=c++0x

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=-lm -lpthread

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/sphinx.exe

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/sphinx.exe: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	mpic++ -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/sphinx ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/Board.o: Board.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -s -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Board.o Board.cpp

${OBJECTDIR}/Constants.o: Constants.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -s -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Constants.o Constants.cpp

${OBJECTDIR}/HashBoard.o: HashBoard.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -s -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/HashBoard.o HashBoard.cpp

${OBJECTDIR}/KillerHeuristic.o: KillerHeuristic.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -s -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/KillerHeuristic.o KillerHeuristic.cpp

${OBJECTDIR}/Line.o: Line.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -s -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Line.o Line.cpp

${OBJECTDIR}/MoveGenerator.o: MoveGenerator.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -s -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/MoveGenerator.o MoveGenerator.cpp

${OBJECTDIR}/PolyGlotBook.o: PolyGlotBook.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -s -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/PolyGlotBook.o PolyGlotBook.cpp

${OBJECTDIR}/Sphinx.o: Sphinx.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -s -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Sphinx.o Sphinx.cpp

${OBJECTDIR}/Tablebase.o: Tablebase.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -s -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Tablebase.o Tablebase.cpp

${OBJECTDIR}/TranspositionTable.o: TranspositionTable.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -s -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/TranspositionTable.o TranspositionTable.cpp

${OBJECTDIR}/main.o: main.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -s -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main.o main.cpp

${OBJECTDIR}/tbcore.o: tbcore.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -O2 -I. -I. -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/tbcore.o tbcore.c

${OBJECTDIR}/tbprobe.o: tbprobe.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -s -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/tbprobe.o tbprobe.cpp

# Subprojects
.build-subprojects:

# Build Test Targets
.build-tests-conf: .build-conf ${TESTFILES}
${TESTDIR}/TestFiles/f1: ${TESTDIR}/tests/BoardTest.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles
	${LINK.cc}   -o ${TESTDIR}/TestFiles/f1 $^ ${LDLIBSOPTIONS} 

${TESTDIR}/TestFiles/f2: ${TESTDIR}/tests/PerformanceTest.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles
	${LINK.cc}   -o ${TESTDIR}/TestFiles/f2 $^ ${LDLIBSOPTIONS} 


${TESTDIR}/tests/BoardTest.o: tests/BoardTest.cpp 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -s -I. -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/BoardTest.o tests/BoardTest.cpp


${TESTDIR}/tests/PerformanceTest.o: tests/PerformanceTest.cpp 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -s -I. -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/PerformanceTest.o tests/PerformanceTest.cpp


${OBJECTDIR}/Board_nomain.o: ${OBJECTDIR}/Board.o Board.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/Board.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O2 -s -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Board_nomain.o Board.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/Board.o ${OBJECTDIR}/Board_nomain.o;\
	fi

${OBJECTDIR}/Constants_nomain.o: ${OBJECTDIR}/Constants.o Constants.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/Constants.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O2 -s -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Constants_nomain.o Constants.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/Constants.o ${OBJECTDIR}/Constants_nomain.o;\
	fi

${OBJECTDIR}/HashBoard_nomain.o: ${OBJECTDIR}/HashBoard.o HashBoard.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/HashBoard.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O2 -s -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/HashBoard_nomain.o HashBoard.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/HashBoard.o ${OBJECTDIR}/HashBoard_nomain.o;\
	fi

${OBJECTDIR}/KillerHeuristic_nomain.o: ${OBJECTDIR}/KillerHeuristic.o KillerHeuristic.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/KillerHeuristic.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O2 -s -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/KillerHeuristic_nomain.o KillerHeuristic.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/KillerHeuristic.o ${OBJECTDIR}/KillerHeuristic_nomain.o;\
	fi

${OBJECTDIR}/Line_nomain.o: ${OBJECTDIR}/Line.o Line.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/Line.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O2 -s -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Line_nomain.o Line.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/Line.o ${OBJECTDIR}/Line_nomain.o;\
	fi

${OBJECTDIR}/MoveGenerator_nomain.o: ${OBJECTDIR}/MoveGenerator.o MoveGenerator.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/MoveGenerator.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O2 -s -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/MoveGenerator_nomain.o MoveGenerator.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/MoveGenerator.o ${OBJECTDIR}/MoveGenerator_nomain.o;\
	fi

${OBJECTDIR}/PolyGlotBook_nomain.o: ${OBJECTDIR}/PolyGlotBook.o PolyGlotBook.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/PolyGlotBook.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O2 -s -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/PolyGlotBook_nomain.o PolyGlotBook.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/PolyGlotBook.o ${OBJECTDIR}/PolyGlotBook_nomain.o;\
	fi

${OBJECTDIR}/Sphinx_nomain.o: ${OBJECTDIR}/Sphinx.o Sphinx.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/Sphinx.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O2 -s -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Sphinx_nomain.o Sphinx.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/Sphinx.o ${OBJECTDIR}/Sphinx_nomain.o;\
	fi

${OBJECTDIR}/Tablebase_nomain.o: ${OBJECTDIR}/Tablebase.o Tablebase.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/Tablebase.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O2 -s -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Tablebase_nomain.o Tablebase.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/Tablebase.o ${OBJECTDIR}/Tablebase_nomain.o;\
	fi

${OBJECTDIR}/TranspositionTable_nomain.o: ${OBJECTDIR}/TranspositionTable.o TranspositionTable.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/TranspositionTable.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O2 -s -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/TranspositionTable_nomain.o TranspositionTable.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/TranspositionTable.o ${OBJECTDIR}/TranspositionTable_nomain.o;\
	fi

${OBJECTDIR}/main_nomain.o: ${OBJECTDIR}/main.o main.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/main.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O2 -s -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main_nomain.o main.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/main.o ${OBJECTDIR}/main_nomain.o;\
	fi

${OBJECTDIR}/tbcore_nomain.o: ${OBJECTDIR}/tbcore.o tbcore.c 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/tbcore.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.c) -O2 -I. -I. -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/tbcore_nomain.o tbcore.c;\
	else  \
	    ${CP} ${OBJECTDIR}/tbcore.o ${OBJECTDIR}/tbcore_nomain.o;\
	fi

${OBJECTDIR}/tbprobe_nomain.o: ${OBJECTDIR}/tbprobe.o tbprobe.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/tbprobe.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -O2 -s -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/tbprobe_nomain.o tbprobe.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/tbprobe.o ${OBJECTDIR}/tbprobe_nomain.o;\
	fi

# Run Test Targets
.test-conf:
	@if [ "${TEST}" = "" ]; \
	then  \
	    ${TESTDIR}/TestFiles/f1 || true; \
	    ${TESTDIR}/TestFiles/f2 || true; \
	else  \
	    ./${TEST} || true; \
	fi

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/sphinx.exe

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
