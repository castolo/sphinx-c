#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=MinGW-w64-Windows
CND_ARTIFACT_DIR_Debug=dist/Debug/MinGW-w64-Windows
CND_ARTIFACT_NAME_Debug=sphinx
CND_ARTIFACT_PATH_Debug=dist/Debug/MinGW-w64-Windows/sphinx
CND_PACKAGE_DIR_Debug=dist/Debug/MinGW-w64-Windows/package
CND_PACKAGE_NAME_Debug=sphinx.tar
CND_PACKAGE_PATH_Debug=dist/Debug/MinGW-w64-Windows/package/sphinx.tar
# Release configuration
CND_PLATFORM_Release=MinGW-w64-Windows
CND_ARTIFACT_DIR_Release=dist/Release/MinGW-w64-Windows
CND_ARTIFACT_NAME_Release=sphinx
CND_ARTIFACT_PATH_Release=dist/Release/MinGW-w64-Windows/sphinx
CND_PACKAGE_DIR_Release=dist/Release/MinGW-w64-Windows/package
CND_PACKAGE_NAME_Release=sphinx.tar
CND_PACKAGE_PATH_Release=dist/Release/MinGW-w64-Windows/package/sphinx.tar
# MPI configuration
CND_PLATFORM_MPI=MPI-Windows
CND_ARTIFACT_DIR_MPI=dist/MPI/MPI-Windows
CND_ARTIFACT_NAME_MPI=sphinx
CND_ARTIFACT_PATH_MPI=dist/MPI/MPI-Windows/sphinx
CND_PACKAGE_DIR_MPI=dist/MPI/MPI-Windows/package
CND_PACKAGE_NAME_MPI=sphinx.tar
CND_PACKAGE_PATH_MPI=dist/MPI/MPI-Windows/package/sphinx.tar
# MPIValgrind configuration
CND_PLATFORM_MPIValgrind=MPI-Windows
CND_ARTIFACT_DIR_MPIValgrind=dist/MPIValgrind/MPI-Windows
CND_ARTIFACT_NAME_MPIValgrind=sphinx
CND_ARTIFACT_PATH_MPIValgrind=dist/MPIValgrind/MPI-Windows/sphinx
CND_PACKAGE_DIR_MPIValgrind=dist/MPIValgrind/MPI-Windows/package
CND_PACKAGE_NAME_MPIValgrind=sphinx.tar
CND_PACKAGE_PATH_MPIValgrind=dist/MPIValgrind/MPI-Windows/package/sphinx.tar
# Sleepy configuration
CND_PLATFORM_Sleepy=MinGW-w64-Windows
CND_ARTIFACT_DIR_Sleepy=dist/Sleepy/MinGW-w64-Windows
CND_ARTIFACT_NAME_Sleepy=sphinx
CND_ARTIFACT_PATH_Sleepy=dist/Sleepy/MinGW-w64-Windows/sphinx
CND_PACKAGE_DIR_Sleepy=dist/Sleepy/MinGW-w64-Windows/package
CND_PACKAGE_NAME_Sleepy=sphinx.tar
CND_PACKAGE_PATH_Sleepy=dist/Sleepy/MinGW-w64-Windows/package/sphinx.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
