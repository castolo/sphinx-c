/* 
 * File:   Constants.h
 * Author: Steve James <SD.James@outlook.com>
 *
 * This file represents (most of) the constants and typedefs used throughout the program.
 * 
 * Created on 11 April 2013, 9:11 AM
 * 
 */

#ifndef CONSTANTS_H
#define	CONSTANTS_H

#include <cstdint>
#include <vector>
#include <string>
#include <array>

/*
 * An evaluation score is simply an integer that represents centipawns
 */
using Evaluation = int;
/*
 * Hash is in fact an unsigned 64-bit integer, used for computing Zobrist hashes
 */
using Hash = uint64_t;
/*
 * A bitboard is represented by an unsigned 64-bit integer.
 */
using Bitboard = uint64_t;
/*
 * A move is represented by a 16-bit integer, with each bit representing some
 * information. See Move.h for more details.
 */
using Move = int16_t;
/*
 * A typedef for a Move array of size 256 (which we can assume to be a good 
 * upperbound on the moves generated in a single position)
 */
using MoveList = std::array<Move, 256>;

/*
 * The maximum number of threads supported, taken to be the number of hardware threads 
 */
extern const unsigned int MAX_SUPPORTED_THREADS;

/*
 * A number used for the De Bruijn sequence B(2,6) used for calculating LS1B.
 * See https://chessprogramming.wikispaces.com/De+Bruijn+sequence and 
 * https://chessprogramming.wikispaces.com/BitScan#DeBruijnMultiplation
 */
extern const uint64_t DEBRUIJN;

/*
 * The maximum number of killer moves stored at each level for the killer heuristic
 */
const int MAX_KILLERS = 7; // insertion sort works well for +- 7 items

/*
 * The number of ranks of the board
 */
extern const int RANKS;

/*
 * The number of files of the board
 */
extern const int FILES;

/*
 * The name of the default opening book
 */
extern const std::string DEFAULT_OPENING_BOOK;

/*
 * Fen string representing the initial position
 */
extern const std::string START_POSITION;

/*
 * Fen string representing the classic Greek Gift Sacrifice position 
 */
extern const std::string GREEK_GIFT;

/*
 * Fen string representing the Fine 70 position for testing transposition tables 
 */
extern const std::string FINE70_POSITION;

/*
 * The minimum depth we are allowed to search to
 */
extern const int DEFAULT_MIN_DEPTH;

/*
 * The maximum depth we are allowed to search to
 */
const int DEFAULT_MAX_DEPTH = 35;

/*
 * Constant representing a NULL_MOVE
 */
extern const Move NULL_MOVE;

/*
 * The representation of infinity as used by the engine
 */
extern const int INFTY;

/*
 * The value representing checkmate
 */
extern const Evaluation MATE_VALUE;

/*
 * The value representing a draw
 */
extern const Evaluation DRAW_VALUE;

/*
 * The contempt value. 
 * See https://chessprogramming.wikispaces.com/Contempt+Factor
 */
extern const Evaluation CONTEMPT_VALUE;

/*
 * The size of the aspiration windows. 
 * See https://chessprogramming.wikispaces.com/Aspiration+Windows
 */
extern const int ASPIRATION_WINDOW_SIZE;

/*
 * The evaluation type as stored by the transposition table
 */
enum class EvalType {
    /*
     * The default evaluation type
     */
    NONE,

    /*
     * The evaluation score is an exact score
     */
    EXACT,

    /*
     * The evaluation represents an upperbound
     */
    UPPERBOUND,

    /*
     * The evaluation score represents an lower bound
     */
    LOWERBOUND,
};


// <editor-fold defaultstate="collapsed" desc="Evaluation Weights">

extern const int DEFAULT_QUEEN_VALUE;
extern const int DEFAULT_ROOK_VALUE;
extern const int DEFAULT_BISHOP_VALUE;
extern const int DEFAULT_KNIGHT_VALUE;
extern const int DEFAULT_PAWN_VALUE;

extern const int DEFAULT_START_MATERIAL;

extern const double DEFAULT_KNIGHT_MOBILITY;
extern const double DEFAULT_KING_MOBILITY;
extern const double DEFAULT_PAWN_MOBILITY;
extern const double DEFAULT_BISHOP_MOBILITY;
extern const double DEFAULT_ROOK_MOBILITY;
extern const double DEFAULT_QUEEN_MOBILITY;

extern const int DEFAULT_PASSED_PAWN_REWARD;
extern const int DEFAULT_DOUBLED_PAWN_PENALTY;
extern const int DEFAULT_ISOLATED_PAWN_PENALTY;

extern const double DEFAULT_ISOLATED_ADVANCED_PAWN_WEIGHT;
extern const double DEFAULT_SUPPORTED_ADVANCED_PAWN_WEIGHT;

extern const int DEFAULT_KNIGHT_ATTACK_WEIGHT;
extern const int DEFAULT_BISHOP_ATTACK_WEIGHT;
extern const int DEFAULT_QUEEN_ATTACK_WEIGHT;
extern const int DEFAULT_ROOK_ATTACK_WEIGHT;

extern const int DEFAULT_POOR_PAWN_SHIELD_PENALTY;

extern const int DEFAULT_PAWN_SHIELD_PENALTY_1;
extern const int DEFAULT_PAWN_SHIELD_PENALTY_2;
extern const int DEFAULT_PAWN_SHIELD_PENALTY_3;
extern const int DEFAULT_PAWN_SHIELD_PENALTY_4;
extern const int DEFAULT_PAWN_SHIELD_PENALTY_5;

extern const int DEFAULT_CENTRAL_KING_PENALTY;

extern const int DEFAULT_PAWN_SHIELD_PENALTY_6;
extern const int DEFAULT_PAWN_SHIELD_PENALTY_7;
extern const int DEFAULT_PAWN_SHIELD_PENALTY_8;
extern const int DEFAULT_PAWN_SHIELD_PENALTY_9;

extern const int DEFAULT_PIECE_VALUE[7];


// </editor-fold>

/*
 * Enum representing a side/colour
 */
enum Colour : short {
    /*
     * The white player
     */
    WHITE = 1,

    /*
     * The black player
     */
    BLACK = -1
};

/*
 * Enum representing directions between squares
 */
enum Direction : short {
    /*
     * The default: no direction
     */
    NONE = 0,

    /*
     * Horizontal direction
     */
    HORIZONTAL = 1,

    /*
     * Diagonal direction from A1 to H8
     */
    LOWER_LEFT_TOP_RIGHT = 7,

    /*
     * Vertical direction
     */
    VERTICAL = 8,

    /*
     * Diagonal direction from H1 to A8
     */
    LOWER_RIGHT_TOP_LEFT = 9,
};

/*
 * State of the castling rights for each side. This refers to whether castling 
 * can still take place at some stage, not whether castling is a legal move in 
 * a given position
 */
enum CastlingRights : short {
    /*
     * White has lost his castling rights
     */
    WHITE_CASTLE_NONE = 0,

    /*
     * White can only castle kingside
     */
    WHITE_CASTLE_SHORT = 1,

    /*
     * White can only castle queenside
     */
    WHITE_CASTLE_LONG = 2,

    /*
     * White can castle both king- and queenside
     */
    WHITE_CASTLE_BOTH = 3,
    /*
     * Black has lost his castling rights
     */
    BLACK_CASTLE_NONE = 4,

    /*
     * Black can only castle kingside
     */
    BLACK_CASTLE_SHORT = 5,

    /*
     * Black can only castle queenside
     */
    BLACK_CASTLE_LONG = 6,

    /*
     * Black can castle both king- and queenside
     */
    BLACK_CASTLE_BOTH = 7,
};

/*
 * Enum representing the different pieces on the board 
 */
enum Piece : short {
    /*
     * An empty square i.e. no piece
     */
    EMPTY_SQUARE,
    /*
     * A pawn
     */
    PAWN,
    /*
     * A knight
     */
    KNIGHT,
    /*
     * A bishop
     */
    BISHOP,
    /*
     * A rook
     */
    ROOK,
    /*
     * A queen
     */
    QUEEN,
    /*
     * A king
     */
    KING,
    /*
     * A white pawn
     */
    WHITE_PAWN = PAWN*WHITE,
    /*
     * A white knight
     */
    WHITE_KNIGHT = KNIGHT * WHITE,
    /*
     * A white bishop
     */
    WHITE_BISHOP = BISHOP * WHITE,
    /*
     * A white rook
     */
    WHITE_ROOK = ROOK * WHITE,
    /*
     * A white queen
     */
    WHITE_QUEEN = QUEEN * WHITE,
    /*
     * A white king
     */
    WHITE_KING = KING * WHITE,
    /*
     * A black pawn
     */
    BLACK_PAWN = PAWN * BLACK,
    /*
     * A black knight
     */
    BLACK_KNIGHT = KNIGHT * BLACK,
    /*
     * A black bishop
     */
    BLACK_BISHOP = BISHOP * BLACK,
    /*
     * A black rook
     */
    BLACK_ROOK = ROOK * BLACK,
    /*
     * A black queen
     */
    BLACK_QUEEN = QUEEN * BLACK,
    /*
     * A black king
     */
    BLACK_KING = KING * BLACK
};

/*
 * Enum representing different move types
 */
enum MoveType : int {
    /*
     * A normal move
     */
    MOVE_NORMAL = 0,

    /*
     * An en passant capture
     */
    MOVE_CAPTURE_EN_PASSANT = 1,

    /*
     * A kingside castle
     */
    MOVE_SHORT_CASTLE = 2,

    /*
     * A queenside castle
     */
    MOVE_LONG_CASTLE = 3,

    /*
     * A promotion to knight
     */
    MOVE_PROMOTION_KNIGHT = 4,

    /*
     * A promotion to bishop
     */
    MOVE_PROMOTION_BISHOP = 5,

    /*
     * A promotion to rook
     */
    MOVE_PROMOTION_ROOK = 6,

    /*
     * A promotion to queen
     */
    MOVE_PROMOTION_QUEEN = 7,
};

/*
 * Represents the different squares. 
 * 
 * <p> 
 * Note that they are ordered in the way the program handles the board with A8 = 0
 * and H1 = 63. This differs from the conventional method of having A1 = 0 and 
 * H8 = 63. 
 * </p>
 *
 */
enum Square : int {
    A8, B8, C8, D8, E8, F8, G8, H8,
    A7, B7, C7, D7, E7, F7, G7, H7,
    A6, B6, C6, D6, E6, F6, G6, H6,
    A5, B5, C5, D5, E5, F5, G5, H5,
    A4, B4, C4, D4, E4, F4, G4, H4,
    A3, B3, C3, D3, E3, F3, G3, H3,
    A2, B2, C2, D2, E2, F2, G2, H2,
    A1, B1, C1, D1, E1, F1, G1, H1,
    NUL = -1
};

/*
 * The index used to map De Bruijn sequences to the correct LS1B value
 */
extern const int LS1B_INDEX[64];

/**
 * Pawn shields
 */

extern const Bitboard WPS_PERFECT;

extern const Bitboard WPS_GOOD_KINGSIDE;
extern const Bitboard WPS_GOOD_QUEENSIDE;
extern const Bitboard WPS_OK;

extern const Bitboard BPS_PERFECT;
extern const Bitboard BPS_GOOD_KINGSIDE;
extern const Bitboard BPS_GOOD_QUEENSIDE;
extern const Bitboard BPS_OK;

/**
 * These masks represent diagonals but EXCLUDE the final ranks/files
 */
extern const Bitboard DIAG_A8_H1_MASK[64];

/**
 * These masks represent diagonals but EXCLUDE the final ranks/files
 */
extern const Bitboard DIAG_A1_H8_MASK[64];

/**
 * These masks represent ranks but EXCLUDE the final files
 */
extern const Bitboard RANK_MASK[64];
/**
 * These masks represent files but EXCLUDE the final ranks
 */
extern const Bitboard FILE_MASK[64];

extern const Bitboard BLACK_PAWN_ATTACKS[64];

extern const Bitboard WHITE_PAWN_ATTACKS[64];

extern const Bitboard BLACK_PAWN_MOVES[64];

extern const Bitboard WHITE_PAWN_MOVES[64];

extern const Bitboard OCCUPANCY_MASK_KING[64];

extern const Bitboard OCCUPANCY_MASK_KNIGHT[64];

extern const Bitboard OCCUPANCY_MASK_ROOK[64];

extern const Bitboard OCCUPANCY_MASK_BISHOP[64];

extern const Bitboard SE_BOARD[64];

extern const Bitboard SW_BOARD[64];

extern const Bitboard NW_BOARD[64];

extern const Bitboard NE_BOARD[64];

extern const Bitboard LEFT_BOARD[64];

extern const Bitboard DOWN_BOARD[64];

extern const Bitboard UP_BOARD[64];

extern const Bitboard RIGHT_BOARD[64];

extern const uint64_t ROOK_MAGICS[64];

extern const uint64_t BISHOP_MAGICS[64];

extern const short MAGIC_ROOK_SHIFTS[64];

extern const short MAGIC_BISHOP_SHIFTS[64];

extern std::array<std::array<Bitboard, 4096>, 64> MAGIC_ROOK_MOVES;
extern std::array<std::array<Bitboard, 512>, 64> MAGIC_BISHOP_MOVES;
extern std::array<std::array<Direction, 64>, 64> DIRECTIONS;

/**
 * For determining passed white pawns
 */
extern const Bitboard WHITE_FRONT_SPANS[64];

/**
 * For determining passed black pawns
 */
extern const Bitboard BLACK_FRONT_SPANS[64];


extern const Bitboard FILE_NEIGHBOUR[64];


extern const Bitboard FULL_BOARD;

/**
 * Masks (http://pages.cs.wisc.edu/~psilord/blog/data/chess-pages/physical.html)
 */


extern const Bitboard MASK_A8_H1_DIAG[64];

extern const Bitboard MASK_A1_H8_DIAG[64];

extern const Bitboard MASK_RANKS[64];

extern const Bitboard MASK_FILES[64];

extern const Bitboard CLEAR_RANK_1;
extern const Bitboard CLEAR_RANK_2;
extern const Bitboard CLEAR_RANK_3;
extern const Bitboard CLEAR_RANK_4;
extern const Bitboard CLEAR_RANK_5;
extern const Bitboard CLEAR_RANK_6;
extern const Bitboard CLEAR_RANK_7;
extern const Bitboard CLEAR_RANK_8;

extern const Bitboard MASK_RANK_1;
extern const Bitboard MASK_RANK_2;
extern const Bitboard MASK_RANK_3;
extern const Bitboard MASK_RANK_4;
extern const Bitboard MASK_RANK_5;
extern const Bitboard MASK_RANK_6;
extern const Bitboard MASK_RANK_7;
extern const Bitboard MASK_RANK_8;

extern const Bitboard CLEAR_FILE_A;
extern const Bitboard CLEAR_FILE_B;
extern const Bitboard CLEAR_FILE_C;
extern const Bitboard CLEAR_FILE_D;
extern const Bitboard CLEAR_FILE_E;
extern const Bitboard CLEAR_FILE_F;
extern const Bitboard CLEAR_FILE_G;
extern const Bitboard CLEAR_FILE_H;

extern const Bitboard MASK_FILE_A;
extern const Bitboard MASK_FILE_B;
extern const Bitboard MASK_FILE_C;
extern const Bitboard MASK_FILE_D;
extern const Bitboard MASK_FILE_E;
extern const Bitboard MASK_FILE_F;
extern const Bitboard MASK_FILE_G;
extern const Bitboard MASK_FILE_H;




extern const Bitboard KINGSIDE_MASK;
extern const Bitboard QUEENSIDE_MASK;


extern const Bitboard E1F1G1_MASK;
extern const Bitboard E8F8G8_MASK;
extern const Bitboard C1D1E1_MASK;
extern const Bitboard C8D8E8_MASK;

extern const Bitboard F1G1_MASK;
extern const Bitboard B1C1D1_MASK;
extern const Bitboard B8C8D8_MASK;
extern const Bitboard F8G8_MASK;

extern const Bitboard PREVENT_WHITE_SHORT_CASTLE;
extern const Bitboard PREVENT_WHITE_LONG_CASTLE;

extern const Bitboard PREVENT_BLACK_SHORT_CASTLE;
extern const Bitboard PREVENT_BLACK_LONG_CASTLE;

extern const uint64_t POLYGLOT_RANDOM[781];


#endif	/* CONSTANTS_H */
