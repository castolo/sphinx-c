/* 
 * File:   Utils.h
 * Author: Workstation
 *
 * Created on 25 November 2013, 10:07 PM
 * 
 * Copyright © Steve James <SD.James@outlook.com>
 * Unauthorized copying of this file, via any medium, is strictly prohibited
 * without the express permission of the author. * 
 */

#ifndef UTILS_H
#define	UTILS_H

#include <vector>
#include <algorithm>
#include <locale>
#include <sstream>
#include <functional>
#include <cctype>
#include <fstream>
#include <chrono>
#include <random>
#include <iostream>
#include <memory>

namespace {

    /*******************************************************************************
     * Assert macro. If the -DDEBUG flag is set, the assertation is tested,        *
     * otherwise it is ignored. Thus, in release mode, the checks are ignored and  * 
     * have no impact on performance.                                              *
     *******************************************************************************/

    inline void __assertError__(const std::string& expr, const std::string& file,
            const std::string& function, int line) {
        std::cerr << "Assertion error '" << expr << "' at function " << function
                << " in file " << file << " (line " << line << ')' << std::endl;
        abort(); //quit the program
    }
}

/*
 * A typedef for the 64-bit integer returned by Utils::getCurrentTime()
 */
using Time = int64_t;

namespace Utils {

#ifdef DEBUG
#define assert(X) if (!(X)) \
                    __assertError__(#X, __FILE__,__func__, __LINE__); //If the conditional X 
    //does not evaluate to true, the program aborts with an assertion error
#else
#define assert(X) //Since the -DDEBUG flag is not set, this line is ignored 
#endif

    /**
     * Calculate lg(N) where N is known to be a power of 2
     */
    inline int lg(int N) {
        const unsigned int b[] = {0xAAAAAAAA, 0xCCCCCCCC, 0xF0F0F0F0,
            0xFF00FF00, 0xFFFF0000};
        register unsigned int r = (N & b[0]) != 0;
        r |= ((N & b[4]) != 0) << 4;
        r |= ((N & b[3]) != 0) << 3;
        r |= ((N & b[2]) != 0) << 2;
        r |= ((N & b[1]) != 0) << 1;
        return r;
    }

    /**
     * Returns the current time since the 1 January 1970. The template provides
     * a way to specify the format of the time, such as seconds, nanoseconds, etc. 
     * Use C++11's chrono class types (e.g. std::chrono::milliseconds) to do this.
     * If no template is provided, this method will return milliseconds.
     */
    template <class T = std::chrono::milliseconds>
    inline Time getCurrentTime() {

        std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
        auto duration = now.time_since_epoch();
        return std::chrono::duration_cast<T>(duration).count();
    }

    /**
     * A psuedo-random number generator that produces a random number in the data type's range.
     * <b>No thread-safety is guaranteed by this method</b>
     */
    template <class T = int>
    inline T random() {
        static std::mt19937_64 engine(getCurrentTime());
        std::uniform_int_distribution<T> dist(std::numeric_limits<T>::lowest(),
                std::numeric_limits<T>::max());
        return dist(engine);
    }

    /**
     * Generates a random integer in the range [min, max]. <b>No thread-safety is 
     * guaranteed by this method</b>
     * @param min the lower bound
     * @param max the upper bound
     * @return a random integer
     */
    inline int random(int min, int max) {
        static std::mt19937 engine(getCurrentTime());
        std::uniform_int_distribution<int> dist(min, max);
        return dist(engine);
    }

    /**
     * Generates a random double in the range [0,1). <b>No thread-safety is 
     * guaranteed by this method</b>
     * @return a random double
     */
    inline double randomDouble() {
        static std::mt19937 engine(getCurrentTime());
        std::uniform_real_distribution<double> dist;
        return dist(engine);
    }

    /**
     * Generates a random integer in the range [min, max]. <b>No thread-safety is 
     * guaranteed by this method</b>
     * @param min the lower bound
     * @param max the upper bound
     * @return a random integer
     */
    inline double randomNormal(double mean, double stddev) {
        static std::mt19937 engine(getCurrentTime());
        std::normal_distribution<> dist(mean, stddev);
        return dist(engine);
    }

    /**
     * Shuffles a vector
     * @param list the vector to shuffle
     */
    template <class T>
    inline void shuffle(std::vector<T> &list) {
        static std::mt19937 engine(getCurrentTime());
        std::shuffle(list.begin(), list.end(), engine);
    }

    /**
     * Shuffles a vector
     * @param list the vector to shuffle
     */
    template <class T>
    inline void shuffle(std::vector<T> &list, int startIdx, int endIdx) {
        static std::mt19937 engine(getCurrentTime());
        std::shuffle(list.begin() + startIdx, list.begin() + endIdx, engine);
    }

    /**
     * Splits a string by a delimiting character
     * @param str the string to be split
     * @param delim the character to split on
     * @return a vector of elements that were split by the delimeter
     */
    inline std::vector<std::string> split(const std::string &str, char delim) {
        std::vector<std::string> elems;
        std::stringstream ss(str);
        std::string item;
        while (std::getline(ss, item, delim)) {
            elems.push_back(item);
        }
        return elems;
    }

    /**
     * Trims leading whitespace
     * @param s the string to trim
     * @return the string with leading whitespace removed
     */
    inline std::string leftTrim(std::string s) {
        s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
        return s;
    }

    /**
     * Trims trailing whitespace
     * @param s the string to trim
     * @return the string with trailing whitespace removed
     */
    inline std::string rightTrim(std::string s) {
        s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
        return s;
    }

    /**
     * Trims whitespace from both ends
     * @param s the string to trim
     * @return the string with leading and trailing whitespace removed
     */
    inline std::string trim(std::string s) {
        return leftTrim(rightTrim(s));
    }

    /**
     * Calculates the sign of a number, without branching
     * @param x the number
     * @return the sign of the number
     */
    inline int sign(int x) {
        return (0 < x) - (x < 0);
    }

    inline void LOG(std::string A) {

        std::ofstream f;
        f.open("log.txt", std::ios::out | std::ios::app);
        f << A << std::endl;
        f.close();
    }
}

namespace std {

    /**
     * While the C++11 standard has a make_shared, it overlooked the corresponding
     * make_unique method. This is the canonical way of creating a unique pointer.
     */
    template<typename T, typename... Args>
    inline std::unique_ptr<T> make_unique(Args&&... args) {
        return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
    }

}

#endif	/* UTILS_H */

