#include "AlternateAPI.h"
#include "PolyGlotBook.h"
#include <iostream>

uint64_t AlternateAPI::divide(Board& board, int depth, bool first) {
    if (depth == 0) {
        return 1;
    }
    uint64_t nodes = 0;
    bool flag = false;
    MoveList moves = board.generatePseudoMoves(flag);
    int j = 0;
    for (int i = 0; Move move = moves[i]; i++) {
        bool legal = board.makeMove(move, flag);
        if (legal) {
            uint64_t count = divide(board, depth - 1, false);
            if (first) {
                j++;
                std::cout << Utils::toInputNotation(move) << ": " << count << std::endl;
            }
            nodes += count;
        }
        board.undoMove(move);
    }
    if (first) {
        std::cout << "Moves: " << j << std::endl;
    }
    return nodes;
}

void AlternateAPI::perft(Board& board, int depth) {
    std::cout << "START" << std::endl;
    std::string path = "perftsuite.epd";
    std::ifstream file(path.c_str());
    std::string temp;
    uint64_t totalNodes = 0;
    double totalTime = 0;
    if (!file.good()) {
        std::cout << "perftsuite.epd not found" << std::endl;
        return;
    }
    for (int i = 0; file.good(); i++) {
        std::getline(file, temp);
        std::vector<std::string> vec = Utils::split(temp, ';');
        std::string fen = vec[0];
        board.setPosition(fen);
        for (int j = 1; j < vec.size() && j <= depth; j++) {
            std::stringstream sstr(vec[j]);
            uint64_t expected;
            sstr >> expected;
            if (expected > 0) {

                int64_t begin = Utils::getCurrentTime();
                uint64_t actual = AlternateAPI::minimax(board, j);
                int64_t end = Utils::getCurrentTime();
                totalTime += double(end - begin) / 1000;
                totalNodes += actual;
                if (actual != expected) {
                    std::cout << "Move gen failed on " << fen << " at depth " << j <<
                            "\nExpected = " << expected << ", actual = " << actual << std::endl;
                }
            }
        }
    }
    std::cout << totalNodes << " nodes evaluated" << " in " << totalTime << " at " <<
            (totalNodes / totalTime) / 1000.0 << " knps" << std::endl;
}

void AlternateAPI::perftSpeed(Board& board, int depth) {
    std::cout << "START" << std::endl;
    int64_t begin = Utils::getCurrentTime();
    uint64_t actual = AlternateAPI::minimax(board, depth);
    int64_t end = Utils::getCurrentTime();
    double time = double(end - begin) / 1000;
    std::cout << actual << " nodes evaluated" << " in " << time << " at " <<
            (actual / time) / 1000.0 << " knps" << std::endl;
}

uint64_t AlternateAPI::minimax(Board& board, int depth) {
    if (depth == 0) {
        return 1;
    }
    uint64_t nodes = 0;
    MoveList moves = board.generatePseudoMoves();
    for (int i = 0; Move move = moves[i]; i++) {
        bool legal = board.makeMove(move);
        if (legal) {
            nodes += minimax(board, depth - 1);
        }
        board.undoMove(move);
    }
    return nodes;
}

void AlternateAPI::showOpeningBook(const Board& board) {

    std::unique_ptr<PolyGlotBook> book = std::make_unique<PolyGlotBook>();
    if (book->isOpen()) {
        book->showMoves(board);
    }
}

void AlternateAPI::qSearch(const Board& board) {
    board.show();
    Sphinx engine;
    engine.setPosition(board.getPosition());
    std::cout << "Eval : " << engine.quiescenceSearch(-INFTY, INFTY) << std::endl;
}



