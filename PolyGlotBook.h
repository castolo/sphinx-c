/* 
 * File:   PolyglotBook.h
 * Author: Steve James <SD.James@outlook.com>
 *
 * A class that represents an interface to a PolyGlot opening book.
 * 
 * Created on 03 December 2013, 2:43 PM
 * 
 */

#ifndef POLYGLOTBOOK_H
#define	POLYGLOTBOOK_H

#include "Board.h"
#include "Optional.h"
#include <cstdint>
#include <vector>

namespace {

    /*
     * Struct representing an individual element in the PolyGlot book
     */
    struct PolyGlotEntry {
        Hash key;
        uint16_t move;
        uint16_t weight;
        uint32_t learn;
    };
}

/**
 * A class that represents an interface to a PolyGlot opening book.
 */
class PolyGlotBook {
public:

    /**
     * Creates a new opening book with the given path (which points to a PolyGlot
     * file)
     */
    explicit PolyGlotBook(const std::string &path = DEFAULT_OPENING_BOOK);
    /**
     * Gets the Zobrist hash from the current board to search the opening book with
     * @param board the board
     * @return the PolyGlot Zobrist hash
     */
    Hash getKey(const Board& board) const;

    /**
     * Displays the possible moves to play in the current position
     * @param board the current board
     */
    void showMoves(const Board& board);

    /**
     * Gets the size of the opening book
     * @return the number of entries in the book
     */
    inline int size() const {
        return mSize;
    }

    /**
     * Closes the book
     */
    inline void close() {
        file.close();
    }

    /**
     * Returns whether the book is open
     */
    inline bool isOpen() const {
        return file.is_open();
    }

    /**
     * Searches the opening book for a viable move and returns it, or null if
     * no such move exists
     * @param board the current board
     * @return a move to play from the opening book, if any
     */
    Optional<Move> probe(const Board& board);

    /**
     * Gets a list of entries that match the current board's position
     * @param board the current board 
     * @return a list of entries that match the current board's position
     */
    inline std::vector<PolyGlotEntry> getEntries(const Board& board) {
        return isOpen() ? getEntries(getKey(board)).first : std::vector<PolyGlotEntry>();
    }

private:
    /**
     * Reads an entry at the given index
     * @param idx the index
     * @return the entry at that index
     */
    PolyGlotEntry readEntry(int idx);

    /**
     * Gets the offset of the piece as used in the PolyGlot hash
     * @param piece the piece
     * @param row the row of the piece
     * @param file the file of the piece
     * @return the index to look up the random number for use in the Zobrist hash
     */
    inline static int getPieceOffset(int piece, int row, int file) {
        return (64 * piece) + (8 * row) + file;
    }

    /**
     * Gets the PolyGlot Zobrist hash based on the material
     * @param board the board
     * @return the material hash
     */
    Hash getPieceHash(const Board& board) const;

    /**
     * Gets the PolyGlot Zobrist hash based on the castling situation
     * @param board the board
     * @return the castling hash
     */
    Hash getCastleHash(const Board& board) const;

    /**
     * Gets the PolyGlot Zobrist hash based on the en passant situation
     * @param board the board
     * @return the en passant hash
     */
    Hash getEnPassantHash(const Board& board) const;

    /**
     * Gets a list of entries that match the current board's position
     * @param key the current position's hash 
     * @return a list of entries that match the current position and the sum of 
     * their weights
     */
    std::pair<std::vector<PolyGlotEntry>, int> getEntries(const Hash key);

    /**
     * Reads an n-bit integer from the file
     * @param numBytes the number of bytes to read
     * @return the integer read from the file
     */
    inline unsigned long long readInteger(int numBytes) {
        unsigned long long N = 0;
        for (int i = 0; i < numBytes; ++i) {
            int b = file.get();
            assert(!file.eof());
            N = (N << 8) | b;
        }
        return N;
    }



    std::ifstream file;
    int mSize;
    const Hash randomSide;
    const Hash * const randomPiece;
    const Hash * const randomCastle;
    const Hash * const randomEnPassant;


};

#endif	/* POLYGLOTBOOK_H */

